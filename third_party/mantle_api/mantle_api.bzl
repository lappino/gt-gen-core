load("@gt_gen_core//third_party:upstream_remotes.bzl", "ECLIPSE_GITLAB")
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_TAG = "v3.0.1"

def mantle_api():
    maybe(
        http_archive,
        name = "mantle_api",
        url = ECLIPSE_GITLAB + "/openpass/mantle-api/-/archive/{tag}/mantle-api-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "5e11a1b035243aa19f188c1f98466c4f38c84cbe07ec274a06f88382a5f9797e",
        strip_prefix = "mantle-api-{tag}".format(tag = _TAG),
        type = "tar.gz",
    )
