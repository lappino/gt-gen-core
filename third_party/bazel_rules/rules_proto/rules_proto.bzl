load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "4.0.0"

def rules_proto():
    maybe(
        http_archive,
        name = "rules_proto",
        sha256 = "66bfdf8782796239d3875d37e7de19b1d94301e8972b3cbd2446b332429b4df1",
        strip_prefix = "rules_proto-{version}".format(version = _VERSION),
        url = "https://github.com/bazelbuild/rules_proto/archive/refs/tags/{version}.tar.gz".format(version = _VERSION),
    )
