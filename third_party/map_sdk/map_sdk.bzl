load("@gt_gen_core//third_party:upstream_remotes.bzl", "ECLIPSE_GITLAB")
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_TAG = "v0.1.0"

def map_sdk():
    maybe(
        http_archive,
        name = "map_sdk",
        url = ECLIPSE_GITLAB + "/openpass/map-sdk/-/archive/{tag}/map-sdk-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "a56ef754a4c8045c8ec0df5bbf007dd73f9307afdb7dc90322fde2a7fe8771b7",
        strip_prefix = "map-sdk-{tag}".format(tag = _TAG),
        type = "tar.gz",
    )
