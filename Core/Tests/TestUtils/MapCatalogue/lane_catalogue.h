/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_LANECATALOGUE_H
#define GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_LANECATALOGUE_H

#include "Core/Tests/TestUtils/MapCatalogue/raw_lane_boundary_builder.h"
#include "Core/Tests/TestUtils/MapCatalogue/raw_lane_builder.h"

#include <cassert>

namespace gtgen::core::test_utils
{

class LaneCatalogue
{
  public:
    enum class Rolled
    {
        kRight,
        kLeft,
        kNormal
    };
    enum class Pitched
    {
        kUp,
        kDown,
        kNormal
    };
    struct LaneConfiguration
    {
        int number_of_center_line_points{2};
        int distance_between_center_line_points{1};
        mantle_api::Vec3<units::length::meter_t> origin{units::length::meter_t(0.0),
                                                        units::length::meter_t(0.0),
                                                        units::length::meter_t(0.0)};
        Rolled rolled{Rolled::kNormal};
        Pitched pitched{Pitched::kNormal};
    };

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    StraightEastingLaneWithNPoints(LaneConfiguration config)
    {
        double lane_width{4.0};
        double half_lane_width{lane_width / 2};
        environment::map::LaneBoundary::Points right_boundary_points;
        environment::map::LaneBoundary::Points left_boundary_points;
        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_points;

        double pitch_height = 0;

        auto create_coordinate_vector = [&pitch_height, &config](int x, double y, double z) {
            z += pitch_height;
            mantle_api::Vec3<units::length::meter_t> center_line_point{
                units::length::meter_t(static_cast<double>(x)), units::length::meter_t(y), units::length::meter_t(z)};
            center_line_point += config.origin;
            return center_line_point;
        };
        for (int i = 0; i < config.number_of_center_line_points * config.distance_between_center_line_points;
             i += config.distance_between_center_line_points)
        {
            double right_height = config.rolled == Rolled::kLeft ? 2.0 : 0.0;
            auto pos = create_coordinate_vector(i, -half_lane_width, right_height);
            right_boundary_points.emplace_back(environment::map::LaneBoundary::Point{pos, 0.0, 0.0});

            double left_height = config.rolled == Rolled::kRight ? 4.0 : (config.rolled == Rolled::kLeft ? -2.0 : 0.0);
            pos = create_coordinate_vector(i, half_lane_width, left_height);
            left_boundary_points.emplace_back(environment::map::LaneBoundary::Point{pos, 0.0, 0.0});

            double center_height = config.rolled == Rolled::kRight ? 2.0 : 0.0;
            pos = create_coordinate_vector(i, 0.0, center_height);
            center_line_points.emplace_back(pos);

            pitch_height += config.pitched == Pitched::kUp ? 20.0 : (config.pitched == Pitched::kDown ? -20.0 : 0.0);
        }

        auto right_boundary = BuildWhiteLineBoundary(right_boundary_points);
        auto left_boundary = BuildWhiteLineBoundary(left_boundary_points);
        auto lane = BuildLaneWithBoundaries(center_line_points, {left_boundary.id}, {right_boundary.id});

        return {lane, {left_boundary, right_boundary}};
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    EastingMergingLeftLaneWithNPoints(LaneConfiguration config)
    {
        double lane_width{4.0};
        double half_lane_width{lane_width / 2};
        environment::map::LaneBoundary::Points right_boundary_points;
        environment::map::LaneBoundary::Points left_boundary_points;
        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_points;

        double y = 0;
        for (int i = 0; i < config.number_of_center_line_points * config.distance_between_center_line_points;
             i += config.distance_between_center_line_points)
        {
            if (i >= config.number_of_center_line_points * config.distance_between_center_line_points / 2.0)
            {
                y -= lane_width / static_cast<double>(config.number_of_center_line_points *
                                                      config.distance_between_center_line_points / 2.0);
                if (y < -lane_width)
                {
                    y = -lane_width;  // make sure that the last center line point is not slightly off
                }
            }

            environment::map::LaneBoundary::Point right_boundary_point{{units::length::meter_t(static_cast<double>(i)),
                                                                        units::length::meter_t(y - half_lane_width),
                                                                        units::length::meter_t(0.0)},
                                                                       0.0,
                                                                       0.0};
            right_boundary_point.position += config.origin;
            right_boundary_points.emplace_back(right_boundary_point);

            environment::map::LaneBoundary::Point left_boundary_point{{units::length::meter_t(static_cast<double>(i)),
                                                                       units::length::meter_t(y + half_lane_width),
                                                                       units::length::meter_t(0.0)},
                                                                      0.0,
                                                                      0.0};
            left_boundary_point.position += config.origin;
            left_boundary_points.emplace_back(left_boundary_point);

            mantle_api::Vec3<units::length::meter_t> center_line_point{
                units::length::meter_t(static_cast<double>(i)), units::length::meter_t(y), units::length::meter_t(0.0)};
            center_line_point += config.origin;
            center_line_points.emplace_back(center_line_point);
        }

        auto right_boundary = BuildWhiteLineBoundary(right_boundary_points);
        auto left_boundary = BuildWhiteLineBoundary(left_boundary_points);
        auto lane = BuildLaneWithBoundaries(center_line_points, {left_boundary.id}, {right_boundary.id});

        return {lane, {left_boundary, right_boundary}};
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    EastingMergingRightLaneWithNPoints(LaneConfiguration config)
    {
        double lane_width{4.0};
        double half_lane_width{lane_width / 2};
        environment::map::LaneBoundary::Points right_boundary_points;
        environment::map::LaneBoundary::Points left_boundary_points;
        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_points;

        double y = 0;
        for (int i = 0; i < config.number_of_center_line_points * config.distance_between_center_line_points;
             i += config.distance_between_center_line_points)
        {
            if (i >= config.number_of_center_line_points * config.distance_between_center_line_points / 2.0)
            {
                y += lane_width / static_cast<double>(config.number_of_center_line_points *
                                                      config.distance_between_center_line_points / 2.0);
                if (y > lane_width)
                {
                    y = lane_width;  // make sure that the last center line point is not slightly off
                }
            }

            environment::map::LaneBoundary::Point left_boundary_point{{units::length::meter_t(static_cast<double>(i)),
                                                                       units::length::meter_t(y + half_lane_width),
                                                                       units::length::meter_t(0.0)},
                                                                      0.0,
                                                                      0.0};
            left_boundary_point.position += config.origin;
            left_boundary_points.emplace_back(left_boundary_point);

            environment::map::LaneBoundary::Point right_boundary_point{{units::length::meter_t(static_cast<double>(i)),
                                                                        units::length::meter_t(y - half_lane_width),
                                                                        units::length::meter_t(0.0)},
                                                                       0.0,
                                                                       0.0};
            right_boundary_point.position += config.origin;
            right_boundary_points.emplace_back(right_boundary_point);

            mantle_api::Vec3<units::length::meter_t> center_line_point{
                units::length::meter_t(static_cast<double>(i)), units::length::meter_t(y), units::length::meter_t(0.0)};
            center_line_point += config.origin;
            center_line_points.emplace_back(center_line_point);
        }

        auto right_boundary = BuildWhiteLineBoundary(right_boundary_points);
        auto left_boundary = BuildWhiteLineBoundary(left_boundary_points);
        auto lane = BuildLaneWithBoundaries(center_line_points, {left_boundary.id}, {right_boundary.id});

        return {lane, {left_boundary, right_boundary}};
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    EastingSplittingLeftLaneWithNPoints(LaneConfiguration config)
    {
        double lane_width{4.0};
        double half_lane_width{lane_width / 2};
        environment::map::LaneBoundary::Points right_boundary_points;
        environment::map::LaneBoundary::Points left_boundary_points;
        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_points;

        double y = -lane_width;
        double max_x = config.number_of_center_line_points * config.distance_between_center_line_points;
        double max_x_half = max_x / 2;
        for (int i = 0; i < max_x; i += config.distance_between_center_line_points)
        {
            if (i > 0 && i < max_x_half)
            {
                y += lane_width / static_cast<double>(max_x_half);
            }
            else if (i >= max_x_half)
            {
                y = 0;
            }

            environment::map::LaneBoundary::Point right_boundary_point{{units::length::meter_t(static_cast<double>(i)),
                                                                        units::length::meter_t(y - half_lane_width),
                                                                        units::length::meter_t(0.0)},
                                                                       0.0,
                                                                       0.0};
            right_boundary_point.position += config.origin;
            right_boundary_points.emplace_back(right_boundary_point);

            environment::map::LaneBoundary::Point left_boundary_point{{units::length::meter_t(static_cast<double>(i)),
                                                                       units::length::meter_t(y + half_lane_width),
                                                                       units::length::meter_t(0.0)},
                                                                      0.0,
                                                                      0.0};
            left_boundary_point.position += config.origin;
            left_boundary_points.emplace_back(left_boundary_point);

            mantle_api::Vec3<units::length::meter_t> center_line_point{
                units::length::meter_t(static_cast<double>(i)), units::length::meter_t(y), units::length::meter_t(0.0)};
            center_line_point += config.origin;
            center_line_points.emplace_back(center_line_point);
        }

        auto right_boundary = BuildWhiteLineBoundary(right_boundary_points);
        auto left_boundary = BuildWhiteLineBoundary(left_boundary_points);
        auto lane = BuildLaneWithBoundaries(center_line_points, {left_boundary.id}, {right_boundary.id});

        return {lane, {left_boundary, right_boundary}};
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    EastingSplittingRightLaneWithNPoints(LaneConfiguration config)
    {
        double lane_width{4.0};
        double half_lane_width{lane_width / 2};
        environment::map::LaneBoundary::Points right_boundary_points;
        environment::map::LaneBoundary::Points left_boundary_points;
        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_points;

        double y = lane_width;
        double max_x = config.number_of_center_line_points * config.distance_between_center_line_points;
        double max_x_half = max_x / 2;
        for (int i = 0; i < max_x; i += config.distance_between_center_line_points)
        {
            if (i > 0 && i < max_x_half)
            {
                y -= lane_width / static_cast<double>(max_x_half);
            }
            else if (i >= max_x_half)
            {
                y = 0;
            }

            environment::map::LaneBoundary::Point right_boundary_point{{units::length::meter_t(static_cast<double>(i)),
                                                                        units::length::meter_t(y - half_lane_width),
                                                                        units::length::meter_t(0.0)},
                                                                       0.0,
                                                                       0.0};
            right_boundary_point.position += config.origin;
            right_boundary_points.emplace_back(right_boundary_point);

            environment::map::LaneBoundary::Point left_boundary_point{{units::length::meter_t(static_cast<double>(i)),
                                                                       units::length::meter_t(y + half_lane_width),
                                                                       units::length::meter_t(0.0)},
                                                                      0.0,
                                                                      0.0};
            left_boundary_point.position += config.origin;
            left_boundary_points.emplace_back(left_boundary_point);

            mantle_api::Vec3<units::length::meter_t> center_line_point{
                units::length::meter_t(static_cast<double>(i)), units::length::meter_t(y), units::length::meter_t(0.0)};
            center_line_point += config.origin;
            center_line_points.emplace_back(center_line_point);
        }

        auto right_boundary = BuildWhiteLineBoundary(right_boundary_points);
        auto left_boundary = BuildWhiteLineBoundary(left_boundary_points);
        auto lane = BuildLaneWithBoundaries(center_line_points, {left_boundary.id}, {right_boundary.id});

        return {lane, {left_boundary, right_boundary}};
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    StraightEastingLaneWithTwoPoints(mantle_api::Vec3<units::length::meter_t> origin = {units::length::meter_t(0.0),
                                                                                        units::length::meter_t(0.0),
                                                                                        units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 2;
        config.distance_between_center_line_points = 1;
        return StraightEastingLaneWithNPoints(config);
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    StraightEastingLaneWithFivePoints(mantle_api::Vec3<units::length::meter_t> origin = {units::length::meter_t(0.0),
                                                                                         units::length::meter_t(0.0),
                                                                                         units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 5;
        config.distance_between_center_line_points = 1;
        return StraightEastingLaneWithNPoints(config);
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    StraightEastingLaneWithHundredPoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                             units::length::meter_t(0.0),
                                             units::length::meter_t(0.0),
                                             units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 100;
        config.distance_between_center_line_points = 1;
        return StraightEastingLaneWithNPoints(config);
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    Straight6kmEastingLaneWithTwoPoints(mantle_api::Vec3<units::length::meter_t> origin = {units::length::meter_t(0.0),
                                                                                           units::length::meter_t(0.0),
                                                                                           units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 2;
        config.distance_between_center_line_points = 6000;
        return StraightEastingLaneWithNPoints(config);
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    LongStraightEastingLaneWithTwoPoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                             units::length::meter_t(0.0),
                                             units::length::meter_t(0.0),
                                             units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 2;
        config.distance_between_center_line_points = 100;
        return StraightEastingLaneWithNPoints(config);
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    LongStraightEastingLaneWithThreePoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                               units::length::meter_t(0.0),
                                               units::length::meter_t(0.0),
                                               units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 3;
        config.distance_between_center_line_points = 100;
        return StraightEastingLaneWithNPoints(config);
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    EastingMergingLeftLaneWithHundredPoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                                units::length::meter_t(0.0),
                                                units::length::meter_t(0.0),
                                                units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 100;
        config.distance_between_center_line_points = 1;
        return EastingMergingLeftLaneWithNPoints(config);
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    EastingMergingRightRightLaneWithHundredPoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                                      units::length::meter_t(0.0),
                                                      units::length::meter_t(0.0),
                                                      units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 100;
        config.distance_between_center_line_points = 1;
        return EastingMergingRightLaneWithNPoints(config);
    }

    /// @brief create easting lane, which merges from left to right, where left lane boundary is parallel to centerline,
    /// while right lane boundary is straight. Lane width at start is 4m
    /// @verbatim
    /*
    ///         ———————————————————\
    ///                              \
    ///                                \
    ///                                  \
    /// Origin  X  *  *  *  *  *  *        \
    ///                             *        \
    ///                               *        \
    ///                                 *        \
    ///         ——————————————————————————*————————
    ///                                     *
    ///                                       *
    ///                                         *
    ///                                           *
    */
    /// @endverbatim

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    EastingMergingTriangularLeftLaneWithHundredPoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                                          units::length::meter_t(0),
                                                          units::length::meter_t(0),
                                                          units::length::meter_t(0)})
    {
        const double lane_width{4.0};
        const double half_lane_width{lane_width / 2};
        auto lane_and_boundaries = EastingMergingLeftLaneWithHundredPoints(origin);

        auto& right_lane_boundary = lane_and_boundaries.second[1];
        for (auto& lane_boundary_point : right_lane_boundary.points)
        {
            lane_boundary_point.position.y = origin.y - units::length::meter_t(half_lane_width);
        }

        return lane_and_boundaries;
    }

    /// @brief create easting lane, which splits from right to left, where left lane boundary is parallel to centerline,
    /// while right lane boundary is straight. Lane width at end is 4m
    /// @verbatim
    /*
    ///                        /———————————————————
    ///                      /
    ///                    /
    ///                  /
    /// Origin  X      /        *  *  *  *  *  *  *
    ///              /        *
    ///            /        *
    ///          /        *
    ///         ————————*——————————————————————————
    ///               *
    ///             *
    ///           *
    ///         *
    */
    /// @endverbatim
    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    EastingSplittingTriangularLeftLaneWithHundredPoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                                            units::length::meter_t(0),
                                                            units::length::meter_t(0),
                                                            units::length::meter_t(0)})
    {
        const double lane_width{4.0};
        const double half_lane_width{lane_width / 2};
        auto lane_and_boundaries = EastingSplittingLeftLaneWithHundredPoints(origin);

        auto& right_lane_boundary = lane_and_boundaries.second[1];
        for (auto& lane_boundary_point : right_lane_boundary.points)
        {
            lane_boundary_point.position.y = origin.y - units::length::meter_t(half_lane_width);
        }

        return lane_and_boundaries;
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    EastingSplittingLeftLaneWithHundredPoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                                  units::length::meter_t(0.0),
                                                  units::length::meter_t(0.0),
                                                  units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 100;
        config.distance_between_center_line_points = 1;
        return EastingSplittingLeftLaneWithNPoints(config);
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    EastingSplittingRightLaneWithHundredPoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                                   units::length::meter_t(0.0),
                                                   units::length::meter_t(0.0),
                                                   units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 100;
        config.distance_between_center_line_points = 1;
        return EastingSplittingRightLaneWithNPoints(config);
    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    StraightNorthingLaneWithNPoints(LaneConfiguration config)
    {
        environment::map::LaneBoundary::Points right_boundary_points;
        environment::map::LaneBoundary::Points left_boundary_points;
        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_points;

        for (int i = 0; i < config.number_of_center_line_points * config.distance_between_center_line_points;
             i += config.distance_between_center_line_points)
        {
            environment::map::LaneBoundary::Point right_boundary_point{{units::length::meter_t(0.5),
                                                                        units::length::meter_t(static_cast<double>(i)),
                                                                        units::length::meter_t(0.0)},
                                                                       0.0,
                                                                       0.0};
            right_boundary_point.position += config.origin;
            right_boundary_points.emplace_back(right_boundary_point);

            environment::map::LaneBoundary::Point left_boundary_point{{units::length::meter_t(-0.5),
                                                                       units::length::meter_t(static_cast<double>(i)),
                                                                       units::length::meter_t(0.0)},
                                                                      0.0,
                                                                      0.0};
            left_boundary_point.position += config.origin;
            left_boundary_points.emplace_back(left_boundary_point);

            mantle_api::Vec3<units::length::meter_t> center_line_point{units::length::meter_t(0.0),
                                                                       units::length::meter_t(static_cast<double>(i)),
                                                                       units::length::meter_t(0.0)};
            center_line_point += config.origin;
            center_line_points.emplace_back(center_line_point);
        }

        auto right_boundary = BuildWhiteLineBoundary(right_boundary_points);
        auto left_boundary = BuildWhiteLineBoundary(left_boundary_points);
        auto lane = BuildLaneWithBoundaries(center_line_points, {left_boundary.id}, {right_boundary.id});

        return {lane, {left_boundary, right_boundary}};
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    StraightNorthingLaneWithTwoPoints(mantle_api::Vec3<units::length::meter_t> origin = {units::length::meter_t(0.0),
                                                                                         units::length::meter_t(0.0),
                                                                                         units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 2;
        config.distance_between_center_line_points = 1;
        return StraightNorthingLaneWithNPoints(config);
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    StraightNorthingLaneWithFivePoints(mantle_api::Vec3<units::length::meter_t> origin = {units::length::meter_t(0.0),
                                                                                          units::length::meter_t(0.0),
                                                                                          units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 5;
        config.distance_between_center_line_points = 1;
        return StraightNorthingLaneWithNPoints(config);
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    StraightNorthingLaneWithHundredPoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                              units::length::meter_t(0.0),
                                              units::length::meter_t(0.0),
                                              units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 100;
        config.distance_between_center_line_points = 1;
        return StraightNorthingLaneWithNPoints(config);
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    LongStraightNorthingLaneWithTwoPoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                              units::length::meter_t(0.0),
                                              units::length::meter_t(0.0),
                                              units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 2;
        config.distance_between_center_line_points = 100;
        return StraightNorthingLaneWithNPoints(config);
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    LongStraightNorthingLaneWithThreePoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                                units::length::meter_t(0.0),
                                                units::length::meter_t(0.0),
                                                units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 3;
        config.distance_between_center_line_points = 100;
        return StraightNorthingLaneWithNPoints(config);
    }

    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    StraightNorthEastingLaneWithNPoints(LaneConfiguration config)
    {
        environment::map::LaneBoundary::Points right_boundary_points;
        environment::map::LaneBoundary::Points left_boundary_points;
        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_points;

        for (int i = 0; i < config.number_of_center_line_points * config.distance_between_center_line_points;
             i += config.distance_between_center_line_points)
        {
            environment::map::LaneBoundary::Point right_boundary_point{
                {units::length::meter_t(static_cast<double>(i) + 0.5),
                 units::length::meter_t(static_cast<double>(i)),
                 units::length::meter_t(0.0)},
                0.0,
                0.0};
            right_boundary_point.position += config.origin;
            right_boundary_points.emplace_back(right_boundary_point);

            environment::map::LaneBoundary::Point left_boundary_point{
                {units::length::meter_t(static_cast<double>(i) - 0.5),
                 units::length::meter_t(static_cast<double>(i)),
                 units::length::meter_t(0.0)},
                0.0,
                0.0};
            left_boundary_point.position += config.origin;
            left_boundary_points.emplace_back(left_boundary_point);

            mantle_api::Vec3<units::length::meter_t> center_line_point{units::length::meter_t(static_cast<double>(i)),
                                                                       units::length::meter_t(static_cast<double>(i)),
                                                                       units::length::meter_t(0.0)};
            center_line_point += config.origin;
            center_line_points.emplace_back(center_line_point);
        }

        auto right_boundary = BuildWhiteLineBoundary(right_boundary_points);
        auto left_boundary = BuildWhiteLineBoundary(left_boundary_points);
        auto lane = BuildLaneWithBoundaries(center_line_points, {left_boundary.id}, {right_boundary.id});

        return {lane, {left_boundary, right_boundary}};
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    StraightNorthEastingLaneWithTwoPoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                              units::length::meter_t(0.0),
                                              units::length::meter_t(0.0),
                                              units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 2;
        config.distance_between_center_line_points = 1;
        return StraightNorthEastingLaneWithNPoints(config);
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    StraightNorthEastingLaneWithFivePoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                               units::length::meter_t(0.0),
                                               units::length::meter_t(0.0),
                                               units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 5;
        config.distance_between_center_line_points = 1;
        return StraightNorthEastingLaneWithNPoints(config);
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    StraightNorthEastingLaneWithHundredPoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                                  units::length::meter_t(0.0),
                                                  units::length::meter_t(0.0),
                                                  units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 100;
        config.distance_between_center_line_points = 1;
        return StraightNorthEastingLaneWithNPoints(config);
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    LongStraightNorthEastingLaneWithTwoPoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                                  units::length::meter_t(0.0),
                                                  units::length::meter_t(0.0),
                                                  units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 2;
        config.distance_between_center_line_points = 100;
        return StraightNorthEastingLaneWithNPoints(config);
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    LongStraightNorthEastingLaneWithThreePoints(mantle_api::Vec3<units::length::meter_t> origin = {
                                                    units::length::meter_t(0.0),
                                                    units::length::meter_t(0.0),
                                                    units::length::meter_t(0.0)})
    {
        LaneConfiguration config;
        config.origin = origin;
        config.number_of_center_line_points = 3;
        config.distance_between_center_line_points = 100;
        return StraightNorthEastingLaneWithNPoints(config);
    }

    /// Note, the boundaries on both side are divided in two parts:
    /// The right side is ordered aka x-> s -> x (they share point "s")
    /// The left side is messed up aka x -> s <- x (the second part has a reverse ordering)
    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>> NorthingToWestingCurve()
    {
        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_points{
            {units::length::meter_t(0.0), units::length::meter_t(0.0), units::length::meter_t(0.0)},
            {units::length::meter_t(0.0), units::length::meter_t(2.0), units::length::meter_t(0.0)},
            {units::length::meter_t(-1.0), units::length::meter_t(4.0), units::length::meter_t(0.0)},
            {units::length::meter_t(-3.0), units::length::meter_t(6.0), units::length::meter_t(0.0)},
            {units::length::meter_t(-5.0), units::length::meter_t(5.0), units::length::meter_t(0.0)},
            {units::length::meter_t(-7.0), units::length::meter_t(7.0), units::length::meter_t(0.0)}};

        environment::map::LaneBoundary::Points right_boundary_points1{
            {{units::length::meter_t(2.0), units::length::meter_t(0.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(2.0), units::length::meter_t(2.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(1.0), units::length::meter_t(4.0), units::length::meter_t(0.0)}, 0, 0}};

        environment::map::LaneBoundary::Points right_boundary_points2{
            {{units::length::meter_t(1.0), units::length::meter_t(4.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(0.0), units::length::meter_t(6.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(-5.0), units::length::meter_t(9.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(-7.0), units::length::meter_t(9.0), units::length::meter_t(0.0)}, 0, 0}};

        environment::map::LaneBoundary::Points left_boundary_points1{
            {{units::length::meter_t(-3.0), units::length::meter_t(4.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(-5.0), units::length::meter_t(5.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(-7.0), units::length::meter_t(5.0), units::length::meter_t(0.0)}, 0, 0}};

        environment::map::LaneBoundary::Points left_boundary_points2{
            {{units::length::meter_t(-2.0), units::length::meter_t(0.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(-2.0), units::length::meter_t(2.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(-3.0), units::length::meter_t(4.0), units::length::meter_t(0.0)}, 0, 0}};

        auto right_boundary1 = BuildWhiteLineBoundary(right_boundary_points1);
        auto right_boundary2 = BuildWhiteLineBoundary(right_boundary_points2);

        auto left_boundary1 = BuildWhiteLineBoundary(left_boundary_points1);
        auto left_boundary2 = BuildWhiteLineBoundary(left_boundary_points2);

        auto lane = BuildLaneWithBoundaries(
            center_line_points, {left_boundary1.id, left_boundary2.id}, {right_boundary1.id, right_boundary2.id});

        return {lane, {left_boundary1, left_boundary2, right_boundary1, right_boundary2}};
    }

    /// A lane in the shape of a "]". First center line point is at the bottom
    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    RightSideCounterClockwiseSemiCircle()
    {
        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_points{
            {units::length::meter_t(0.0), units::length::meter_t(-2.0), units::length::meter_t(0.0)},
            {units::length::meter_t(2.0), units::length::meter_t(-2.0), units::length::meter_t(0.0)},
            {units::length::meter_t(2.0), units::length::meter_t(0.0), units::length::meter_t(0.0)},
            {units::length::meter_t(2.0), units::length::meter_t(2.0), units::length::meter_t(0.0)},
            {units::length::meter_t(0.0), units::length::meter_t(2.0), units::length::meter_t(0.0)}};

        environment::map::LaneBoundary::Points right_boundary_points{
            {{units::length::meter_t(0.0), units::length::meter_t(-3.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(3.0), units::length::meter_t(-3.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(3.0), units::length::meter_t(3.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(0.0), units::length::meter_t(3.0), units::length::meter_t(0.0)}, 0, 0}};

        environment::map::LaneBoundary::Points left_boundary_points{
            {{units::length::meter_t(0.0), units::length::meter_t(-1.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(1.0), units::length::meter_t(-1.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(1.0), units::length::meter_t(1.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(0.0), units::length::meter_t(1.0), units::length::meter_t(0.0)}, 0, 0}};

        auto right_boundary1 = BuildWhiteLineBoundary(right_boundary_points);
        auto left_boundary1 = BuildWhiteLineBoundary(left_boundary_points);
        auto lane = BuildLaneWithBoundaries(center_line_points, {left_boundary1.id}, {right_boundary1.id});

        return {lane, {left_boundary1, right_boundary1}};
    }

    /// A lane in the shape of a "[". First center line point is at the top
    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    LeftSideCounterClockwiseSemiCircle()
    {
        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_points{
            {units::length::meter_t(0.0), units::length::meter_t(2.0), units::length::meter_t(0.0)},
            {units::length::meter_t(-2.0), units::length::meter_t(2.0), units::length::meter_t(0.0)},
            {units::length::meter_t(-2.0), units::length::meter_t(0.0), units::length::meter_t(0.0)},
            {units::length::meter_t(-2.0), units::length::meter_t(-2.0), units::length::meter_t(0.0)},
            {units::length::meter_t(0.0), units::length::meter_t(-2.0), units::length::meter_t(0.0)}};

        environment::map::LaneBoundary::Points right_boundary_points{
            {{units::length::meter_t(0.0), units::length::meter_t(-3.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(-3.0), units::length::meter_t(-3.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(-3.0), units::length::meter_t(3.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(0.0), units::length::meter_t(3.0), units::length::meter_t(0.0)}, 0, 0}};

        environment::map::LaneBoundary::Points left_boundary_points{
            {{units::length::meter_t(0.0), units::length::meter_t(-1.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(-1.0), units::length::meter_t(-1.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(-1.0), units::length::meter_t(1.0), units::length::meter_t(0.0)}, 0, 0},
            {{units::length::meter_t(0.0), units::length::meter_t(1.0), units::length::meter_t(0.0)}, 0, 0}};

        auto right_boundary1 = BuildWhiteLineBoundary(right_boundary_points);
        auto left_boundary1 = BuildWhiteLineBoundary(left_boundary_points);
        auto lane = BuildLaneWithBoundaries(center_line_points, {left_boundary1.id}, {right_boundary1.id});

        return {lane, {left_boundary1, right_boundary1}};
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>> MapOdrProblematicCurve()
    {
        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line = {
            {units::length::meter_t(-649.346), units::length::meter_t(2333.28), units::length::meter_t(0)},
            {units::length::meter_t(-661.375), units::length::meter_t(2326.91), units::length::meter_t(0)},
            {units::length::meter_t(-664.207), units::length::meter_t(2325.39), units::length::meter_t(0)},
            {units::length::meter_t(-666.327), units::length::meter_t(2324.24), units::length::meter_t(0)},
            {units::length::meter_t(-668.791), units::length::meter_t(2322.86), units::length::meter_t(0)},
            {units::length::meter_t(-671.242), units::length::meter_t(2321.45), units::length::meter_t(0)},
            {units::length::meter_t(-673.328), units::length::meter_t(2320.2), units::length::meter_t(0)},
            {units::length::meter_t(-675.397), units::length::meter_t(2318.92), units::length::meter_t(0)},
            {units::length::meter_t(-677.444), units::length::meter_t(2317.6), units::length::meter_t(0)},
            {units::length::meter_t(-679.467), units::length::meter_t(2316.23), units::length::meter_t(0)},
            {units::length::meter_t(-681.461), units::length::meter_t(2314.82), units::length::meter_t(0)},
            {units::length::meter_t(-683.42), units::length::meter_t(2313.35), units::length::meter_t(0)},
            {units::length::meter_t(-685.341), units::length::meter_t(2311.82), units::length::meter_t(0)},
            {units::length::meter_t(-687.217), units::length::meter_t(2310.24), units::length::meter_t(0)},
            {units::length::meter_t(-689.045), units::length::meter_t(2308.6), units::length::meter_t(0)},
            {units::length::meter_t(-690.532), units::length::meter_t(2307.19), units::length::meter_t(0)},
            {units::length::meter_t(-692.27), units::length::meter_t(2305.46), units::length::meter_t(0)},
            {units::length::meter_t(-693.957), units::length::meter_t(2303.68), units::length::meter_t(0)},
            {units::length::meter_t(-694.78), units::length::meter_t(2302.77), units::length::meter_t(0)},
            {units::length::meter_t(-695.857), units::length::meter_t(2301.54), units::length::meter_t(0)},
            {units::length::meter_t(-697.427), units::length::meter_t(2299.65), units::length::meter_t(0)},
            {units::length::meter_t(-698.942), units::length::meter_t(2297.72), units::length::meter_t(0)},
            {units::length::meter_t(-699.677), units::length::meter_t(2296.74), units::length::meter_t(0)},
            {units::length::meter_t(-700.636), units::length::meter_t(2295.41), units::length::meter_t(0)},
            {units::length::meter_t(-702.025), units::length::meter_t(2293.39), units::length::meter_t(0)},
            {units::length::meter_t(-703.354), units::length::meter_t(2291.32), units::length::meter_t(0)},
            {units::length::meter_t(-704.621), units::length::meter_t(2289.22), units::length::meter_t(0)},
            {units::length::meter_t(-705.827), units::length::meter_t(2287.08), units::length::meter_t(0)},
            {units::length::meter_t(-706.97), units::length::meter_t(2284.91), units::length::meter_t(0)},
            {units::length::meter_t(-707.696), units::length::meter_t(2283.45), units::length::meter_t(0)},
            {units::length::meter_t(-708.222), units::length::meter_t(2282.34), units::length::meter_t(0)},
            {units::length::meter_t(-708.898), units::length::meter_t(2280.85), units::length::meter_t(0)},
            {units::length::meter_t(-709.386), units::length::meter_t(2279.72), units::length::meter_t(0)},
            {units::length::meter_t(-710.311), units::length::meter_t(2277.45), units::length::meter_t(0)},
            {units::length::meter_t(-711.17), units::length::meter_t(2275.15), units::length::meter_t(0)},
            {units::length::meter_t(-711.961), units::length::meter_t(2272.82), units::length::meter_t(0)},
            {units::length::meter_t(-712.684), units::length::meter_t(2270.48), units::length::meter_t(0)},
            {units::length::meter_t(-713.019), units::length::meter_t(2269.3), units::length::meter_t(0)},
            {units::length::meter_t(-713.44), units::length::meter_t(2267.72), units::length::meter_t(0)},
            {units::length::meter_t(-714.012), units::length::meter_t(2265.33), units::length::meter_t(0)},
            {units::length::meter_t(-714.515), units::length::meter_t(2262.93), units::length::meter_t(0)},
            {units::length::meter_t(-714.811), units::length::meter_t(2261.32), units::length::meter_t(0)},
            {units::length::meter_t(-715.012), units::length::meter_t(2260.11), units::length::meter_t(0)},
            {units::length::meter_t(-715.361), units::length::meter_t(2257.68), units::length::meter_t(0)},
            {units::length::meter_t(-715.509), units::length::meter_t(2256.46), units::length::meter_t(0)},
            {units::length::meter_t(-715.678), units::length::meter_t(2254.83), units::length::meter_t(0)},
            {units::length::meter_t(-715.872), units::length::meter_t(2252.39), units::length::meter_t(0)},
            {units::length::meter_t(-715.995), units::length::meter_t(2249.94), units::length::meter_t(0)},
            {units::length::meter_t(-716.037), units::length::meter_t(2248.3), units::length::meter_t(0)},
            {units::length::meter_t(-716.047), units::length::meter_t(2247.07), units::length::meter_t(0)},
            {units::length::meter_t(-716.014), units::length::meter_t(2244.62), units::length::meter_t(0)},
            {units::length::meter_t(-715.952), units::length::meter_t(2242.98), units::length::meter_t(0)},
            {units::length::meter_t(-715.885), units::length::meter_t(2241.76), units::length::meter_t(0)},
            {units::length::meter_t(-715.8), units::length::meter_t(2240.53), units::length::meter_t(0)},
            {units::length::meter_t(-715.658), units::length::meter_t(2238.9), units::length::meter_t(0)},
            {units::length::meter_t(-715.386), units::length::meter_t(2236.46), units::length::meter_t(0)},
            {units::length::meter_t(-715.165), units::length::meter_t(2234.84), units::length::meter_t(0)},
            {units::length::meter_t(-714.979), units::length::meter_t(2233.63), units::length::meter_t(0)},
            {units::length::meter_t(-714.553), units::length::meter_t(2231.21), units::length::meter_t(0)},
            {units::length::meter_t(-714.056), units::length::meter_t(2228.81), units::length::meter_t(0)},
            {units::length::meter_t(-713.489), units::length::meter_t(2226.42), units::length::meter_t(0)},
            {units::length::meter_t(-712.853), units::length::meter_t(2224.05), units::length::meter_t(0)},
            {units::length::meter_t(-712.147), units::length::meter_t(2221.7), units::length::meter_t(0)},
            {units::length::meter_t(-711.373), units::length::meter_t(2219.37), units::length::meter_t(0)},
            {units::length::meter_t(-710.531), units::length::meter_t(2217.07), units::length::meter_t(0)},
            {units::length::meter_t(-709.626), units::length::meter_t(2214.79), units::length::meter_t(0)},
            {units::length::meter_t(-708.664), units::length::meter_t(2212.54), units::length::meter_t(0)},
            {units::length::meter_t(-707.651), units::length::meter_t(2210.32), units::length::meter_t(0)},
            {units::length::meter_t(-706.412), units::length::meter_t(2207.76), units::length::meter_t(0)},
            {units::length::meter_t(-705.121), units::length::meter_t(2205.23), units::length::meter_t(0)},
            {units::length::meter_t(-703.785), units::length::meter_t(2202.73), units::length::meter_t(0)},
            {units::length::meter_t(-702.414), units::length::meter_t(2200.26), units::length::meter_t(0)},
            {units::length::meter_t(-700.814), units::length::meter_t(2197.46), units::length::meter_t(0)},
            {units::length::meter_t(-698.986), units::length::meter_t(2194.34), units::length::meter_t(0)},
            {units::length::meter_t(-696.119), units::length::meter_t(2189.52), units::length::meter_t(0)},
        };
        std::vector<mantle_api::Vec3<units::length::meter_t>> left_boundary_474 = {
            {units::length::meter_t(-648.493), units::length::meter_t(2331.67), units::length::meter_t(0)},
            {units::length::meter_t(-660.576), units::length::meter_t(2325.27), units::length::meter_t(0)},
            {units::length::meter_t(-665.152), units::length::meter_t(2322.8), units::length::meter_t(0)},
            {units::length::meter_t(-667.599), units::length::meter_t(2321.44), units::length::meter_t(0)},
            {units::length::meter_t(-670.027), units::length::meter_t(2320.05), units::length::meter_t(0)},
            {units::length::meter_t(-672.091), units::length::meter_t(2318.82), units::length::meter_t(0)},
            {units::length::meter_t(-674.134), units::length::meter_t(2317.56), units::length::meter_t(0)},
            {units::length::meter_t(-676.154), units::length::meter_t(2316.26), units::length::meter_t(0)},
            {units::length::meter_t(-678.146), units::length::meter_t(2314.93), units::length::meter_t(0)},
            {units::length::meter_t(-680.107), units::length::meter_t(2313.54), units::length::meter_t(0)},
            {units::length::meter_t(-682.032), units::length::meter_t(2312.11), units::length::meter_t(0)},
            {units::length::meter_t(-683.917), units::length::meter_t(2310.62), units::length::meter_t(0)},
            {units::length::meter_t(-685.758), units::length::meter_t(2309.08), units::length::meter_t(0)},
            {units::length::meter_t(-687.257), units::length::meter_t(2307.76), units::length::meter_t(0)},
            {units::length::meter_t(-689.012), units::length::meter_t(2306.12), units::length::meter_t(0)},
            {units::length::meter_t(-690.719), units::length::meter_t(2304.44), units::length::meter_t(0)},
            {units::length::meter_t(-692.375), units::length::meter_t(2302.7), units::length::meter_t(0)},
            {units::length::meter_t(-693.184), units::length::meter_t(2301.81), units::length::meter_t(0)},
            {units::length::meter_t(-694.243), units::length::meter_t(2300.61), units::length::meter_t(0)},
            {units::length::meter_t(-695.786), units::length::meter_t(2298.78), units::length::meter_t(0)},
            {units::length::meter_t(-697.275), units::length::meter_t(2296.89), units::length::meter_t(0)},
            {units::length::meter_t(-698.708), units::length::meter_t(2294.97), units::length::meter_t(0)},
            {units::length::meter_t(-699.632), units::length::meter_t(2293.66), units::length::meter_t(0)},
            {units::length::meter_t(-700.308), units::length::meter_t(2292.67), units::length::meter_t(0)},
            {units::length::meter_t(-701.616), units::length::meter_t(2290.66), units::length::meter_t(0)},
            {units::length::meter_t(-702.865), units::length::meter_t(2288.61), units::length::meter_t(0)},
            {units::length::meter_t(-704.053), units::length::meter_t(2286.52), units::length::meter_t(0)},
            {units::length::meter_t(-705.18), units::length::meter_t(2284.41), units::length::meter_t(0)},
            {units::length::meter_t(-705.719), units::length::meter_t(2283.33), units::length::meter_t(0)},
            {units::length::meter_t(-706.415), units::length::meter_t(2281.89), units::length::meter_t(0)},
            {units::length::meter_t(-706.918), units::length::meter_t(2280.8), units::length::meter_t(0)},
            {units::length::meter_t(-707.564), units::length::meter_t(2279.34), units::length::meter_t(0)},
            {units::length::meter_t(-708.479), units::length::meter_t(2277.12), units::length::meter_t(0)},
            {units::length::meter_t(-709.053), units::length::meter_t(2275.63), units::length::meter_t(0)},
            {units::length::meter_t(-709.464), units::length::meter_t(2274.5), units::length::meter_t(0)},
            {units::length::meter_t(-710.236), units::length::meter_t(2272.23), units::length::meter_t(0)},
            {units::length::meter_t(-710.597), units::length::meter_t(2271.08), units::length::meter_t(0)},
            {units::length::meter_t(-711.053), units::length::meter_t(2269.55), units::length::meter_t(0)},
            {units::length::meter_t(-711.68), units::length::meter_t(2267.23), units::length::meter_t(0)},
            {units::length::meter_t(-711.968), units::length::meter_t(2266.07), units::length::meter_t(0)},
            {units::length::meter_t(-712.325), units::length::meter_t(2264.51), units::length::meter_t(0)},
            {units::length::meter_t(-712.804), units::length::meter_t(2262.16), units::length::meter_t(0)},
            {units::length::meter_t(-713.214), units::length::meter_t(2259.79), units::length::meter_t(0)},
            {units::length::meter_t(-713.449), units::length::meter_t(2258.21), units::length::meter_t(0)},
            {units::length::meter_t(-713.604), units::length::meter_t(2257.02), units::length::meter_t(0)},
            {units::length::meter_t(-713.864), units::length::meter_t(2254.63), units::length::meter_t(0)},
            {units::length::meter_t(-714.053), units::length::meter_t(2252.24), units::length::meter_t(0)},
            {units::length::meter_t(-714.172), units::length::meter_t(2249.84), units::length::meter_t(0)},
            {units::length::meter_t(-714.205), units::length::meter_t(2248.65), units::length::meter_t(0)},
            {units::length::meter_t(-714.222), units::length::meter_t(2247.05), units::length::meter_t(0)},
            {units::length::meter_t(-714.189), units::length::meter_t(2244.65), units::length::meter_t(0)},
            {units::length::meter_t(-714.147), units::length::meter_t(2243.45), units::length::meter_t(0)},
            {units::length::meter_t(-714.062), units::length::meter_t(2241.85), units::length::meter_t(0)},
            {units::length::meter_t(-713.877), units::length::meter_t(2239.46), units::length::meter_t(0)},
            {units::length::meter_t(-713.715), units::length::meter_t(2237.86), units::length::meter_t(0)},
            {units::length::meter_t(-713.573), units::length::meter_t(2236.67), units::length::meter_t(0)},
            {units::length::meter_t(-713.414), units::length::meter_t(2235.48), units::length::meter_t(0)},
            {units::length::meter_t(-713.174), units::length::meter_t(2233.9), units::length::meter_t(0)},
            {units::length::meter_t(-712.757), units::length::meter_t(2231.54), units::length::meter_t(0)},
            {units::length::meter_t(-712.271), units::length::meter_t(2229.19), units::length::meter_t(0)},
            {units::length::meter_t(-711.716), units::length::meter_t(2226.85), units::length::meter_t(0)},
            {units::length::meter_t(-711.093), units::length::meter_t(2224.54), units::length::meter_t(0)},
            {units::length::meter_t(-710.403), units::length::meter_t(2222.24), units::length::meter_t(0)},
            {units::length::meter_t(-709.645), units::length::meter_t(2219.96), units::length::meter_t(0)},
            {units::length::meter_t(-708.822), units::length::meter_t(2217.71), units::length::meter_t(0)},
            {units::length::meter_t(-707.935), units::length::meter_t(2215.48), units::length::meter_t(0)},
            {units::length::meter_t(-706.991), units::length::meter_t(2213.27), units::length::meter_t(0)},
            {units::length::meter_t(-705.995), units::length::meter_t(2211.09), units::length::meter_t(0)},
            {units::length::meter_t(-704.775), units::length::meter_t(2208.56), units::length::meter_t(0)},
            {units::length::meter_t(-703.501), units::length::meter_t(2206.07), units::length::meter_t(0)},
            {units::length::meter_t(-702.18), units::length::meter_t(2203.6), units::length::meter_t(0)},
            {units::length::meter_t(-700.625), units::length::meter_t(2200.81), units::length::meter_t(0)},
            {units::length::meter_t(-699.032), units::length::meter_t(2198.03), units::length::meter_t(0)},
            {units::length::meter_t(-697.21), units::length::meter_t(2194.93), units::length::meter_t(0)},
            {units::length::meter_t(-694.552), units::length::meter_t(2190.46), units::length::meter_t(0)},
        };
        std::vector<mantle_api::Vec3<units::length::meter_t>> left_boundary_584 = {
            {units::length::meter_t(-694.552), units::length::meter_t(2190.46), units::length::meter_t(0)},
            {units::length::meter_t(-697.211), units::length::meter_t(2194.93), units::length::meter_t(0)},
            {units::length::meter_t(-699.034), units::length::meter_t(2198.03), units::length::meter_t(0)},
            {units::length::meter_t(-700.628), units::length::meter_t(2200.81), units::length::meter_t(0)},
            {units::length::meter_t(-702.183), units::length::meter_t(2203.61), units::length::meter_t(0)},
            {units::length::meter_t(-703.504), units::length::meter_t(2206.08), units::length::meter_t(0)},
            {units::length::meter_t(-704.779), units::length::meter_t(2208.57), units::length::meter_t(0)},
            {units::length::meter_t(-705.999), units::length::meter_t(2211.09), units::length::meter_t(0)},
            {units::length::meter_t(-706.995), units::length::meter_t(2213.28), units::length::meter_t(0)},
            {units::length::meter_t(-707.94), units::length::meter_t(2215.49), units::length::meter_t(0)},
            {units::length::meter_t(-708.826), units::length::meter_t(2217.72), units::length::meter_t(0)},
            {units::length::meter_t(-709.65), units::length::meter_t(2219.97), units::length::meter_t(0)},
            {units::length::meter_t(-710.407), units::length::meter_t(2222.25), units::length::meter_t(0)},
            {units::length::meter_t(-711.097), units::length::meter_t(2224.55), units::length::meter_t(0)},
            {units::length::meter_t(-711.72), units::length::meter_t(2226.87), units::length::meter_t(0)},
            {units::length::meter_t(-712.274), units::length::meter_t(2229.2), units::length::meter_t(0)},
            {units::length::meter_t(-712.76), units::length::meter_t(2231.56), units::length::meter_t(0)},
            {units::length::meter_t(-713.177), units::length::meter_t(2233.92), units::length::meter_t(0)},
            {units::length::meter_t(-713.36), units::length::meter_t(2235.11), units::length::meter_t(0)},
            {units::length::meter_t(-713.576), units::length::meter_t(2236.69), units::length::meter_t(0)},
            {units::length::meter_t(-713.842), units::length::meter_t(2239.08), units::length::meter_t(0)},
            {units::length::meter_t(-713.949), units::length::meter_t(2240.27), units::length::meter_t(0)},
            {units::length::meter_t(-714.064), units::length::meter_t(2241.87), units::length::meter_t(0)},
            {units::length::meter_t(-714.13), units::length::meter_t(2243.07), units::length::meter_t(0)},
            {units::length::meter_t(-714.19), units::length::meter_t(2244.67), units::length::meter_t(0)},
            {units::length::meter_t(-714.222), units::length::meter_t(2247.07), units::length::meter_t(0)},
            {units::length::meter_t(-714.212), units::length::meter_t(2248.27), units::length::meter_t(0)},
            {units::length::meter_t(-714.171), units::length::meter_t(2249.87), units::length::meter_t(0)},
            {units::length::meter_t(-714.051), units::length::meter_t(2252.27), units::length::meter_t(0)},
            {units::length::meter_t(-713.861), units::length::meter_t(2254.66), units::length::meter_t(0)},
            {units::length::meter_t(-713.695), units::length::meter_t(2256.25), units::length::meter_t(0)},
            {units::length::meter_t(-713.551), units::length::meter_t(2257.45), units::length::meter_t(0)},
            {units::length::meter_t(-713.209), units::length::meter_t(2259.82), units::length::meter_t(0)},
            {units::length::meter_t(-713.012), units::length::meter_t(2261.01), units::length::meter_t(0)},
            {units::length::meter_t(-712.723), units::length::meter_t(2262.58), units::length::meter_t(0)},
            {units::length::meter_t(-712.232), units::length::meter_t(2264.93), units::length::meter_t(0)},
            {units::length::meter_t(-711.672), units::length::meter_t(2267.27), units::length::meter_t(0)},
            {units::length::meter_t(-711.26), units::length::meter_t(2268.81), units::length::meter_t(0)},
            {units::length::meter_t(-710.932), units::length::meter_t(2269.97), units::length::meter_t(0)},
            {units::length::meter_t(-710.225), units::length::meter_t(2272.26), units::length::meter_t(0)},
            {units::length::meter_t(-709.451), units::length::meter_t(2274.53), units::length::meter_t(0)},
            {units::length::meter_t(-708.611), units::length::meter_t(2276.78), units::length::meter_t(0)},
            {units::length::meter_t(-708.015), units::length::meter_t(2278.27), units::length::meter_t(0)},
            {units::length::meter_t(-707.549), units::length::meter_t(2279.38), units::length::meter_t(0)},
            {units::length::meter_t(-706.568), units::length::meter_t(2281.57), units::length::meter_t(0)},
            {units::length::meter_t(-706.053), units::length::meter_t(2282.65), units::length::meter_t(0)},
            {units::length::meter_t(-705.343), units::length::meter_t(2284.09), units::length::meter_t(0)},
            {units::length::meter_t(-704.225), units::length::meter_t(2286.21), units::length::meter_t(0)},
            {units::length::meter_t(-703.045), units::length::meter_t(2288.3), units::length::meter_t(0)},
            {units::length::meter_t(-701.805), units::length::meter_t(2290.36), units::length::meter_t(0)},
            {units::length::meter_t(-700.505), units::length::meter_t(2292.38), units::length::meter_t(0)},
            {units::length::meter_t(-699.146), units::length::meter_t(2294.36), units::length::meter_t(0)},
            {units::length::meter_t(-698.209), units::length::meter_t(2295.65), units::length::meter_t(0)},
            {units::length::meter_t(-697.489), units::length::meter_t(2296.61), units::length::meter_t(0)},
            {units::length::meter_t(-696.008), units::length::meter_t(2298.5), units::length::meter_t(0)},
            {units::length::meter_t(-694.472), units::length::meter_t(2300.35), units::length::meter_t(0)},
            {units::length::meter_t(-693.418), units::length::meter_t(2301.55), units::length::meter_t(0)},
            {units::length::meter_t(-692.612), units::length::meter_t(2302.44), units::length::meter_t(0)},
            {units::length::meter_t(-690.963), units::length::meter_t(2304.19), units::length::meter_t(0)},
            {units::length::meter_t(-690.119), units::length::meter_t(2305.04), units::length::meter_t(0)},
            {units::length::meter_t(-688.974), units::length::meter_t(2306.16), units::length::meter_t(0)},
            {units::length::meter_t(-687.513), units::length::meter_t(2307.53), units::length::meter_t(0)},
            {units::length::meter_t(-685.717), units::length::meter_t(2309.12), units::length::meter_t(0)},
            {units::length::meter_t(-684.185), units::length::meter_t(2310.41), units::length::meter_t(0)},
            {units::length::meter_t(-682.305), units::length::meter_t(2311.9), units::length::meter_t(0)},
            {units::length::meter_t(-680.385), units::length::meter_t(2313.34), units::length::meter_t(0)},
            {units::length::meter_t(-678.428), units::length::meter_t(2314.73), units::length::meter_t(0)},
            {units::length::meter_t(-676.439), units::length::meter_t(2316.08), units::length::meter_t(0)},
            {units::length::meter_t(-674.422), units::length::meter_t(2317.38), units::length::meter_t(0)},
            {units::length::meter_t(-672.381), units::length::meter_t(2318.64), units::length::meter_t(0)},
            {units::length::meter_t(-670.319), units::length::meter_t(2319.87), units::length::meter_t(0)},
            {units::length::meter_t(-667.892), units::length::meter_t(2321.27), units::length::meter_t(0)},
            {units::length::meter_t(-665.446), units::length::meter_t(2322.64), units::length::meter_t(0)},
            {units::length::meter_t(-662.986), units::length::meter_t(2323.98), units::length::meter_t(0)},
            {units::length::meter_t(-660.517), units::length::meter_t(2325.3), units::length::meter_t(0)},
            {units::length::meter_t(-648.493), units::length::meter_t(2331.67), units::length::meter_t(0)},
        };
        std::vector<mantle_api::Vec3<units::length::meter_t>> right_boundary_470 = {
            {units::length::meter_t(-650.199), units::length::meter_t(2334.89), units::length::meter_t(0)},
            {units::length::meter_t(-662.292), units::length::meter_t(2328.49), units::length::meter_t(0)},
            {units::length::meter_t(-665.133), units::length::meter_t(2326.97), units::length::meter_t(0)},
            {units::length::meter_t(-667.265), units::length::meter_t(2325.8), units::length::meter_t(0)},
            {units::length::meter_t(-669.747), units::length::meter_t(2324.42), units::length::meter_t(0)},
            {units::length::meter_t(-672.22), units::length::meter_t(2322.99), units::length::meter_t(0)},
            {units::length::meter_t(-674.329), units::length::meter_t(2321.73), units::length::meter_t(0)},
            {units::length::meter_t(-676.424), units::length::meter_t(2320.43), units::length::meter_t(0)},
            {units::length::meter_t(-678.501), units::length::meter_t(2319.09), units::length::meter_t(0)},
            {units::length::meter_t(-680.556), units::length::meter_t(2317.7), units::length::meter_t(0)},
            {units::length::meter_t(-682.584), units::length::meter_t(2316.26), units::length::meter_t(0)},
            {units::length::meter_t(-684.582), units::length::meter_t(2314.75), units::length::meter_t(0)},
            {units::length::meter_t(-686.542), units::length::meter_t(2313.19), units::length::meter_t(0)},
            {units::length::meter_t(-688.457), units::length::meter_t(2311.58), units::length::meter_t(0)},
            {units::length::meter_t(-690.323), units::length::meter_t(2309.9), units::length::meter_t(0)},
            {units::length::meter_t(-691.84), units::length::meter_t(2308.47), units::length::meter_t(0)},
            {units::length::meter_t(-692.734), units::length::meter_t(2307.59), units::length::meter_t(0)},
            {units::length::meter_t(-693.905), units::length::meter_t(2306.4), units::length::meter_t(0)},
            {units::length::meter_t(-695.618), units::length::meter_t(2304.57), units::length::meter_t(0)},
            {units::length::meter_t(-697.276), units::length::meter_t(2302.69), units::length::meter_t(0)},
            {units::length::meter_t(-698.879), units::length::meter_t(2300.76), units::length::meter_t(0)},
            {units::length::meter_t(-700.424), units::length::meter_t(2298.78), units::length::meter_t(0)},
            {units::length::meter_t(-701.911), units::length::meter_t(2296.77), units::length::meter_t(0)},
            {units::length::meter_t(-703.338), units::length::meter_t(2294.71), units::length::meter_t(0)},
            {units::length::meter_t(-704.029), units::length::meter_t(2293.66), units::length::meter_t(0)},
            {units::length::meter_t(-704.926), units::length::meter_t(2292.25), units::length::meter_t(0)},
            {units::length::meter_t(-706.22), units::length::meter_t(2290.1), units::length::meter_t(0)},
            {units::length::meter_t(-706.844), units::length::meter_t(2289.02), units::length::meter_t(0)},
            {units::length::meter_t(-707.65), units::length::meter_t(2287.55), units::length::meter_t(0)},
            {units::length::meter_t(-708.805), units::length::meter_t(2285.33), units::length::meter_t(0)},
            {units::length::meter_t(-709.894), units::length::meter_t(2283.07), units::length::meter_t(0)},
            {units::length::meter_t(-710.918), units::length::meter_t(2280.78), units::length::meter_t(0)},
            {units::length::meter_t(-711.404), units::length::meter_t(2279.63), units::length::meter_t(0)},
            {units::length::meter_t(-712.026), units::length::meter_t(2278.07), units::length::meter_t(0)},
            {units::length::meter_t(-712.902), units::length::meter_t(2275.73), units::length::meter_t(0)},
            {units::length::meter_t(-713.709), units::length::meter_t(2273.35), units::length::meter_t(0)},
            {units::length::meter_t(-714.446), units::length::meter_t(2270.96), units::length::meter_t(0)},
            {units::length::meter_t(-715.112), units::length::meter_t(2268.54), units::length::meter_t(0)},
            {units::length::meter_t(-715.419), units::length::meter_t(2267.32), units::length::meter_t(0)},
            {units::length::meter_t(-715.8), units::length::meter_t(2265.7), units::length::meter_t(0)},
            {units::length::meter_t(-716.313), units::length::meter_t(2263.24), units::length::meter_t(0)},
            {units::length::meter_t(-716.614), units::length::meter_t(2261.6), units::length::meter_t(0)},
            {units::length::meter_t(-716.819), units::length::meter_t(2260.36), units::length::meter_t(0)},
            {units::length::meter_t(-717.175), units::length::meter_t(2257.88), units::length::meter_t(0)},
            {units::length::meter_t(-717.325), units::length::meter_t(2256.64), units::length::meter_t(0)},
            {units::length::meter_t(-717.498), units::length::meter_t(2254.98), units::length::meter_t(0)},
            {units::length::meter_t(-717.695), units::length::meter_t(2252.48), units::length::meter_t(0)},
            {units::length::meter_t(-717.82), units::length::meter_t(2249.97), units::length::meter_t(0)},
            {units::length::meter_t(-717.855), units::length::meter_t(2248.72), units::length::meter_t(0)},
            {units::length::meter_t(-717.872), units::length::meter_t(2247.05), units::length::meter_t(0)},
            {units::length::meter_t(-717.864), units::length::meter_t(2245.8), units::length::meter_t(0)},
            {units::length::meter_t(-717.825), units::length::meter_t(2244.12), units::length::meter_t(0)},
            {units::length::meter_t(-717.705), units::length::meter_t(2241.62), units::length::meter_t(0)},
            {units::length::meter_t(-717.512), units::length::meter_t(2239.12), units::length::meter_t(0)},
            {units::length::meter_t(-717.388), units::length::meter_t(2237.87), units::length::meter_t(0)},
            {units::length::meter_t(-717.194), units::length::meter_t(2236.21), units::length::meter_t(0)},
            {units::length::meter_t(-716.843), units::length::meter_t(2233.73), units::length::meter_t(0)},
            {units::length::meter_t(-716.419), units::length::meter_t(2231.26), units::length::meter_t(0)},
            {units::length::meter_t(-715.923), units::length::meter_t(2228.8), units::length::meter_t(0)},
            {units::length::meter_t(-715.356), units::length::meter_t(2226.36), units::length::meter_t(0)},
            {units::length::meter_t(-714.717), units::length::meter_t(2223.94), units::length::meter_t(0)},
            {units::length::meter_t(-714.012), units::length::meter_t(2221.55), units::length::meter_t(0)},
            {units::length::meter_t(-713.233), units::length::meter_t(2219.17), units::length::meter_t(0)},
            {units::length::meter_t(-712.384), units::length::meter_t(2216.81), units::length::meter_t(0)},
            {units::length::meter_t(-711.471), units::length::meter_t(2214.48), units::length::meter_t(0)},
            {units::length::meter_t(-710.5), units::length::meter_t(2212.18), units::length::meter_t(0)},
            {units::length::meter_t(-709.478), units::length::meter_t(2209.92), units::length::meter_t(0)},
            {units::length::meter_t(-708.229), units::length::meter_t(2207.31), units::length::meter_t(0)},
            {units::length::meter_t(-706.927), units::length::meter_t(2204.75), units::length::meter_t(0)},
            {units::length::meter_t(-705.583), units::length::meter_t(2202.22), units::length::meter_t(0)},
            {units::length::meter_t(-704.004), units::length::meter_t(2199.37), units::length::meter_t(0)},
            {units::length::meter_t(-702.393), units::length::meter_t(2196.55), units::length::meter_t(0)},
            {units::length::meter_t(-700.557), units::length::meter_t(2193.42), units::length::meter_t(0)},
            {units::length::meter_t(-697.687), units::length::meter_t(2188.59), units::length::meter_t(0)},
        };

        auto right_boundary1 = BuildWhiteLineBoundary(right_boundary_470);

        auto left_boundary1 = BuildWhiteLineBoundary(left_boundary_474);
        auto left_boundary2 = BuildWhiteLineBoundary(left_boundary_584);

        auto lane = BuildLaneWithBoundaries(center_line, {left_boundary1.id, left_boundary2.id}, {right_boundary1.id});

        return {lane, {left_boundary1, left_boundary2, right_boundary1}};
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    MapOdrProblematicStraightLine()
    {
        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line = {
            {units::length::meter_t(-696.119), units::length::meter_t(2189.52), units::length::meter_t(0)},
            {units::length::meter_t(-691.001), units::length::meter_t(2180.93), units::length::meter_t(0)},
        };

        std::vector<mantle_api::Vec3<units::length::meter_t>> right_boundary_462 = {
            {units::length::meter_t(-697.687), units::length::meter_t(2188.59), units::length::meter_t(0)},
            {units::length::meter_t(-692.569), units::length::meter_t(2180), units::length::meter_t(0)}};
        auto right_boundary1 = BuildWhiteLineBoundary(right_boundary_462);

        std::vector<mantle_api::Vec3<units::length::meter_t>> left_boundary_466 = {
            {units::length::meter_t(-694.552), units::length::meter_t(2190.46), units::length::meter_t(0)},
            {units::length::meter_t(-689.433), units::length::meter_t(2181.87), units::length::meter_t(0)}};
        std::vector<mantle_api::Vec3<units::length::meter_t>> left_boundary_582 = {
            {units::length::meter_t(-689.433), units::length::meter_t(2181.87), units::length::meter_t(0)},
            {units::length::meter_t(-694.552), units::length::meter_t(2190.46), units::length::meter_t(0)}};
        auto left_boundary1 = BuildWhiteLineBoundary(left_boundary_466);
        auto left_boundary2 = BuildWhiteLineBoundary(left_boundary_582);

        auto lane = BuildLaneWithBoundaries(center_line, {left_boundary1.id, left_boundary2.id}, {right_boundary1.id});

        return {lane, {left_boundary1, left_boundary2, right_boundary1}};
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>> MapOdrProblematicCurvyLane()
    {
        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line = {
            {units::length::meter_t(-446.287), units::length::meter_t(2049.81), units::length::meter_t(0)},
            {units::length::meter_t(-445.505), units::length::meter_t(2054.15), units::length::meter_t(0)},
            {units::length::meter_t(-444.959), units::length::meter_t(2057.33), units::length::meter_t(0)},
            {units::length::meter_t(-444.52), units::length::meter_t(2060.12), units::length::meter_t(0)},
            {units::length::meter_t(-444.126), units::length::meter_t(2062.92), units::length::meter_t(0)},
            {units::length::meter_t(-443.779), units::length::meter_t(2065.73), units::length::meter_t(0)},
            {units::length::meter_t(-443.478), units::length::meter_t(2068.55), units::length::meter_t(0)},
            {units::length::meter_t(-443.223), units::length::meter_t(2071.37), units::length::meter_t(0)},
            {units::length::meter_t(-442.989), units::length::meter_t(2074.59), units::length::meter_t(0)},
            {units::length::meter_t(-442.873), units::length::meter_t(2076.61), units::length::meter_t(0)},
            {units::length::meter_t(-442.766), units::length::meter_t(2079.03), units::length::meter_t(0)},
            {units::length::meter_t(-442.693), units::length::meter_t(2081.46), units::length::meter_t(0)},
            {units::length::meter_t(-442.654), units::length::meter_t(2083.88), units::length::meter_t(0)},
            {units::length::meter_t(-442.65), units::length::meter_t(2087.91), units::length::meter_t(0)},
            {units::length::meter_t(-442.749), units::length::meter_t(2100.31), units::length::meter_t(0)},
            {units::length::meter_t(-442.732), units::length::meter_t(2103.88), units::length::meter_t(0)},
            {units::length::meter_t(-442.685), units::length::meter_t(2105.84), units::length::meter_t(0)},
            {units::length::meter_t(-442.62), units::length::meter_t(2107.41), units::length::meter_t(0)},
            {units::length::meter_t(-442.529), units::length::meter_t(2108.98), units::length::meter_t(0)},
            {units::length::meter_t(-442.41), units::length::meter_t(2110.54), units::length::meter_t(0)},
            {units::length::meter_t(-442.179), units::length::meter_t(2112.88), units::length::meter_t(0)},
            {units::length::meter_t(-441.939), units::length::meter_t(2114.83), units::length::meter_t(0)},
            {units::length::meter_t(-441.655), units::length::meter_t(2116.77), units::length::meter_t(0)},
            {units::length::meter_t(-441.329), units::length::meter_t(2118.7), units::length::meter_t(0)},
            {units::length::meter_t(-440.96), units::length::meter_t(2120.62), units::length::meter_t(0)},
            {units::length::meter_t(-440.548), units::length::meter_t(2122.54), units::length::meter_t(0)},
            {units::length::meter_t(-440.094), units::length::meter_t(2124.45), units::length::meter_t(0)},
            {units::length::meter_t(-439.597), units::length::meter_t(2126.34), units::length::meter_t(0)},
            {units::length::meter_t(-439.17), units::length::meter_t(2127.85), units::length::meter_t(0)},
            {units::length::meter_t(-438.715), units::length::meter_t(2129.35), units::length::meter_t(0)},
            {units::length::meter_t(-438.357), units::length::meter_t(2130.47), units::length::meter_t(0)},
            {units::length::meter_t(-437.856), units::length::meter_t(2131.96), units::length::meter_t(0)},
            {units::length::meter_t(-437.329), units::length::meter_t(2133.43), units::length::meter_t(0)},
            {units::length::meter_t(-436.776), units::length::meter_t(2134.9), units::length::meter_t(0)},
            {units::length::meter_t(-436.197), units::length::meter_t(2136.36), units::length::meter_t(0)},
            {units::length::meter_t(-435.592), units::length::meter_t(2137.8), units::length::meter_t(0)},
            {units::length::meter_t(-434.961), units::length::meter_t(2139.24), units::length::meter_t(0)},
            {units::length::meter_t(-434.305), units::length::meter_t(2140.66), units::length::meter_t(0)},
            {units::length::meter_t(-433.624), units::length::meter_t(2142.07), units::length::meter_t(0)},
            {units::length::meter_t(-432.917), units::length::meter_t(2143.47), units::length::meter_t(0)},
            {units::length::meter_t(-432.186), units::length::meter_t(2144.86), units::length::meter_t(0)},
            {units::length::meter_t(-431.431), units::length::meter_t(2146.23), units::length::meter_t(0)},
            {units::length::meter_t(-430.848), units::length::meter_t(2147.26), units::length::meter_t(0)},
            {units::length::meter_t(-430.05), units::length::meter_t(2148.61), units::length::meter_t(0)},
            {units::length::meter_t(-429.228), units::length::meter_t(2149.94), units::length::meter_t(0)},
            {units::length::meter_t(-428.596), units::length::meter_t(2150.93), units::length::meter_t(0)},
            {units::length::meter_t(-427.951), units::length::meter_t(2151.91), units::length::meter_t(0)},
            {units::length::meter_t(-427.071), units::length::meter_t(2153.21), units::length::meter_t(0)},
            {units::length::meter_t(-426.395), units::length::meter_t(2154.17), units::length::meter_t(0)},
            {units::length::meter_t(-425.475), units::length::meter_t(2155.44), units::length::meter_t(0)},
            {units::length::meter_t(-424.532), units::length::meter_t(2156.7), units::length::meter_t(0)},
            {units::length::meter_t(-423.811), units::length::meter_t(2157.63), units::length::meter_t(0)},
            {units::length::meter_t(-422.078), units::length::meter_t(2159.76), units::length::meter_t(0)},
            {units::length::meter_t(-420.285), units::length::meter_t(2161.87), units::length::meter_t(0)},
            {units::length::meter_t(-418.451), units::length::meter_t(2163.96), units::length::meter_t(0)},
            {units::length::meter_t(-413.407), units::length::meter_t(2169.64), units::length::meter_t(0)},
        };
        std::vector<mantle_api::Vec3<units::length::meter_t>> left_boundary_442 = {
            {units::length::meter_t(-448.083), units::length::meter_t(2050.14), units::length::meter_t(0)},
            {units::length::meter_t(-447.298), units::length::meter_t(2054.49), units::length::meter_t(0)},
            {units::length::meter_t(-446.756), units::length::meter_t(2057.64), units::length::meter_t(0)},
            {units::length::meter_t(-446.322), units::length::meter_t(2060.41), units::length::meter_t(0)},
            {units::length::meter_t(-445.986), units::length::meter_t(2062.79), units::length::meter_t(0)},
            {units::length::meter_t(-445.59), units::length::meter_t(2065.96), units::length::meter_t(0)},
            {units::length::meter_t(-445.292), units::length::meter_t(2068.75), units::length::meter_t(0)},
            {units::length::meter_t(-445.008), units::length::meter_t(2071.93), units::length::meter_t(0)},
            {units::length::meter_t(-444.809), units::length::meter_t(2074.73), units::length::meter_t(0)},
            {units::length::meter_t(-444.695), units::length::meter_t(2076.72), units::length::meter_t(0)},
            {units::length::meter_t(-444.589), units::length::meter_t(2079.12), units::length::meter_t(0)},
            {units::length::meter_t(-444.517), units::length::meter_t(2081.52), units::length::meter_t(0)},
            {units::length::meter_t(-444.479), units::length::meter_t(2083.92), units::length::meter_t(0)},
            {units::length::meter_t(-444.475), units::length::meter_t(2087.92), units::length::meter_t(0)},
            {units::length::meter_t(-444.574), units::length::meter_t(2100.32), units::length::meter_t(0)},
            {units::length::meter_t(-444.556), units::length::meter_t(2103.92), units::length::meter_t(0)},
            {units::length::meter_t(-444.508), units::length::meter_t(2105.92), units::length::meter_t(0)},
            {units::length::meter_t(-444.443), units::length::meter_t(2107.52), units::length::meter_t(0)},
            {units::length::meter_t(-444.321), units::length::meter_t(2109.51), units::length::meter_t(0)},
            {units::length::meter_t(-444.192), units::length::meter_t(2111.11), units::length::meter_t(0)},
            {units::length::meter_t(-443.991), units::length::meter_t(2113.1), units::length::meter_t(0)},
            {units::length::meter_t(-443.746), units::length::meter_t(2115.08), units::length::meter_t(0)},
            {units::length::meter_t(-443.456), units::length::meter_t(2117.06), units::length::meter_t(0)},
            {units::length::meter_t(-443.123), units::length::meter_t(2119.03), units::length::meter_t(0)},
            {units::length::meter_t(-442.746), units::length::meter_t(2121), units::length::meter_t(0)},
            {units::length::meter_t(-442.326), units::length::meter_t(2122.95), units::length::meter_t(0)},
            {units::length::meter_t(-441.862), units::length::meter_t(2124.9), units::length::meter_t(0)},
            {units::length::meter_t(-441.355), units::length::meter_t(2126.83), units::length::meter_t(0)},
            {units::length::meter_t(-440.919), units::length::meter_t(2128.37), units::length::meter_t(0)},
            {units::length::meter_t(-440.455), units::length::meter_t(2129.9), units::length::meter_t(0)},
            {units::length::meter_t(-439.964), units::length::meter_t(2131.43), units::length::meter_t(0)},
            {units::length::meter_t(-439.578), units::length::meter_t(2132.56), units::length::meter_t(0)},
            {units::length::meter_t(-439.04), units::length::meter_t(2134.07), units::length::meter_t(0)},
            {units::length::meter_t(-438.475), units::length::meter_t(2135.57), units::length::meter_t(0)},
            {units::length::meter_t(-437.884), units::length::meter_t(2137.05), units::length::meter_t(0)},
            {units::length::meter_t(-437.266), units::length::meter_t(2138.53), units::length::meter_t(0)},
            {units::length::meter_t(-436.623), units::length::meter_t(2139.99), units::length::meter_t(0)},
            {units::length::meter_t(-435.953), units::length::meter_t(2141.45), units::length::meter_t(0)},
            {units::length::meter_t(-435.258), units::length::meter_t(2142.89), units::length::meter_t(0)},
            {units::length::meter_t(-434.719), units::length::meter_t(2143.96), units::length::meter_t(0)},
            {units::length::meter_t(-433.979), units::length::meter_t(2145.38), units::length::meter_t(0)},
            {units::length::meter_t(-433.214), units::length::meter_t(2146.78), units::length::meter_t(0)},
            {units::length::meter_t(-432.425), units::length::meter_t(2148.17), units::length::meter_t(0)},
            {units::length::meter_t(-431.61), units::length::meter_t(2149.55), units::length::meter_t(0)},
            {units::length::meter_t(-430.983), units::length::meter_t(2150.58), units::length::meter_t(0)},
            {units::length::meter_t(-430.126), units::length::meter_t(2151.93), units::length::meter_t(0)},
            {units::length::meter_t(-429.468), units::length::meter_t(2152.93), units::length::meter_t(0)},
            {units::length::meter_t(-428.57), units::length::meter_t(2154.25), units::length::meter_t(0)},
            {units::length::meter_t(-427.648), units::length::meter_t(2155.56), units::length::meter_t(0)},
            {units::length::meter_t(-426.941), units::length::meter_t(2156.53), units::length::meter_t(0)},
            {units::length::meter_t(-425.979), units::length::meter_t(2157.81), units::length::meter_t(0)},
            {units::length::meter_t(-425.243), units::length::meter_t(2158.76), units::length::meter_t(0)},
            {units::length::meter_t(-423.478), units::length::meter_t(2160.93), units::length::meter_t(0)},
            {units::length::meter_t(-421.663), units::length::meter_t(2163.06), units::length::meter_t(0)},
            {units::length::meter_t(-419.817), units::length::meter_t(2165.17), units::length::meter_t(0)},
            {units::length::meter_t(-414.773), units::length::meter_t(2170.85), units::length::meter_t(0)},
        };
        std::vector<mantle_api::Vec3<units::length::meter_t>> left_boundary_576 = {
            {units::length::meter_t(-415.436), units::length::meter_t(2170.1), units::length::meter_t(0)},
            {units::length::meter_t(-419.958), units::length::meter_t(2165.01), units::length::meter_t(0)},
            {units::length::meter_t(-421.805), units::length::meter_t(2162.9), units::length::meter_t(0)},
            {units::length::meter_t(-423.62), units::length::meter_t(2160.76), units::length::meter_t(0)},
            {units::length::meter_t(-425.382), units::length::meter_t(2158.58), units::length::meter_t(0)},
            {units::length::meter_t(-426.36), units::length::meter_t(2157.31), units::length::meter_t(0)},
            {units::length::meter_t(-427.078), units::length::meter_t(2156.34), units::length::meter_t(0)},
            {units::length::meter_t(-427.784), units::length::meter_t(2155.37), units::length::meter_t(0)},
            {units::length::meter_t(-428.704), units::length::meter_t(2154.06), units::length::meter_t(0)},
            {units::length::meter_t(-429.378), units::length::meter_t(2153.06), units::length::meter_t(0)},
            {units::length::meter_t(-430.257), units::length::meter_t(2151.72), units::length::meter_t(0)},
            {units::length::meter_t(-430.9), units::length::meter_t(2150.71), units::length::meter_t(0)},
            {units::length::meter_t(-431.737), units::length::meter_t(2149.34), units::length::meter_t(0)},
            {units::length::meter_t(-432.549), units::length::meter_t(2147.96), units::length::meter_t(0)},
            {units::length::meter_t(-433.336), units::length::meter_t(2146.56), units::length::meter_t(0)},
            {units::length::meter_t(-434.099), units::length::meter_t(2145.15), units::length::meter_t(0)},
            {units::length::meter_t(-434.836), units::length::meter_t(2143.73), units::length::meter_t(0)},
            {units::length::meter_t(-435.547), units::length::meter_t(2142.29), units::length::meter_t(0)},
            {units::length::meter_t(-436.233), units::length::meter_t(2140.85), units::length::meter_t(0)},
            {units::length::meter_t(-436.731), units::length::meter_t(2139.75), units::length::meter_t(0)},
            {units::length::meter_t(-437.371), units::length::meter_t(2138.28), units::length::meter_t(0)},
            {units::length::meter_t(-437.835), units::length::meter_t(2137.17), units::length::meter_t(0)},
            {units::length::meter_t(-438.429), units::length::meter_t(2135.68), units::length::meter_t(0)},
            {units::length::meter_t(-438.997), units::length::meter_t(2134.19), units::length::meter_t(0)},
            {units::length::meter_t(-439.538), units::length::meter_t(2132.68), units::length::meter_t(0)},
            {units::length::meter_t(-440.052), units::length::meter_t(2131.16), units::length::meter_t(0)},
            {units::length::meter_t(-440.539), units::length::meter_t(2129.63), units::length::meter_t(0)},
            {units::length::meter_t(-440.999), units::length::meter_t(2128.1), units::length::meter_t(0)},
            {units::length::meter_t(-441.431), units::length::meter_t(2126.55), units::length::meter_t(0)},
            {units::length::meter_t(-441.932), units::length::meter_t(2124.61), units::length::meter_t(0)},
            {units::length::meter_t(-442.391), units::length::meter_t(2122.66), units::length::meter_t(0)},
            {units::length::meter_t(-442.806), units::length::meter_t(2120.7), units::length::meter_t(0)},
            {units::length::meter_t(-443.177), units::length::meter_t(2118.73), units::length::meter_t(0)},
            {units::length::meter_t(-443.504), units::length::meter_t(2116.76), units::length::meter_t(0)},
            {units::length::meter_t(-443.787), units::length::meter_t(2114.77), units::length::meter_t(0)},
            {units::length::meter_t(-444.026), units::length::meter_t(2112.79), units::length::meter_t(0)},
            {units::length::meter_t(-444.22), units::length::meter_t(2110.79), units::length::meter_t(0)},
            {units::length::meter_t(-444.344), units::length::meter_t(2109.19), units::length::meter_t(0)},
            {units::length::meter_t(-444.439), units::length::meter_t(2107.59), units::length::meter_t(0)},
            {units::length::meter_t(-444.506), units::length::meter_t(2105.99), units::length::meter_t(0)},
            {units::length::meter_t(-444.555), units::length::meter_t(2103.99), units::length::meter_t(0)},
            {units::length::meter_t(-444.574), units::length::meter_t(2100.38), units::length::meter_t(0)},
            {units::length::meter_t(-444.475), units::length::meter_t(2087.96), units::length::meter_t(0)},
            {units::length::meter_t(-444.478), units::length::meter_t(2083.96), units::length::meter_t(0)},
            {units::length::meter_t(-444.516), units::length::meter_t(2081.55), units::length::meter_t(0)},
            {units::length::meter_t(-444.588), units::length::meter_t(2079.15), units::length::meter_t(0)},
            {units::length::meter_t(-444.674), units::length::meter_t(2077.15), units::length::meter_t(0)},
            {units::length::meter_t(-444.808), units::length::meter_t(2074.75), units::length::meter_t(0)},
            {units::length::meter_t(-445.007), units::length::meter_t(2071.95), units::length::meter_t(0)},
            {units::length::meter_t(-445.291), units::length::meter_t(2068.76), units::length::meter_t(0)},
            {units::length::meter_t(-445.589), units::length::meter_t(2065.97), units::length::meter_t(0)},
            {units::length::meter_t(-445.933), units::length::meter_t(2063.18), units::length::meter_t(0)},
            {units::length::meter_t(-446.323), units::length::meter_t(2060.41), units::length::meter_t(0)},
            {units::length::meter_t(-446.758), units::length::meter_t(2057.63), units::length::meter_t(0)},
            {units::length::meter_t(-447.3), units::length::meter_t(2054.48), units::length::meter_t(0)},
            {units::length::meter_t(-448.083), units::length::meter_t(2050.14), units::length::meter_t(0)},
        };
        std::vector<mantle_api::Vec3<units::length::meter_t>> right_boundary_438 = {
            {units::length::meter_t(-444.491), units::length::meter_t(2049.49), units::length::meter_t(0)},
            {units::length::meter_t(-443.704), units::length::meter_t(2053.85), units::length::meter_t(0)},
            {units::length::meter_t(-443.155), units::length::meter_t(2057.05), units::length::meter_t(0)},
            {units::length::meter_t(-442.712), units::length::meter_t(2059.87), units::length::meter_t(0)},
            {units::length::meter_t(-442.314), units::length::meter_t(2062.7), units::length::meter_t(0)},
            {units::length::meter_t(-441.964), units::length::meter_t(2065.54), units::length::meter_t(0)},
            {units::length::meter_t(-441.66), units::length::meter_t(2068.39), units::length::meter_t(0)},
            {units::length::meter_t(-441.37), units::length::meter_t(2071.64), units::length::meter_t(0)},
            {units::length::meter_t(-441.166), units::length::meter_t(2074.5), units::length::meter_t(0)},
            {units::length::meter_t(-441.05), units::length::meter_t(2076.54), units::length::meter_t(0)},
            {units::length::meter_t(-440.941), units::length::meter_t(2078.98), units::length::meter_t(0)},
            {units::length::meter_t(-440.868), units::length::meter_t(2081.43), units::length::meter_t(0)},
            {units::length::meter_t(-440.829), units::length::meter_t(2083.88), units::length::meter_t(0)},
            {units::length::meter_t(-440.825), units::length::meter_t(2087.94), units::length::meter_t(0)},
            {units::length::meter_t(-440.924), units::length::meter_t(2100.32), units::length::meter_t(0)},
            {units::length::meter_t(-440.907), units::length::meter_t(2103.85), units::length::meter_t(0)},
            {units::length::meter_t(-440.861), units::length::meter_t(2105.79), units::length::meter_t(0)},
            {units::length::meter_t(-440.798), units::length::meter_t(2107.32), units::length::meter_t(0)},
            {units::length::meter_t(-440.708), units::length::meter_t(2108.86), units::length::meter_t(0)},
            {units::length::meter_t(-440.591), units::length::meter_t(2110.39), units::length::meter_t(0)},
            {units::length::meter_t(-440.365), units::length::meter_t(2112.68), units::length::meter_t(0)},
            {units::length::meter_t(-440.13), units::length::meter_t(2114.58), units::length::meter_t(0)},
            {units::length::meter_t(-439.853), units::length::meter_t(2116.48), units::length::meter_t(0)},
            {units::length::meter_t(-439.533), units::length::meter_t(2118.37), units::length::meter_t(0)},
            {units::length::meter_t(-439.172), units::length::meter_t(2120.26), units::length::meter_t(0)},
            {units::length::meter_t(-438.768), units::length::meter_t(2122.14), units::length::meter_t(0)},
            {units::length::meter_t(-438.323), units::length::meter_t(2124), units::length::meter_t(0)},
            {units::length::meter_t(-437.837), units::length::meter_t(2125.86), units::length::meter_t(0)},
            {units::length::meter_t(-437.418), units::length::meter_t(2127.34), units::length::meter_t(0)},
            {units::length::meter_t(-436.973), units::length::meter_t(2128.81), units::length::meter_t(0)},
            {units::length::meter_t(-436.502), units::length::meter_t(2130.27), units::length::meter_t(0)},
            {units::length::meter_t(-436.006), units::length::meter_t(2131.72), units::length::meter_t(0)},
            {units::length::meter_t(-435.616), units::length::meter_t(2132.8), units::length::meter_t(0)},
            {units::length::meter_t(-435.074), units::length::meter_t(2134.24), units::length::meter_t(0)},
            {units::length::meter_t(-434.507), units::length::meter_t(2135.67), units::length::meter_t(0)},
            {units::length::meter_t(-433.915), units::length::meter_t(2137.08), units::length::meter_t(0)},
            {units::length::meter_t(-433.454), units::length::meter_t(2138.14), units::length::meter_t(0)},
            {units::length::meter_t(-432.817), units::length::meter_t(2139.54), units::length::meter_t(0)},
            {units::length::meter_t(-432.156), units::length::meter_t(2140.92), units::length::meter_t(0)},
            {units::length::meter_t(-431.471), units::length::meter_t(2142.3), units::length::meter_t(0)},
            {units::length::meter_t(-430.761), units::length::meter_t(2143.66), units::length::meter_t(0)},
            {units::length::meter_t(-430.027), units::length::meter_t(2145.01), units::length::meter_t(0)},
            {units::length::meter_t(-429.269), units::length::meter_t(2146.34), units::length::meter_t(0)},
            {units::length::meter_t(-428.487), units::length::meter_t(2147.66), units::length::meter_t(0)},
            {units::length::meter_t(-427.886), units::length::meter_t(2148.64), units::length::meter_t(0)},
            {units::length::meter_t(-427.064), units::length::meter_t(2149.94), units::length::meter_t(0)},
            {units::length::meter_t(-426.219), units::length::meter_t(2151.22), units::length::meter_t(0)},
            {units::length::meter_t(-425.351), units::length::meter_t(2152.49), units::length::meter_t(0)},
            {units::length::meter_t(-424.461), units::length::meter_t(2153.74), units::length::meter_t(0)},
            {units::length::meter_t(-423.778), units::length::meter_t(2154.67), units::length::meter_t(0)},
            {units::length::meter_t(-422.85), units::length::meter_t(2155.89), units::length::meter_t(0)},
            {units::length::meter_t(-422.139), units::length::meter_t(2156.8), units::length::meter_t(0)},
            {units::length::meter_t(-420.676), units::length::meter_t(2158.59), units::length::meter_t(0)},
            {units::length::meter_t(-418.905), units::length::meter_t(2160.67), units::length::meter_t(0)},
            {units::length::meter_t(-417.084), units::length::meter_t(2162.75), units::length::meter_t(0)},
            {units::length::meter_t(-412.043), units::length::meter_t(2168.43), units::length::meter_t(0)},
        };

        auto right_boundary1 = BuildWhiteLineBoundary(right_boundary_438);
        auto left_boundary1 = BuildWhiteLineBoundary(left_boundary_442);
        auto left_boundary2 = BuildWhiteLineBoundary(left_boundary_576);

        auto lane = BuildLaneWithBoundaries(center_line, {left_boundary1.id, left_boundary2.id}, {right_boundary1.id});

        return {lane, {left_boundary1, left_boundary2, right_boundary1}};
    }

    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>
    LaneWithSpatiallyIdenticalDuplicatedBoundaries()
    {
        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line = {
            {units::length::meter_t(0), units::length::meter_t(0), units::length::meter_t(0)},
            {units::length::meter_t(6000), units::length::meter_t(0), units::length::meter_t(0)},
        };

        std::vector<mantle_api::Vec3<units::length::meter_t>> right_boundary_pts_1 = {
            {units::length::meter_t(0), units::length::meter_t(-2), units::length::meter_t(0)},
            {units::length::meter_t(6000), units::length::meter_t(-2), units::length::meter_t(0)}};
        std::vector<mantle_api::Vec3<units::length::meter_t>> right_boundary_pts_2 = {
            {units::length::meter_t(0), units::length::meter_t(-2), units::length::meter_t(0)},
            {units::length::meter_t(6000), units::length::meter_t(-2), units::length::meter_t(0)}};
        auto right_boundary1 = BuildWhiteLineBoundary(right_boundary_pts_1);
        auto right_boundary2 = BuildWhiteLineBoundary(right_boundary_pts_2);

        std::vector<mantle_api::Vec3<units::length::meter_t>> left_boundary_pts_1 = {
            {units::length::meter_t(0), units::length::meter_t(2), units::length::meter_t(0)},
            {units::length::meter_t(6000), units::length::meter_t(2), units::length::meter_t(0)}};
        std::vector<mantle_api::Vec3<units::length::meter_t>> left_boundary_pts_2 = {
            {units::length::meter_t(0), units::length::meter_t(2), units::length::meter_t(0)},
            {units::length::meter_t(6000), units::length::meter_t(2), units::length::meter_t(0)}};
        auto left_boundary1 = BuildWhiteLineBoundary(left_boundary_pts_1);
        auto left_boundary2 = BuildWhiteLineBoundary(left_boundary_pts_2);

        auto lane = BuildLaneWithBoundaries(
            center_line, {left_boundary1.id, left_boundary2.id}, {right_boundary1.id, right_boundary2.id});

        return {lane, {left_boundary1, left_boundary2, right_boundary1, right_boundary2}};
    }

    ///
    /// @brief Create  counter clockwise arc lane with one lef and right lane-boundary
    /// @param center Center of the arc
    /// @param radius Radius of the arc
    /// @param start_angle Starting angle of the arc
    /// @param end_angle Ending angle of the arc
    /// @param number_of_points Number of points
    ///
    static std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>> CounterClockwiseArcLane(
        const mantle_api::Vec3<units::length::meter_t> center,
        const units::length::meter_t radius,
        const units::angle::radian_t start_angle,
        const units::angle::radian_t end_angle,
        const std::size_t number_of_points)
    {
        assert(number_of_points >= 2 && "number_of_points must be greater than 2.");
        assert(end_angle > start_angle && "end_angle must be greater than start_angle.");

        const auto left_lane_boundary_radius = radius - units::length::meter_t(1.0);
        const auto right_lane_boundary_radius = radius + units::length::meter_t(1.0);

        const auto distance = (end_angle - start_angle) / static_cast<double>(number_of_points - 1);
        std::vector<units::angle::radian_t> angle_space(number_of_points);  // linear angle space
        std::generate(angle_space.begin(), angle_space.end(), [pos = start_angle - distance, &distance]() mutable {
            return pos += distance;
        });

        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_points{};
        environment::map::LaneBoundary::Points right_boundary_points{};
        environment::map::LaneBoundary::Points left_boundary_points{};

        const auto conver_to_cartesian = [](auto radius, auto angle) {
            return mantle_api::Vec3<units::length::meter_t>{
                radius * units::math::cos(angle), radius * units::math::sin(angle), units::length::meter_t{0.0}};
        };

        for (const auto& angle : angle_space)
        {
            center_line_points.push_back(conver_to_cartesian(radius, angle) + center);
            left_boundary_points.push_back({{conver_to_cartesian(left_lane_boundary_radius, angle) + center}, 0, 0});
            right_boundary_points.push_back({{conver_to_cartesian(right_lane_boundary_radius, angle) + center}, 0, 0});
        }

        const auto right_boundary1 = BuildWhiteLineBoundary(right_boundary_points);
        const auto left_boundary1 = BuildWhiteLineBoundary(left_boundary_points);
        const auto lane = BuildLaneWithBoundaries(center_line_points, {left_boundary1.id}, {right_boundary1.id});

        return {lane, {left_boundary1, right_boundary1}};
    }

    // This lane group was dumped from a ODR map
    static std::vector<std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>>
    ThreeParallelStraightLanes2km()
    {

        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_1 = {
            {units::length::meter_t(-3.25), units::length::meter_t(-9.99999), units::length::meter_t(0)},
            {units::length::meter_t(-3.25735), units::length::meter_t(1990.0), units::length::meter_t(0)},
        };

        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_2 = {
            {units::length::meter_t(0.25), units::length::meter_t(-9.99998), units::length::meter_t(0)},
            {units::length::meter_t(0.242654), units::length::meter_t(1990.0), units::length::meter_t(0)},
        };
        std::vector<mantle_api::Vec3<units::length::meter_t>> center_line_3 = {
            {units::length::meter_t(3.75), units::length::meter_t(-9.99997), units::length::meter_t(0)},
            {units::length::meter_t(3.74265), units::length::meter_t(1990.0), units::length::meter_t(0)},
        };

        std::vector<mantle_api::Vec3<units::length::meter_t>> lane_boundary_1_pts = {
            {units::length::meter_t(-5.0), units::length::meter_t(-10.0), units::length::meter_t(0)},
            {units::length::meter_t(-5.00735), units::length::meter_t(1990.0), units::length::meter_t(0)}};

        std::vector<mantle_api::Vec3<units::length::meter_t>> lane_boundary_2_pts = {
            {units::length::meter_t(-1.5), units::length::meter_t(-9.99999), units::length::meter_t(0)},
            {units::length::meter_t(-1.50735), units::length::meter_t(1990.0), units::length::meter_t(0)}};

        std::vector<mantle_api::Vec3<units::length::meter_t>> lane_boundary_3_pts = {
            {units::length::meter_t(2.0), units::length::meter_t(-9.99997), units::length::meter_t(0)},
            {units::length::meter_t(1.99265), units::length::meter_t(1990.0), units::length::meter_t(0)}};

        std::vector<mantle_api::Vec3<units::length::meter_t>> lane_boundary_4_pts = {
            {units::length::meter_t(5.5), units::length::meter_t(-9.99996), units::length::meter_t(0)},
            {units::length::meter_t(5.49265), units::length::meter_t(1990.0), units::length::meter_t(0)}};

        auto lane_boundary_1_left = BuildWhiteLineBoundary(lane_boundary_1_pts);
        auto lane_boundary_1_right = BuildWhiteLineBoundary(lane_boundary_2_pts);
        auto lane_1 = BuildLaneWithBoundaries(center_line_1, {lane_boundary_1_left.id}, {lane_boundary_1_right.id});

        auto lane_boundary_2_left = BuildWhiteLineBoundary(lane_boundary_2_pts);
        auto lane_boundary_2_right = BuildWhiteLineBoundary(lane_boundary_3_pts);
        auto lane_2 = BuildLaneWithBoundaries(center_line_2, {lane_boundary_2_left.id}, {lane_boundary_2_right.id});

        auto lane_boundary_3_left = BuildWhiteLineBoundary(lane_boundary_3_pts);
        auto lane_boundary_3_right = BuildWhiteLineBoundary(lane_boundary_4_pts);
        auto lane_3 = BuildLaneWithBoundaries(center_line_3, {lane_boundary_3_left.id}, {lane_boundary_3_right.id});

        std::vector<std::pair<environment::map::Lane, std::vector<environment::map::LaneBoundary>>> three_lanes{};

        three_lanes.push_back({lane_1, {lane_boundary_1_left, lane_boundary_1_right}});
        three_lanes.push_back({lane_2, {lane_boundary_2_left, lane_boundary_2_right}});
        three_lanes.push_back({lane_3, {lane_boundary_3_left, lane_boundary_3_right}});
        return three_lanes;
    }
};

}  // namespace gtgen::core::test_utils

#endif  // GTGEN_CORE_TESTS_TESTUTILS_MAPCATALOGUE_LANECATALOGUE_H
