/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/traffic_light_control_unit.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"

namespace gtgen::core::environment::controller
{

TrafficLightControlUnit::TrafficLightControlUnit(std::vector<mantle_api::TrafficLightPhase> light_phases, bool repeat)
    : state_machine_{std::move(light_phases), repeat}
{
}

std::unique_ptr<IControlUnit> TrafficLightControlUnit::Clone() const
{
    return std::make_unique<TrafficLightControlUnit>(*this);
}

void TrafficLightControlUnit::SetEntity(mantle_api::IEntity& entity)
{
    IAbstractControlUnit::SetEntity(entity);

    auto* traffic_light_entity = dynamic_cast<entities::TrafficLightEntity*>(&entity);
    if (traffic_light_entity != nullptr)
    {
        traffic_light_entity_ = traffic_light_entity;
    }
    else
    {
        throw EnvironmentException(
            "The Entity with ID [{}] was assigned to a Traffic Light Controller but does not represent a Traffic "
            "Light, and therefore cannot be controlled as one. Ensure that the Static Object with ID [{}] in the "
            "scenario file is setup correctly.",
            entity_->GetUniqueId(),
            entity_->GetUniqueId());
    }

    state_machine_.Start(traffic_light_entity_);
}

void TrafficLightControlUnit::StepControlUnit()
{
    auto local_controller_time = current_simulation_time_ - spawn_time_;

    state_machine_.Step(local_controller_time);
}

bool TrafficLightControlUnit::HasFinished() const
{
    return false;
}

}  // namespace gtgen::core::environment::controller
