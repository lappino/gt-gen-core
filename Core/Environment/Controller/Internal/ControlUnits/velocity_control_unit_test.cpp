/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/velocity_control_unit.h"

#include "Core/Environment/Entities/vehicle_entity.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::controller
{
using units::literals::operator""_mps_pow4;
using units::literals::operator""_mps_cu;
using units::literals::operator""_mps_sq;
using units::literals::operator""_mps;
using units::literals::operator""_kph;

std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> GetDeceleratingVelocitySpline()
{
    mantle_api::SplineSection<units::velocity::meters_per_second> spline_section1 = {
        mantle_api::Time{0}, mantle_api::Time{2'000}, {0_mps_pow4, 0_mps_cu, -2.5_mps_sq, 5_mps}};
    mantle_api::SplineSection<units::velocity::meters_per_second> spline_section2 = {
        mantle_api::Time{2'000}, mantle_api::Time{3'000}, {0_mps_pow4, 0_mps_cu, 0_mps_sq, 0_mps}};
    return {spline_section1, spline_section2};
}

std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> GetAcceleratingVelocitySpline()
{
    mantle_api::SplineSection<units::velocity::meters_per_second> spline_section1 = {
        mantle_api::Time{0}, mantle_api::Time{2'000}, {0_mps_pow4, 0_mps_cu, 2.5_mps_sq, 0_mps}};
    mantle_api::SplineSection<units::velocity::meters_per_second> spline_section2 = {
        mantle_api::Time{2'000}, mantle_api::Time{3'000}, {0_mps_pow4, 0_mps_cu, 0_mps_sq, 5_mps}};
    return {spline_section1, spline_section2};
}

TEST(VelocityControlUnitTest, GivenVelocityControlUnit_WhenClone_ThenCopyCreated)
{
    auto controller_ptr = std::make_unique<VelocityControlUnit>(
        std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>>{}, 0.0_mps);

    auto copy = controller_ptr->Clone();

    ASSERT_NE(nullptr, copy);
    EXPECT_NE(controller_ptr.get(), copy.get());
}

TEST(VelocityControlUnitTest, GivenNoVelocitySpline_WhenStep_ThenDefaultVelocityIsSet)
{
    VelocityControlUnit velocity_control_unit({}, 30.0_mps);

    ControllerDataExchangeContainer data_exchange_container{};
    velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    velocity_control_unit.Step(mantle_api::Time{0});

    EXPECT_EQ(30.0_mps, data_exchange_container.velocity_scalars.back());
}

TEST(VelocityControlUnitTest, GivenNoVelocitySpline_WhenStepTwice_ThenDefaultVelocityIsSet)
{
    VelocityControlUnit velocity_control_unit({}, 30.0_mps);

    ControllerDataExchangeContainer data_exchange_container{};
    velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    velocity_control_unit.Step(mantle_api::Time{0});
    velocity_control_unit.Step(mantle_api::Time{30});

    EXPECT_EQ(30.0_mps, data_exchange_container.velocity_scalars.back());
}

TEST(VelocityControlUnitTest, GivenVelocitySpline_WhenSimulationTimeAfterLastSpline_ThenFinished)
{
    mantle_api::SplineSection<units::velocity::meters_per_second> spline_section = {
        mantle_api::Time{100}, mantle_api::Time{1000}, {0.0_mps_pow4, 0.0_mps_cu, 0.0_mps_sq, 0.0_mps}};
    VelocityControlUnit velocity_control_unit({spline_section}, 0.0_mps);

    ControllerDataExchangeContainer data_exchange_container{};
    velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    EXPECT_FALSE(velocity_control_unit.HasFinished());

    velocity_control_unit.Step(mantle_api::Time{2000});
    EXPECT_FALSE(velocity_control_unit.HasFinished());

    velocity_control_unit.Step(mantle_api::Time{2100});
    EXPECT_FALSE(velocity_control_unit.HasFinished());

    velocity_control_unit.Step(mantle_api::Time{3000});
    EXPECT_FALSE(velocity_control_unit.HasFinished());

    velocity_control_unit.Step(mantle_api::Time{3001});
    EXPECT_TRUE(velocity_control_unit.HasFinished());
}

TEST(VelocityControlUnitTest, GivenVelocitySpline_WhenSimulationTimeOutsideOfSpline_ThenDefaultVelocityIsSet)
{
    mantle_api::SplineSection<units::velocity::meters_per_second> spline_section = {
        mantle_api::Time{100}, mantle_api::Time{1000}, {0.0_mps_pow4, 0.0_mps_cu, 0.0_mps_sq, 42_kph}};
    VelocityControlUnit velocity_control_unit({spline_section}, 0.0_mps);

    ControllerDataExchangeContainer data_exchange_container{};
    velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    velocity_control_unit.Step(mantle_api::Time{0});
    EXPECT_EQ(0.0_mps, data_exchange_container.velocity_scalars.back());

    velocity_control_unit.Step(mantle_api::Time{99});
    EXPECT_EQ(0.0_mps, data_exchange_container.velocity_scalars.back());

    velocity_control_unit.Step(mantle_api::Time{1001});
    EXPECT_EQ(0.0_mps, data_exchange_container.velocity_scalars.back());
}

TEST(VelocityControlUnitTest, GivenVelocitySpline_WhenSimulating100ms_Then100VelocitiesAreCalculated)
{
    const units::velocity::kilometers_per_hour_t expected_velocity{42};
    mantle_api::SplineSection<units::velocity::meters_per_second> spline_section = {
        mantle_api::Time{0}, mantle_api::Time{1000}, {0.0_mps_pow4, 0.0_mps_cu, 0.0_mps_sq, expected_velocity}};
    VelocityControlUnit velocity_control_unit({spline_section}, 0.0_mps);

    ControllerDataExchangeContainer data_exchange_container{};
    velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    velocity_control_unit.Step(mantle_api::Time{0});
    ASSERT_TRUE(data_exchange_container.velocity_scalars.size() == 1);
    EXPECT_EQ(expected_velocity, data_exchange_container.velocity_scalars.back());

    data_exchange_container.Reset();

    velocity_control_unit.Step(mantle_api::Time{100});
    ASSERT_TRUE(data_exchange_container.velocity_scalars.size() == 100)
        << "Size: " << data_exchange_container.velocity_scalars.size();
    EXPECT_EQ(expected_velocity, data_exchange_container.velocity_scalars.back());
}

TEST(VelocityControlUnitTest, GivenVelocitySpline_WhenSimulationTimeInsideOfSpline_ThenSplineVelocityIsSet)
{
    const units::velocity::kilometers_per_hour_t expected_velocity{42};
    mantle_api::SplineSection<units::velocity::meters_per_second> spline_section = {
        mantle_api::Time{100}, mantle_api::Time{1000}, {0.0_mps_pow4, 0.0_mps_cu, 0.0_mps_sq, expected_velocity}};
    VelocityControlUnit velocity_control_unit({spline_section}, 0.0_mps);

    ControllerDataExchangeContainer data_exchange_container{};
    velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    velocity_control_unit.Step(mantle_api::Time{0});
    EXPECT_EQ(0.0_mps, data_exchange_container.velocity_scalars.back());

    velocity_control_unit.Step(mantle_api::Time{100});
    EXPECT_EQ(expected_velocity, data_exchange_container.velocity_scalars.back());

    velocity_control_unit.Step(mantle_api::Time{500});
    EXPECT_EQ(expected_velocity, data_exchange_container.velocity_scalars.back());

    velocity_control_unit.Step(mantle_api::Time{1000});
    EXPECT_EQ(expected_velocity, data_exchange_container.velocity_scalars.back());
}

TEST(VelocityControlUnitTest, GivenNoVelocitySpline_WhenStep_ThenAccelerationIsZero)
{
    VelocityControlUnit velocity_control_unit({}, 30.0_mps);

    ControllerDataExchangeContainer data_exchange_container{};
    velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    velocity_control_unit.Step(mantle_api::Time{0});  // initial step

    EXPECT_EQ(0.0_mps_sq, data_exchange_container.acceleration_scalar);
}

TEST(VelocityControlUnitTest, GivenVelocitySpline_WhenSimulationTimeOutsideOfSpline_ThenAccelerationIsZero)
{
    mantle_api::SplineSection<units::velocity::meters_per_second> spline_section = {
        mantle_api::Time{100}, mantle_api::Time{1000}, {0.0_mps_pow4, 0.0_mps_cu, 1.0_mps_sq, 0_mps}};
    VelocityControlUnit velocity_control_unit({spline_section}, 0.0_mps);

    ControllerDataExchangeContainer data_exchange_container{};
    velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    velocity_control_unit.Step(mantle_api::Time{0});
    EXPECT_EQ(0.0_mps_sq, data_exchange_container.acceleration_scalar);

    velocity_control_unit.Step(mantle_api::Time{99});
    EXPECT_EQ(0.0_mps_sq, data_exchange_container.acceleration_scalar);

    velocity_control_unit.Step(mantle_api::Time{1001});
    EXPECT_EQ(0.0_mps_sq, data_exchange_container.acceleration_scalar);
}

TEST(VelocityControlUnitTest, GivenVelocitySpline_WhenSimulationTimeInsideOfSpline_ThenAccelerationIsSet)
{
    mantle_api::SplineSection<units::velocity::meters_per_second> spline_section = {
        mantle_api::Time{100}, mantle_api::Time{1000}, {0.0_mps_pow4, 0.0_mps_cu, 1.0_mps_sq, 0_mps}};
    VelocityControlUnit velocity_control_unit({spline_section}, 0.0_mps);

    ControllerDataExchangeContainer data_exchange_container{};
    velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    velocity_control_unit.Step(mantle_api::Time{2000});
    EXPECT_EQ(0.0_mps_sq, data_exchange_container.acceleration_scalar);

    velocity_control_unit.Step(mantle_api::Time{2100});
    EXPECT_EQ(1.0_mps_sq, data_exchange_container.acceleration_scalar);

    velocity_control_unit.Step(mantle_api::Time{2500});
    EXPECT_EQ(1.0_mps_sq, data_exchange_container.acceleration_scalar);

    velocity_control_unit.Step(mantle_api::Time{3000});
    EXPECT_EQ(1.0_mps_sq, data_exchange_container.acceleration_scalar);
}

TEST(VelocityControlUnitTest, GivenDeceleratingVelocitySpline_WhenVelocityReachesZero_ThenAccelerationIsAlsoZero)
{
    VelocityControlUnit velocity_control_unit(GetDeceleratingVelocitySpline(), 5.0_mps);

    ControllerDataExchangeContainer data_exchange_container{};
    velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    // Initial should be 5m/s
    velocity_control_unit.Step(mantle_api::Time{0});
    EXPECT_EQ(5.0_mps, data_exchange_container.velocity_scalars.back());
    EXPECT_EQ(-2.5_mps_sq, data_exchange_container.acceleration_scalar);

    // And at 0m/s after 2 seconds
    velocity_control_unit.Step(mantle_api::Time{2'000});
    EXPECT_EQ(0.0_mps, data_exchange_container.velocity_scalars.back());
    EXPECT_EQ(0.0_mps_sq, data_exchange_container.acceleration_scalar);

    // Then remain at 0m/s with no deceleration
    velocity_control_unit.Step(mantle_api::Time{3'000});
    EXPECT_EQ(0.0_mps, data_exchange_container.velocity_scalars.back());
    EXPECT_EQ(0.0_mps_sq, data_exchange_container.acceleration_scalar);
}

TEST(VelocityControlUnitTest,
     GivenDeceleratingVelocitySpline_WhenDecelerating_ThenAccelerationIsConstantAndNoVelocitySpikes)
{
    VelocityControlUnit velocity_control_unit(GetDeceleratingVelocitySpline(), 5.0_mps);

    ControllerDataExchangeContainer data_exchange_container{};
    velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    velocity_control_unit.Step(mantle_api::Time{0});

    std::int64_t step_size = 1;
    std::int64_t target_time = 2000;
    const double velocity_change_per_step = -0.0025;
    const double starting_velocity = 5.0;

    for (std::int64_t step = step_size; step < target_time; step += step_size)
    {
        // Constant velocity decrease and no change to acceleration
        velocity_control_unit.Step(mantle_api::Time{static_cast<double>(step)});
        EXPECT_EQ(-2.5_mps_sq, data_exchange_container.acceleration_scalar);

        const double expected_new_velocity = starting_velocity + (velocity_change_per_step * static_cast<double>(step));
        EXPECT_NEAR(expected_new_velocity, data_exchange_container.velocity_scalars.back()(), 1E-7);
    }
}

TEST(VelocityControlUnitTest, GivenAcceleratingVelocitySpline_WhenVelocityReachesTarget_ThenAccelerationIsZero)
{
    VelocityControlUnit velocity_control_unit(GetAcceleratingVelocitySpline(), 0.0_mps);

    ControllerDataExchangeContainer data_exchange_container{};
    velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    // Initial should be 0m/s
    velocity_control_unit.Step(mantle_api::Time{0});
    EXPECT_EQ(0.0_mps, data_exchange_container.velocity_scalars.back());
    EXPECT_EQ(2.5_mps_sq, data_exchange_container.acceleration_scalar);

    // And at 5m/s after 2 seconds
    velocity_control_unit.Step(mantle_api::Time{2'000});
    EXPECT_EQ(5.0_mps, data_exchange_container.velocity_scalars.back());
    EXPECT_EQ(0_mps_sq, data_exchange_container.acceleration_scalar);

    // Then remain at 5m/s with no acceleration
    velocity_control_unit.Step(mantle_api::Time{3'000});
    EXPECT_EQ(5.0_mps, data_exchange_container.velocity_scalars.back());
    EXPECT_EQ(0.0_mps_sq, data_exchange_container.acceleration_scalar);
}

TEST(VelocityControlUnitTest,
     GivenAcceleratingVelocitySpline_WhenAccelerating_ThenAccelerationIsConstantAndNoVelocitySpikes)
{
    VelocityControlUnit velocity_control_unit(GetAcceleratingVelocitySpline(), 0.0_mps);

    ControllerDataExchangeContainer data_exchange_container{};
    velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    velocity_control_unit.Step(mantle_api::Time{0});

    std::int64_t step_size = 1;
    std::int64_t target_time = 2000;
    double velocity_change_per_step = 0.0025;
    double starting_velocity = 0.0;
    for (std::int64_t step = step_size; step < target_time; step += step_size)
    {
        // Constant velocity increase and no change to acceleration
        velocity_control_unit.Step(mantle_api::Time{static_cast<double>(step)});
        EXPECT_EQ(2.5_mps_sq, data_exchange_container.acceleration_scalar);

        const double expected_new_velocity = starting_velocity + (velocity_change_per_step * static_cast<double>(step));
        EXPECT_NEAR(expected_new_velocity, data_exchange_container.velocity_scalars.back()(), 1E-7);
    }
}

TEST(VelocityControlUnitTest, GivenEntitySpawningNotAtTimeZero_WhenGetVelocity_ThenCorrectSplineValueIsUsed)
{
    VelocityControlUnit velocity_control_unit(GetAcceleratingVelocitySpline(), 0.0_mps);
    ControllerDataExchangeContainer data_exchange_container{};
    velocity_control_unit.SetControllerDataExchangeContainer(data_exchange_container);

    velocity_control_unit.Step(mantle_api::Time{1000});

    EXPECT_EQ(2.5_mps_sq, data_exchange_container.acceleration_scalar);
    EXPECT_EQ(0.0_mps, data_exchange_container.velocity_scalars.back());
}

}  // namespace gtgen::core::environment::controller
