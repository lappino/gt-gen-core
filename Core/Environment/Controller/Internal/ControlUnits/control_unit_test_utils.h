/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_CONTROLUNITTESTUTILS_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_CONTROLUNITTESTUTILS_H

#include "Core/Environment/Controller/Internal/ControlUnits/i_control_unit.h"

namespace gtgen::core::environment::controller
{
template <typename C>
bool ContainsController(std::vector<std::unique_ptr<IControlUnit>>& control_units)
{
    auto contains_controller = [](const auto& control_unit) { return dynamic_cast<C*>(control_unit.get()) != nullptr; };
    return std::any_of(control_units.cbegin(), control_units.cend(), contains_controller);
}
}  // namespace gtgen::core::environment::controller
#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_CONTROLUNITS_CONTROLUNITTESTUTILS_H
