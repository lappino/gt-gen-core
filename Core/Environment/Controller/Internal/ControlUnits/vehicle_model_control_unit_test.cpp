/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/ControlUnits/vehicle_model_control_unit.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Common/vector.h>
#include <glm/glm.hpp>
#include <gtest/gtest.h>

namespace gtgen::core::environment::controller
{
using units::literals::operator""_m;
using units::literals::operator""_mps;
using units::literals::operator""_deg;
using units::literals::operator""_rad;
using units::literals::operator""_mps_sq;
using units::literals::operator""_s;
using units::literals::operator""_rad_per_s;
using units::literals::operator""_rad_per_s_sq;

class VehicleModelControlUnitTest : public testing::Test
{
  protected:
    void SetUp() override
    {
        auto properties = std::make_unique<mantle_api::VehicleProperties>();
        properties->front_axle.bb_center_to_axle_center = {1.5_m, 0_m, 0_m};
        properties->rear_axle.bb_center_to_axle_center = {-1.5_m, 0_m, 0_m};
        properties->bounding_box.dimension.width = 2_m;
        properties->bounding_box.dimension.length = 4_m;
        properties->bounding_box.dimension.height = 2_m;
        entity_->SetProperties(std::move(properties));
    }
    host::HostVehicleModel GetVehicleModelWithFilledVMO(std::int64_t timestamp,
                                                        mantle_api::Vec3<units::length::meter_t> position)
    {
        host::HostVehicleModel vehicle_model;
        host::VehicleModelOut vmo{position,
                                  expected_orientation_,
                                  expected_velocity_,
                                  expected_acceleration_,
                                  expected_orientation_rate_,
                                  expected_indicator_state_,
                                  mantle_api::Time{static_cast<double>(timestamp)}};

        vehicle_model.vehicle_model_out = vmo;

        return vehicle_model;
    }

    void AddFrictionPatchToMap(double mue)
    {
        map::Polygon3d shape_3d{
            {{-10_m, -10_m, 0_m}, {10_m, -10_m, 0_m}, {10_m, 10_m, 0_m}, {-10_m, 10_m, 0_m}, {-10_m, -10_m, 0_m}}};
        auto friction_patch = std::make_shared<environment::map::FixedFrictionPatch>(42, shape_3d, mue);
        gtgen_map_.patches.push_back(friction_patch);
    }

    mantle_api::Vec3<units::velocity::meters_per_second_t> expected_velocity_{1_mps, 2_mps, 3_mps};
    mantle_api::Orientation3<units::angle::radian_t> expected_orientation_{90.0_deg, 0_rad, 0_rad};
    mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> expected_acceleration_{10_mps_sq,
                                                                                              11_mps_sq,
                                                                                              12_mps_sq};
    mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> expected_orientation_rate_{};
    mantle_api::IndicatorState expected_indicator_state_{mantle_api::IndicatorState::kLeft};

    map::GtGenMap gtgen_map_{};
    std::unique_ptr<entities::VehicleEntity> entity_{std::make_unique<entities::VehicleEntity>(0, "host")};
};

TEST_F(VehicleModelControlUnitTest, GivenVehicleModelWithSameTimeStampInVMO_WhenStepAtZeroTime_ThenEntityIsNotUpdated)
{
    mantle_api::Vec3<units::length::meter_t> ignored_position{4_m, 5_m, 6_m};
    auto vehicle_model = GetVehicleModelWithFilledVMO(-1, ignored_position);
    mantle_api::Vec3<units::length::meter_t> expected_position{42_m, 42_m, 42_m};
    entity_->SetPosition(expected_position);

    auto vehicle_model_control_unit = std::make_unique<VehicleModelControlUnit>(&vehicle_model, &gtgen_map_);
    vehicle_model_control_unit->SetEntity(*entity_);

    vehicle_model_control_unit->Step(mantle_api::Time{0});

    EXPECT_TRIPLE(expected_position, entity_->GetPosition())
}

TEST_F(VehicleModelControlUnitTest, GivenVehicleModelWithNewTimeStampInVMO_WhenStepAtZeroTime_ThenEntityIsUpdated)
{
    mantle_api::Vec3<units::length::meter_t> expected_position{4_m, 5_m, 6_m};
    auto vehicle_model = GetVehicleModelWithFilledVMO(0, expected_position);

    auto vehicle_model_control_unit = std::make_unique<VehicleModelControlUnit>(&vehicle_model, &gtgen_map_);
    vehicle_model_control_unit->SetEntity(*entity_);

    vehicle_model_control_unit->Step(mantle_api::Time{0});

    EXPECT_TRIPLE(expected_position, entity_->GetPosition())
    EXPECT_TRIPLE(expected_velocity_, entity_->GetVelocity())
    EXPECT_TRIPLE(expected_acceleration_, entity_->GetAcceleration())
    EXPECT_TRIPLE(expected_orientation_, entity_->GetOrientation())
    EXPECT_TRIPLE(expected_orientation_rate_, entity_->GetOrientationRate())
    EXPECT_TRIPLE(mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>{},
                  entity_->GetOrientationAcceleration())

    EXPECT_EQ(expected_indicator_state_, entity_->GetIndicatorState());
}

TEST_F(VehicleModelControlUnitTest, GivenVehicleModelWithOrientationChangeInVMO_WhenStepped_ThenEntityIsUpdated)
{
    /// Setup
    mantle_api::Vec3<units::length::meter_t> expected_position{4_m, 5_m, 6_m};
    auto vehicle_model = GetVehicleModelWithFilledVMO(0, expected_position);
    vehicle_model.vehicle_model_out.orientation.yaw = 0.0_rad;

    auto vehicle_model_control_unit = std::make_unique<VehicleModelControlUnit>(&vehicle_model, &gtgen_map_);
    vehicle_model_control_unit->SetEntity(*entity_);

    vehicle_model_control_unit->Step(mantle_api::Time{0});

    /// Step 1000ms and turn 90 degrees
    const units::angle::degree_t amount_to_turn = 90.0_deg;
    const int time_step = 1000;
    vehicle_model.vehicle_model_out.orientation.yaw = amount_to_turn;
    vehicle_model.vehicle_model_out.orientation_rate.yaw = amount_to_turn / 1_s;
    vehicle_model.vehicle_model_out.time_stamp = mantle_api::Time{time_step};

    vehicle_model_control_unit->Step(mantle_api::Time{time_step});

    mantle_api::Orientation3<units::angle::radian_t> expected_second_orientation_all{amount_to_turn, 0_rad, 0_rad};
    mantle_api::Orientation3<units::angular_velocity::radians_per_second_t> expected_second_orientation_rate_all{
        amount_to_turn / 1_s, 0_rad_per_s, 0_rad_per_s};
    mantle_api::Orientation3<units::angular_acceleration::radians_per_second_squared_t>
        expected_second_orientation_acceleration_all{amount_to_turn / (1_s * 1_s), 0_rad_per_s_sq, 0_rad_per_s_sq};
    EXPECT_TRIPLE(expected_second_orientation_all, entity_->GetOrientation())
    EXPECT_TRIPLE(expected_second_orientation_rate_all, entity_->GetOrientationRate())
    EXPECT_TRIPLE(expected_second_orientation_acceleration_all, entity_->GetOrientationAcceleration())
}

TEST_F(VehicleModelControlUnitTest,
       GivenEntityCompletelyOnFrictionPatch_WhenStep_ThenFrictionValuesForEachWheelSetInVMI)
{
    double expected_mue{0.8};
    AddFrictionPatchToMap(expected_mue);
    auto vehicle_model = GetVehicleModelWithFilledVMO(0, {0_m, 0_m, 0_m});

    auto vehicle_model_control_unit = std::make_unique<VehicleModelControlUnit>(&vehicle_model, &gtgen_map_);
    vehicle_model_control_unit->SetEntity(*entity_);

    vehicle_model_control_unit->Step(mantle_api::Time{0});

    EXPECT_EQ(expected_mue, vehicle_model.vehicle_model_in.wheel_states.front_left_mue);
    EXPECT_EQ(expected_mue, vehicle_model.vehicle_model_in.wheel_states.front_right_mue);
    EXPECT_EQ(expected_mue, vehicle_model.vehicle_model_in.wheel_states.rear_left_mue);
    EXPECT_EQ(expected_mue, vehicle_model.vehicle_model_in.wheel_states.rear_right_mue);
}

TEST_F(VehicleModelControlUnitTest,
       GivenEntityCompletelyOnFrictionPatch_WhenStep_ThenFrictionValuesForEachWheelSetInVehicleEntity)
{
    double expected_mue{0.8};
    AddFrictionPatchToMap(expected_mue);
    auto vehicle_model = GetVehicleModelWithFilledVMO(0, {0_m, 0_m, 0_m});

    auto vehicle_model_control_unit = std::make_unique<VehicleModelControlUnit>(&vehicle_model, &gtgen_map_);
    vehicle_model_control_unit->SetEntity(*entity_);

    vehicle_model_control_unit->Step(mantle_api::Time{0});

    EXPECT_EQ(expected_mue, entity_->GetWheelStates().front_left_mue);
    EXPECT_EQ(expected_mue, entity_->GetWheelStates().front_right_mue);
    EXPECT_EQ(expected_mue, entity_->GetWheelStates().rear_left_mue);
    EXPECT_EQ(expected_mue, entity_->GetWheelStates().rear_right_mue);
}

TEST_F(VehicleModelControlUnitTest, GivenVehicleModel_WhenStep_ThenTimeSTampInVMIIsSetToCurrentTime)
{
    mantle_api::Time expected_simulation_time{42};
    host::HostVehicleModel vehicle_model;
    auto vehicle_model_control_unit = std::make_unique<VehicleModelControlUnit>(&vehicle_model, &gtgen_map_);
    vehicle_model_control_unit->SetEntity(*entity_);

    vehicle_model_control_unit->Step(expected_simulation_time);

    EXPECT_EQ(expected_simulation_time, vehicle_model.vehicle_model_in.time_stamp);
}

TEST_F(VehicleModelControlUnitTest,
       GivenEntityFrontWheelsOnFrictionPatch_WhenStep_ThenFrictionValuesForFrontWheelsSetInVMI)
{
    double expected_mue{0.8};
    AddFrictionPatchToMap(expected_mue);
    auto vehicle_model = GetVehicleModelWithFilledVMO(0, {0_m, -10_m, 0_m});

    auto vehicle_model_control_unit = std::make_unique<VehicleModelControlUnit>(&vehicle_model, &gtgen_map_);
    vehicle_model_control_unit->SetEntity(*entity_);

    vehicle_model_control_unit->Step(mantle_api::Time{0});

    EXPECT_EQ(expected_mue, vehicle_model.vehicle_model_in.wheel_states.front_left_mue);
    EXPECT_EQ(expected_mue, vehicle_model.vehicle_model_in.wheel_states.front_right_mue);
    EXPECT_EQ(1.0, vehicle_model.vehicle_model_in.wheel_states.rear_left_mue);
    EXPECT_EQ(1.0, vehicle_model.vehicle_model_in.wheel_states.rear_right_mue);
}

TEST_F(VehicleModelControlUnitTest,
       GivenEntityFrontWheelsOnFrictionPatch_WhenStep_ThenFrictionValuesForFrontWheelsSetInVehicleEntity)
{
    double expected_mue{0.8};
    AddFrictionPatchToMap(expected_mue);
    auto vehicle_model = GetVehicleModelWithFilledVMO(0, {0_m, -10_m, 0_m});

    auto vehicle_model_control_unit = std::make_unique<VehicleModelControlUnit>(&vehicle_model, &gtgen_map_);
    vehicle_model_control_unit->SetEntity(*entity_);

    vehicle_model_control_unit->Step(mantle_api::Time{0});

    EXPECT_EQ(expected_mue, entity_->GetWheelStates().front_left_mue);
    EXPECT_EQ(expected_mue, entity_->GetWheelStates().front_right_mue);
    EXPECT_EQ(1.0, entity_->GetWheelStates().rear_left_mue);
    EXPECT_EQ(1.0, entity_->GetWheelStates().rear_right_mue);
}

TEST_F(VehicleModelControlUnitTest, GivenVehicleModelControlUnit_WhenHasFinshedIsCalled_ThenReturnsAlwaysFalse)
{
    host::HostVehicleModel vehicle_model;
    VehicleModelControlUnit control{&vehicle_model, nullptr};

    EXPECT_FALSE(control.HasFinished());
}

TEST_F(VehicleModelControlUnitTest, GivenVehicleModelControlUnit_WhenClone_ThenCopyCreated)
{
    host::HostVehicleModel vehicle_model;
    auto control_ptr = std::make_unique<VehicleModelControlUnit>(&vehicle_model, nullptr);

    auto copy = control_ptr->Clone();

    ASSERT_NE(nullptr, copy);
    EXPECT_NE(control_ptr.get(), copy.get());
}

}  // namespace gtgen::core::environment::controller
