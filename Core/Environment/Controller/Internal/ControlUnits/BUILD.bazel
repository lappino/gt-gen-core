cc_library(
    name = "control_unit_test_utils",
    testonly = True,
    hdrs = ["control_unit_test_utils.h"],
    visibility = ["//Core/Environment/Controller:__subpackages__"],
    deps = [":i_control_unit"],
)

cc_library(
    name = "control_units",
    visibility = [
        "//Core/Environment/Controller:__subpackages__",
        "//Core/Environment/GtGenEnvironment:__subpackages__",
    ],
    deps = [
        ":follow_trajectory_control_unit",
        ":follow_trajectory_with_speed_control_unit",
        ":host_vehicle_interface_control_unit",
        ":keep_velocity_control_unit",
        ":lane_change_control_unit",
        ":lane_offset_control_unit",
        ":maneuver_control_unit",
        ":no_op_control_unit",
        ":path_control_unit",
        ":recovery_control_unit",
        ":traffic_light_control_unit",
        ":traffic_participant_control_unit",
        ":vehicle_model_control_unit",
        ":velocity_control_unit",
    ],
)

cc_library(
    name = "dog_control_unit_nds_map",
    visibility = ["//:__pkg__"],
    deps = [":traffic_participant_control_unit"],
)

cc_library(
    name = "dog_control_unit_xodr_map",
    visibility = ["//:__pkg__"],
    deps = [":traffic_participant_control_unit"],
)

cc_library(
    name = "follow_trajectory_control_unit",
    srcs = ["follow_trajectory_control_unit.cpp"],
    hdrs = ["follow_trajectory_control_unit.h"],
    deps = [
        ":i_abstract_control_unit",
        "//Core/Environment/Controller/Internal/Utilities:controller_helpers",
        "//Core/Environment/Controller/Internal/Utilities:vector_utilities",
        "//Core/Environment/LaneFollowing:point_list_traverser",
    ],
)

cc_test(
    name = "follow_trajectory_control_unit_test",
    timeout = "short",
    srcs = ["follow_trajectory_control_unit_test.cpp"],
    deps = [
        ":follow_trajectory_control_unit",
        "//Core/Environment/Entities:entities",
        "//Core/Tests/TestUtils:expect_extensions",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "follow_trajectory_with_speed_control_unit",
    srcs = ["follow_trajectory_with_speed_control_unit.cpp"],
    hdrs = ["follow_trajectory_with_speed_control_unit.h"],
    deps = [
        ":i_abstract_control_unit",
        "//Core/Environment/Controller/Internal/Utilities:controller_helpers",
        "//Core/Environment/Controller/Internal/Utilities:vector_utilities",
        "//Core/Environment/LaneFollowing:point_list_traverser",
        "//Core/Service/Utility:derivative_utils",
    ],
)

cc_test(
    name = "follow_trajectory_with_speed_control_unit_test",
    timeout = "short",
    srcs = ["follow_trajectory_with_speed_control_unit_test.cpp"],
    deps = [
        ":follow_trajectory_with_speed_control_unit",
        "//Core/Environment/Entities:entities",
        "//Core/Tests/TestUtils:expect_extensions",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "host_vehicle_interface_control_unit",
    srcs = ["host_vehicle_interface_control_unit.cpp"],
    hdrs = ["host_vehicle_interface_control_unit.h"],
    visibility = ["//:__pkg__"],
    deps = [
        ":i_abstract_control_unit",
        "//Core/Environment/Exception:exception",
        "//Core/Environment/PathFinding:path_finder",
        "//Core/Environment/TrafficCommand:traffic_command_builder",
        "//Core/Service/Utility:string_utils",
    ],
)

cc_test(
    name = "host_vehicle_interface_control_unit_test",
    timeout = "short",
    srcs = ["host_vehicle_interface_control_unit_test.cpp"],
    deps = [
        ":host_vehicle_interface_control_unit",
        "//Core/Tests/TestUtils:convert_first_custom_traffic_command_of_host_vehicle_to_route",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "i_abstract_control_unit",
    hdrs = ["i_abstract_control_unit.h"],
    visibility = ["//Core/Environment/GtGenEnvironment:__subpackages__"],
    deps = [
        ":i_control_unit",
        "//Core/Environment/Controller/Internal/Utilities:vector_utilities",
        "//Core/Service/Utility:derivative_utils",
    ],
)

cc_test(
    name = "i_abstract_control_unit_test",
    timeout = "short",
    srcs = ["i_abstract_control_unit_test.cpp"],
    deps = [
        ":control_units",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "i_control_unit",
    hdrs = ["i_control_unit.h"],
    deps = [
        "//Core/Environment/Controller:controller_data_exchange_container",
        "//Core/Service/Profiling:profiling",
        "@mantle_api",
    ],
)

cc_library(
    name = "keep_velocity_control_unit",
    srcs = ["keep_velocity_control_unit.cpp"],
    hdrs = ["keep_velocity_control_unit.h"],
    deps = [":i_abstract_control_unit"],
)

cc_test(
    name = "keep_velocity_control_unit_test",
    timeout = "short",
    srcs = ["keep_velocity_control_unit_test.cpp"],
    deps = [
        ":keep_velocity_control_unit",
        "//Core/Environment/Entities:entities",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "lane_change_control_unit",
    srcs = ["lane_change_control_unit.cpp"],
    hdrs = ["lane_change_control_unit.h"],
    deps = [
        ":i_abstract_control_unit",
        ":path_control_unit",
        "//Core/Environment/Controller/Internal/TransitionDynamics:i_transition_dynamics",
        "//Core/Environment/Map/LaneLocationProvider:lane_location_provider",
        "//Core/Service/Utility:position_utils",
    ],
)

cc_test(
    name = "lane_change_control_unit_test",
    timeout = "short",
    srcs = ["lane_change_control_unit_test.cpp"],
    deps = [
        ":lane_change_control_unit",
        "//Core/Environment/Entities:entities",
        "//Core/Tests/TestUtils:expect_extensions",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "lane_offset_control_unit",
    srcs = ["lane_offset_control_unit.cpp"],
    hdrs = ["lane_offset_control_unit.h"],
    deps = [
        ":i_abstract_control_unit",
        "//Core/Environment/Controller/Internal/Utilities:spline_utilities",
        "//Core/Environment/Map/LaneLocationProvider:lane_location_provider",
        "//Core/Service/GlmWrapper:glm_basic_vector_utils",
    ],
)

cc_test(
    name = "lane_offset_control_unit_test",
    timeout = "short",
    srcs = ["lane_offset_control_unit_test.cpp"],
    deps = [
        ":lane_offset_control_unit",
        "//Core/Environment/Entities:entities",
        "//Core/Tests/TestUtils:expect_extensions",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "maneuver_control_unit",
    srcs = ["maneuver_control_unit.cpp"],
    hdrs = ["maneuver_control_unit.h"],
    deps = [
        ":i_abstract_control_unit",
        "//Core/Environment/Controller/Internal/Utilities:spline_utilities",
        "//Core/Environment/Controller/Internal/Utilities:vector_utilities",
        "//Core/Environment/Map/LaneLocationProvider:lane_location_provider",
    ],
)

cc_test(
    name = "maneuver_control_unit_test",
    timeout = "short",
    srcs = ["maneuver_control_unit_test.cpp"],
    deps = [
        ":maneuver_control_unit",
        "//Core/Environment/Entities:entities",
        "//Core/Tests/TestUtils:expect_extensions",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "no_op_control_unit",
    srcs = ["no_op_control_unit.cpp"],
    hdrs = ["no_op_control_unit.h"],
    deps = [":i_abstract_control_unit"],
)

cc_test(
    name = "no_op_control_unit_test",
    timeout = "short",
    srcs = ["no_op_control_unit_test.cpp"],
    deps = [
        ":no_op_control_unit",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "path_control_unit",
    srcs = ["path_control_unit.cpp"],
    hdrs = ["path_control_unit.h"],
    deps = [
        ":i_abstract_control_unit",
        "//Core/Environment/Controller/Internal/Utilities:controller_helpers",
        "//Core/Environment/Controller/Internal/Utilities:vector_utilities",
        "//Core/Environment/PathFinding:path_finder",
        "//Core/Service/Utility:math_utils",
        "//Core/Service/Utility:position_utils",
    ],
)

cc_test(
    name = "path_control_unit_test",
    timeout = "short",
    srcs = ["path_control_unit_test.cpp"],
    deps = [
        ":path_control_unit",
        "//Core/Environment/Entities:entities",
        "//Core/Tests/TestUtils:expect_extensions",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "recovery_control_unit",
    srcs = ["recovery_control_unit.cpp"],
    hdrs = ["recovery_control_unit.h"],
    deps = [
        ":i_abstract_control_unit",
        "//Core/Environment/Host:host_vehicle_interface",
        "//Core/Environment/Map/LaneLocationProvider:lane_location_provider",
        "//Core/Service/Utility:position_utils",
        "@//third_party/boost:circular_buffer",
    ],
)

cc_test(
    name = "recovery_control_unit_test",
    timeout = "short",
    srcs = ["recovery_control_unit_test.cpp"],
    deps = [
        ":recovery_control_unit",
        "//Core/Environment/Entities:entities",
        "//Core/Tests/TestUtils:expect_extensions",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "traffic_light_control_unit",
    srcs = ["traffic_light_control_unit.cpp"],
    hdrs = ["traffic_light_control_unit.h"],
    deps = [
        ":i_abstract_control_unit",
        "//Core/Environment/Controller/Internal/Utilities:traffic_light_state_machine",
        "//Core/Environment/Entities:traffic_light_entity",
        "//Core/Service/MantleApiExtension:static_object_properties",
    ],
)

cc_test(
    name = "traffic_light_control_unit_test",
    timeout = "short",
    srcs = ["traffic_light_control_unit_test.cpp"],
    deps = [
        ":traffic_light_control_unit",
        "//Core/Environment/Entities:entities",
        "//Core/Tests/TestUtils:expect_extensions",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "traffic_participant_control_unit",
    srcs = ["traffic_participant_control_unit.cpp"],
    hdrs = ["traffic_participant_control_unit.h"],
    visibility = ["//Core/Environment/GtGenEnvironment:__subpackages__"],
    deps = [
        ":i_abstract_control_unit",
        "//Core/Environment/Controller:external_controller_config",
        "//Core/Environment/Controller/Internal/Utilities/PlausibilityCheck:plausibility_check",
        "//Core/Environment/Exception:exception",
        "//Core/Environment/GroundTruth:sensor_view_builder",
        "//Core/Environment/Map/Common:converter_utility",
        "//Core/Service/FileSystem:file_system_utils",
        "//Core/Service/GroundTruthConversions:proto_to_mantle",
        "//Core/Service/Logging:logging",
        "@//third_party/boost:dll",
        "@//third_party/boost:function",
        "@//third_party/open_simulation_interface:open_simulation_interface",
        "@//third_party/osi_traffic_participant",
        "@fmt",
        "@mantle_api",
    ],
)

cc_test(
    name = "traffic_participant_control_unit_test",
    timeout = "short",
    srcs = ["traffic_participant_control_unit_test.cpp"],
    data = [":traffic_participant_model_example.so"],
    deps = [
        ":traffic_participant_control_unit",
        "//Core/Environment/Entities:entities",
        "//Core/Environment/Exception:exception",
        "//Core/Environment/GtGenEnvironment:entity_repository",
        "//Core/Service/FileSystem:file_system_utils",
        "//Core/Service/Utility:clock",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "//Core/Tests/TestUtils/ProtoUtils:proto_utilities",
        "@//third_party/open_simulation_interface:open_simulation_interface",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "traffic_participant_model_example",
    srcs = ["traffic_participant_model_example.cpp"],
    hdrs = ["traffic_participant_model_example.h"],
    visibility = ["//:__pkg__"],
    deps = [
        "//Core/Service/Logging:logging",
        "@//third_party/boost:dll",
        "@//third_party/open_simulation_interface:open_simulation_interface",
        "@//third_party/osi_traffic_participant",
    ],
)

cc_binary(
    name = "traffic_participant_model_example.so",
    srcs = ["traffic_participant_model_example.cpp"],
    linkshared = True,
    visibility = [
        "//:__pkg__",
        "//Core/Environment:__subpackages__",
    ],
    deps = [":traffic_participant_model_example"],
)

cc_test(
    name = "traffic_participant_model_example_test",
    timeout = "short",
    srcs = ["traffic_participant_model_example_test.cpp"],
    deps = [
        ":traffic_participant_model_example",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "vehicle_model_control_unit",
    srcs = ["vehicle_model_control_unit.cpp"],
    hdrs = ["vehicle_model_control_unit.h"],
    deps = [
        ":i_abstract_control_unit",
        "//Core/Environment/Controller/Internal/Utilities:wheel_contact",
        "//Core/Environment/Controller/Internal/Utilities/PlausibilityCheck:plausibility_check",
        "//Core/Environment/Host:host_vehicle_model",
        "//Core/Environment/Map/Common:converter_utility",
        "//Core/Environment/Map/GtGenMap:gtgenmap",
        "//Core/Service/Utility:derivative_utils",
    ],
)

cc_test(
    name = "vehicle_model_control_unit_test",
    timeout = "short",
    srcs = ["vehicle_model_control_unit_test.cpp"],
    deps = [
        ":vehicle_model_control_unit",
        "//Core/Tests/TestUtils:expect_extensions",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "velocity_control_unit",
    srcs = ["velocity_control_unit.cpp"],
    hdrs = ["velocity_control_unit.h"],
    deps = [
        ":i_abstract_control_unit",
        "//Core/Environment/Controller/Internal/Utilities:spline_utilities",
    ],
)

cc_test(
    name = "velocity_control_unit_test",
    timeout = "short",
    srcs = ["velocity_control_unit_test.cpp"],
    deps = [
        ":velocity_control_unit",
        "//Core/Environment/Entities:entities",
        "//Core/Service/GlmWrapper:glm_wrapper",
        "//Core/Tests/TestUtils:expect_extensions",
        "@googletest//:gtest_main",
    ],
)
