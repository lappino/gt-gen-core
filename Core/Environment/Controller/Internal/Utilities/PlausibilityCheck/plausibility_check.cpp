/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/plausibility_check.h"

namespace gtgen::core::environment::plausibility_check
{

void PlausibilityCheck::CheckPlausibility(const mantle_api::IEntity& entity,
                                          const units::time::second_t& elapsed_time,
                                          const std::string& map_coordinates)
{
    plausibility_check_position_derivatives_.Step(entity, elapsed_time, map_coordinates);
    plausibility_check_orientation_derivatives_.Step(entity, elapsed_time, map_coordinates);
}

}  // namespace gtgen::core::environment::plausibility_check
