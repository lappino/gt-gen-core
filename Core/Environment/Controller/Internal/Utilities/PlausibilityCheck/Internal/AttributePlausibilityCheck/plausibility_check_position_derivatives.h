/*******************************************************************************
 * Copyright (c) 2022-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_INTERNAL_ATTRIBUTEPLAUSIBILITYCHECK_PLAUSIBILITYCHECKPOSITIONDERIVATIVES_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_INTERNAL_ATTRIBUTEPLAUSIBILITYCHECK_PLAUSIBILITYCHECKPOSITIONDERIVATIVES_H

#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/Common/plausibility_check_base.h"
#include "Core/Environment/Controller/Internal/Utilities/PlausibilityCheck/Internal/Common/plausibility_check_log_messages.h"

#include <MantleAPI/Common/vector.h>
#include <units.h>

#include <optional>

namespace gtgen::core::environment::plausibility_check
{

struct PlausibilityThresholdsPositionDerivatives
{
    static constexpr units::velocity::meters_per_second_t velocity_epsilon{1.0 / 3.6};
    static constexpr units::acceleration::meters_per_second_squared_t acceleration_epsilon{1.0 / 3.6};

    static constexpr units::velocity::meters_per_second_t longitudinal_velocity_maximum{86.11};
    static constexpr units::velocity::meters_per_second_t longitudinal_velocity_minimum{-13.89};

    static constexpr units::velocity::meters_per_second_t lateral_velocity_maximum{4.17};
    static constexpr units::velocity::meters_per_second_t lateral_velocity_minimum{-4.17};

    static constexpr units::velocity::meters_per_second_t vertical_velocity_maximum{0.83};
    static constexpr units::velocity::meters_per_second_t vertical_velocity_minimum{-0.83};

    static constexpr units::acceleration::meters_per_second_squared_t longitudinal_acceleration_maximum{12.0};
    static constexpr units::acceleration::meters_per_second_squared_t longitudinal_acceleration_minimum{-12.0};

    static constexpr units::acceleration::meters_per_second_squared_t lateral_acceleration_maximum{12.0};
    static constexpr units::acceleration::meters_per_second_squared_t lateral_acceleration_minimum{-12.0};
};

class PlausibilityCheckPositionDerivatives final : public PlausibilityCheckBase
{
  private:
    PlausibilityCheckPositionDerivativesLogMessages log_messages_;
    PlausibilityThresholdsPositionDerivatives thresholds_;

    void ProcessAttributes() override;
    void CompareInternalAndExternalAttributes() const override;
    void CheckExternalAttributesPlausibility() const override;
    void UpdatePreviousAttributes() override;
    void CheckExternalVelocityPlausibility() const;
    void CheckExternalAccelerationPlausibility() const;

    mantle_api::Vec3<units::length::meter_t> current_external_position_{};
    mantle_api::Vec3<units::velocity::meters_per_second_t> current_external_velocity_{};
    mantle_api::Vec3<units::velocity::meters_per_second_t> local_current_external_velocity_{};
    mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> current_external_acceleration_{};
    mantle_api::Vec3<units::acceleration::meters_per_second_squared_t> local_current_external_acceleration_{};
    std::optional<mantle_api::Vec3<units::length::meter_t>> previous_external_position_{std::nullopt};
    std::optional<mantle_api::Vec3<units::velocity::meters_per_second_t>> previous_internal_velocity_{std::nullopt};
    std::optional<mantle_api::Vec3<units::velocity::meters_per_second_t>> current_internal_velocity_{std::nullopt};
    std::optional<mantle_api::Vec3<units::velocity::meters_per_second_t>> local_current_internal_velocity_{
        std::nullopt};
    std::optional<mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>> current_internal_acceleration_{
        std::nullopt};
    std::optional<mantle_api::Vec3<units::acceleration::meters_per_second_squared_t>>
        local_current_internal_acceleration_{std::nullopt};
};

}  // namespace gtgen::core::environment::plausibility_check

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_PLAUSIBILITYCHECK_INTERNAL_ATTRIBUTEPLAUSIBILITYCHECK_PLAUSIBILITYCHECKPOSITIONDERIVATIVES_H
