/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_TRAFFICLIGHTSTATEMACHINE_H
#define GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_TRAFFICLIGHTSTATEMACHINE_H

#include "Core/Environment/Controller/Internal/Utilities/traffic_light_phase_state.h"
#include "Core/Environment/Entities/traffic_light_entity.h"

#include <MantleAPI/Traffic/traffic_light_properties.h>

#include <vector>
namespace gtgen::core::environment::controller
{
class TrafficLightStateMachine
{
  public:
    TrafficLightStateMachine(std::vector<mantle_api::TrafficLightPhase> light_phases, bool repeat);
    TrafficLightStateMachine(TrafficLightStateMachine const& state_machine);
    TrafficLightStateMachine& operator=(const TrafficLightStateMachine&) = delete;
    TrafficLightStateMachine(TrafficLightStateMachine&&) = delete;
    TrafficLightStateMachine& operator=(TrafficLightStateMachine&&) = delete;
    ~TrafficLightStateMachine() = default;

    virtual void Start(entities::TrafficLightEntity* traffic_light_entity);

    void Step(mantle_api::Time simulation_time);

    virtual void Transition();

  private:
    void ActivateLightPhaseState();

    std::vector<mantle_api::TrafficLightPhase> light_phases_;
    bool repeat_;
    std::int16_t repeat_count_{0};

    entities::TrafficLightEntity* traffic_light_entity_{nullptr};
    std::unique_ptr<TrafficLightPhaseState> active_phase_state_{nullptr};
    std::size_t active_phase_index_{0};
};
}  // namespace gtgen::core::environment::controller

#endif  // GTGEN_CORE_ENVIRONMENT_CONTROLLER_INTERNAL_UTILITIES_TRAFFICLIGHTSTATEMACHINE_H
