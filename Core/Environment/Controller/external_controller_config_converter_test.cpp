/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "external_controller_config_converter.h"

#include <gtest/gtest.h>

#include <memory>

namespace gtgen::core::environment::controller
{

class ExternalControllerConfigConverterTest : public testing::Test
{
  protected:
    ExternalControllerConfigConverterData GetConverterData()
    {
        ExternalControllerConfigConverterData data;
        data.gtgen_map = &gtgen_map_;
        user_settings_.map_chunking = expected_map_chunking_;
        data.user_settings = &user_settings_;
        data.host_vehicle_interface = &host_vehicle_interface_;
        data.host_vehicle_model = &host_vehicle_model_;
        data.proto_ground_truth_builder_config = expected_proto_ground_truth_builder_config_;
        data.traffic_command_builder = &traffic_command_builder_;

        return data;
    }

    mantle_api::RouteDefinition GetRouteDefinition()
    {
        using units::literals::operator""_m;

        mantle_api::RouteDefinition route_definition;
        route_definition.waypoints.push_back({{1_m, 1_m, 1_m}, {}});
        route_definition.waypoints.push_back({{2_m, 2_m, 2_m}, {}});
        return route_definition;
    }

    mantle_api::UniqueId expected_id_{42};
    map::GtGenMap gtgen_map_{};
    service::user_settings::MapChunking expected_map_chunking_{40, 3};
    service::user_settings::UserSettings user_settings_{};
    host::HostVehicleInterface host_vehicle_interface_{};
    traffic_command::TrafficCommandBuilder traffic_command_builder_{};
    host::HostVehicleModel host_vehicle_model_{};
    ProtoGroundTruthBuilderConfig expected_proto_ground_truth_builder_config_{mantle_api::Time{5}};
};

TEST_F(ExternalControllerConfigConverterTest,
       GivenConfigConverter_WhenConvertToExternalHostConfig_ThenGtGenExternalControllerConfigCreated)
{
    bool expected_recovery_mode = true;
    user_settings_.host_vehicle.recovery_mode = expected_recovery_mode;
    mantle_api::ExternalControllerConfig external_config;
    external_config.route_definition = GetRouteDefinition();
    external_config.control_strategies.push_back(std::make_unique<mantle_api::KeepVelocityControlStrategy>());

    ExternalControllerConfigConverter converter(GetConverterData());
    std::unique_ptr<GtGenExternalControllerConfig> actual_config(
        dynamic_cast<GtGenExternalControllerConfig*>(converter.GetConfig(&external_config)));

    EXPECT_EQ(&gtgen_map_, actual_config->gtgen_map);
    EXPECT_EQ(expected_recovery_mode, actual_config->recovery_mode_enabled);
    EXPECT_EQ(&host_vehicle_model_, actual_config->host_vehicle_model);
    EXPECT_EQ(&host_vehicle_interface_, actual_config->host_interface);
    EXPECT_EQ(&traffic_command_builder_, actual_config->traffic_command_builder);
    EXPECT_EQ(2, actual_config->route_definition.waypoints.size());
    EXPECT_EQ(2, actual_config->waypoints.size());
    EXPECT_EQ(1, actual_config->control_strategies.size());
}

TEST_F(ExternalControllerConfigConverterTest, GivenConfigConverter_WhenConvertToTpmConfig_ThenDTpmConfigCreated)
{
    mantle_api::ExternalControllerConfig external_config;

    external_config.name = "traffic_participant_model_example";
    external_config.parameters["velocity"] = "27.777";

    ExternalControllerConfigConverter converter(GetConverterData());
    std::unique_ptr<TrafficParticipantControlUnitConfig> actual_config(
        dynamic_cast<TrafficParticipantControlUnitConfig*>(converter.GetConfig(&external_config)));

    EXPECT_EQ(external_config.parameters, actual_config->parameters);
    EXPECT_EQ(expected_proto_ground_truth_builder_config_.step_size,
              actual_config->proto_ground_truth_builder_config.step_size);
    EXPECT_EQ(expected_map_chunking_.cells_per_direction, actual_config->map_chunking.cells_per_direction);
    EXPECT_EQ(expected_map_chunking_.chunk_grid_size, actual_config->map_chunking.chunk_grid_size);
}

}  // namespace gtgen::core::environment::controller
