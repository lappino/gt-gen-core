/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/LaneFollowing/find_longest_path.h"

#include <MantleAPI/Common/i_identifiable.h>

namespace gtgen::core::environment::lanefollowing
{

namespace detail
{
bool IsLaneNotAdded(const std::vector<const map::Lane*>& lanes, mantle_api::UniqueId id)
{
    return std::find_if(lanes.begin(), lanes.end(), [id](const map::Lane* l) { return l->id == id; }) == lanes.end();
}

bool HasLaneAnySuccessor(const map::Lane* lane)
{
    return lane != nullptr && !lane->successors.empty();
}

bool HasLaneAnyPredecessor(const map::Lane* lane)
{
    return lane != nullptr && !lane->predecessors.empty();
}

const map::Lane* FindSuccessorLane(const map::GtGenMap& map, const map::Lane* lane)
{
    auto successor_ids = lane->successors;

    const map::Lane* successor_candidate{nullptr};
    for (const auto& successor_id : successor_ids)
    {
        successor_candidate = map.FindLane(successor_id);
        if (detail::HasLaneAnySuccessor(successor_candidate))
        {
            return successor_candidate;
        }
    }

    return successor_candidate;
}

const map::Lane* FindPredecessorLane(const map::GtGenMap& map, const map::Lane* lane)
{
    auto predecessor_ids = lane->predecessors;

    const map::Lane* predecessor_candidate{nullptr};
    for (const auto& predecessor_id : predecessor_ids)
    {
        predecessor_candidate = map.FindLane(predecessor_id);
        if (detail::HasLaneAnyPredecessor(predecessor_candidate))
        {
            return predecessor_candidate;
        }
    }

    return predecessor_candidate;
}

}  // namespace detail

std::vector<const map::Lane*> FindLongestPath(const map::GtGenMap& map,
                                              const std::vector<const map::Lane*>& start_lanes)
{
    std::vector<const map::Lane*> longest_path{};
    std::vector<const map::Lane*> current_path{};
    for (const auto* start_lane : start_lanes)
    {
        current_path = FindLongestPath(map, start_lane);
        if (current_path.size() > longest_path.size())
        {
            longest_path = current_path;
        }
    }

    return longest_path;
}

std::vector<const map::Lane*> FindLongestPath(const map::GtGenMap& map, const map::Lane* start_lane)
{
    std::vector<const map::Lane*> lanes{};

    const map::Lane* current_lane = start_lane;

    while (current_lane != nullptr && detail::IsLaneNotAdded(lanes, current_lane->id))
    {
        lanes.push_back(current_lane);
        current_lane = detail::FindSuccessorLane(map, current_lane);
    }

    return lanes;
}

std::vector<const map::Lane*> FindLongestBackwardsPath(const map::GtGenMap& map, const map::Lane* start_lane)
{
    std::vector<const map::Lane*> lanes{};

    const map::Lane* current_lane = start_lane;

    while (current_lane != nullptr && detail::IsLaneNotAdded(lanes, current_lane->id))
    {
        lanes.push_back(current_lane);
        current_lane = detail::FindPredecessorLane(map, current_lane);
    }

    return lanes;
}

}  // namespace gtgen::core::environment::lanefollowing
