/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_LANEFOLLOWING_FINDLONGESTPATH_H
#define GTGEN_CORE_ENVIRONMENT_LANEFOLLOWING_FINDLONGESTPATH_H

#include "Core/Environment/Map/GtGenMap/gtgen_map.h"

namespace gtgen::core::environment::lanefollowing
{
std::vector<const map::Lane*> FindLongestPath(const map::GtGenMap& map,
                                              const std::vector<const map::Lane*>& start_lanes);
std::vector<const map::Lane*> FindLongestPath(const map::GtGenMap& map, const map::Lane* start_lane);

std::vector<const map::Lane*> FindLongestBackwardsPath(const map::GtGenMap& map, const map::Lane* start_lane);

}  // namespace gtgen::core::environment::lanefollowing

#endif  // GTGEN_CORE_ENVIRONMENT_LANEFOLLOWING_FINDLONGESTPATH_H
