/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GtGenEnvironment/Internal/lane_assignment_service.h"

#include "Core/Environment/Controller/Internal/ControlUnits/traffic_participant_control_unit.h"
#include "Core/Environment/Controller/composite_controller.h"
#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map.h"
#include "Core/Service/GlmWrapper/glm_wrapper.h"
#include "Core/Service/Utility/string_utils.h"

#include <MantleAPI/Traffic/i_entity.h>

namespace gtgen::core::environment::api
{
using units::literals::operator""_m;

LaneAssignmentService::LaneAssignmentService(mantle_api::IEntityRepository& entity_repository,
                                             const ActiveControllerRepository& active_controller_repository)
    : entity_repository_{entity_repository}, active_controller_repository_{active_controller_repository}
{
}

void LaneAssignmentService::SetLaneLocationProvider(const map::LaneLocationProvider* lane_location_provider)
{
    lane_location_provider_ = lane_location_provider;
}

std::vector<mantle_api::LaneId> LaneAssignmentService::GetScenarioAssignedLaneForEntity(
    gtgen::core::environment::entities::BaseEntity* entity) const
{
    mantle_api::EntityProperties* properties = entity->GetProperties();
    if (properties == nullptr)
    {
        return std::vector<mantle_api::LaneId>{};
    }

    std::map<std::string, std::string>& properties_map = properties->properties;
    auto property_pair = properties_map.find("lane_assignment");

    if (property_pair == properties_map.end())
    {
        return std::vector<mantle_api::LaneId>{};
    }
    return gtgen::core::service::utility::StringToVectorInt(property_pair->second);
}

void LaneAssignmentService::SetIsEntityAllowedToLeaveLane(bool is_entity_allowed_to_leave_lane)
{
    is_entity_allowed_to_leave_lane_ = is_entity_allowed_to_leave_lane;
}

void LaneAssignmentService::Step()
{
    for (const auto& entity : entity_repository_.GetEntities())
    {
        gtgen::core::environment::entities::BaseEntity* base_entity_ptr =
            dynamic_cast<gtgen::core::environment::entities::BaseEntity*>(
                &entity_repository_.Get(entity->GetUniqueId()).value().get());

        if (base_entity_ptr == nullptr)
        {
            throw std::runtime_error("Entity doesn't inherit BaseEntity");
        }

        if (!base_entity_ptr->AreAssignedLanesDirty())
        {
            continue;
        }

        bool is_current_entity_allowed_to_leave_lane{is_entity_allowed_to_leave_lane_};
        auto composite_controllers =
            active_controller_repository_.GetControllersByEntityId(base_entity_ptr->GetUniqueId());
        auto active_controller = std::find_if(
            composite_controllers.begin(), composite_controllers.end(), [](auto it) { return it->IsActive(); });

        mantle_api::Vec3<units::length::meter_t> translated_pos = service::glmwrapper::ToGlobalSpace(
            base_entity_ptr->GetPosition(),
            base_entity_ptr->GetOrientation(),
            mantle_api::Vec3<units::length::meter_t>{
                0_m, 0_m, -base_entity_ptr->GetProperties()->bounding_box.dimension.height * 0.5});

        if (active_controller != composite_controllers.end())
        {
            if ((*active_controller)->GetType() ==
                gtgen::core::environment::controller::CompositeController::Type::kExternal)
            {
                auto composite_controller_ptr =
                    dynamic_cast<gtgen::core::environment::controller::CompositeController*>(*active_controller);
                auto& control_units = composite_controller_ptr->GetControlUnits();
                auto traffic_participant_control_unit =
                    find_if(control_units.begin(), control_units.end(), [](auto& it) {
                        return dynamic_cast<gtgen::core::environment::controller::TrafficParticipantControlUnit*>(
                                   it.get()) != nullptr;
                    });
                is_current_entity_allowed_to_leave_lane = traffic_participant_control_unit != control_units.end();
            }

            if (!lane_location_provider_->IsPositionOnLane(translated_pos) && !is_current_entity_allowed_to_leave_lane)
            {
                throw EnvironmentException(
                    "A moving object with Id {} was positioned off the road.\nWorld-Coordinate: "
                    "{}\nMap-Coordinate: {}",
                    base_entity_ptr->GetUniqueId(),
                    translated_pos,
                    environment::map::GetMapCoordinateString(
                        translated_pos, lane_location_provider_->GetGtGenMap().coordinate_converter.get()));
            }
        }

        const std::vector<mantle_api::UniqueId> sorted_lane_ids = lane_location_provider_->GetSortedLaneIdsAtPosition(
            base_entity_ptr->GetPosition(), base_entity_ptr->GetOrientation());

        const auto scenario_assigned_lanes = GetScenarioAssignedLaneForEntity(base_entity_ptr);
        if (!scenario_assigned_lanes.empty() && !sorted_lane_ids.empty())
        {
            std::vector<mantle_api::UniqueId> global_assigned_lane_ids{};

            const gtgen::core::environment::map::GtGenMap& map = lane_location_provider_->GetGtGenMap();
            const gtgen::core::environment::map::Lane* lane = lane_location_provider_->FindLane(sorted_lane_ids[0]);
            const gtgen::core::environment::map::LaneGroup* current_lane_group =
                map.FindLaneGroup(lane->parent_lane_group_id);

            for (const auto global_lane_id : current_lane_group->lane_ids)
            {
                // get local lane id
                const gtgen::core::environment::map::Lane* lane_in_group =
                    lane_location_provider_->FindLane(global_lane_id);

                if (std::find(
                        scenario_assigned_lanes.begin(), scenario_assigned_lanes.end(), lane_in_group->local_id) !=
                    scenario_assigned_lanes
                        .end()) /* is local lane_id matching to the assigned lane id vector in the scenario*/
                {
                    global_assigned_lane_ids.push_back(global_lane_id);
                }
            }
            base_entity_ptr->SetAssignedLaneIds(global_assigned_lane_ids);
        }
        else
        {
            base_entity_ptr->SetAssignedLaneIds(sorted_lane_ids);
        }
    }
}
}  // namespace gtgen::core::environment::api
