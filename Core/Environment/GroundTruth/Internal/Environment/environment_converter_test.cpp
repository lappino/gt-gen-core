/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Environment/environment_converter.h"

#include <gtest/gtest.h>

#include <utility>

namespace gtgen::core::environment::proto_groundtruth
{

//
// EnvironmentConditions AmbientIllumination
//
class AmbientIlluminationOSIConverterTestFixture
    : public testing::TestWithParam<
          std::tuple<mantle_api::Illumination, osi3::EnvironmentalConditions_AmbientIllumination>>
{
};

INSTANTIATE_TEST_SUITE_P(
    EnvironmentConverter,
    AmbientIlluminationOSIConverterTestFixture,
    testing::ValuesIn(
        std::vector<std::tuple<mantle_api::Illumination, osi3::EnvironmentalConditions_AmbientIllumination>>{
            std::make_tuple(mantle_api::Illumination::kOther,
                            osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_OTHER),
            std::make_tuple(mantle_api::Illumination::kUnknown,
                            osi3::EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_UNKNOWN),
            std::make_tuple(mantle_api::Illumination::kLevel1,
                            osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL1),
            std::make_tuple(mantle_api::Illumination::kLevel2,
                            osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL2),
            std::make_tuple(mantle_api::Illumination::kLevel3,
                            osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL3),
            std::make_tuple(mantle_api::Illumination::kLevel4,
                            osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL4),
            std::make_tuple(mantle_api::Illumination::kLevel5,
                            osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL5),
            std::make_tuple(mantle_api::Illumination::kLevel6,
                            osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL6),
            std::make_tuple(mantle_api::Illumination::kLevel7,
                            osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL7),
            std::make_tuple(mantle_api::Illumination::kLevel8,
                            osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL8),
            std::make_tuple(mantle_api::Illumination::kLevel9,
                            osi3::EnvironmentalConditions_AmbientIllumination::
                                EnvironmentalConditions_AmbientIllumination_AMBIENT_ILLUMINATION_LEVEL9)}));

TEST_P(AmbientIlluminationOSIConverterTestFixture,
       GivenEnvironmentIllumination_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    mantle_api::Illumination gtgen_type = std::get<0>(GetParam());
    osi3::EnvironmentalConditions_AmbientIllumination expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = GetOSIAmbientIllumination(gtgen_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

//
// EnvironmentConditions Precipitation
//
class PrecipitationOSIConverterTestFixture
    : public testing::TestWithParam<std::tuple<mantle_api::Precipitation, osi3::EnvironmentalConditions_Precipitation>>
{
};

INSTANTIATE_TEST_SUITE_P(
    EnvironmentConverter,
    PrecipitationOSIConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<mantle_api::Precipitation, osi3::EnvironmentalConditions_Precipitation>>{
        std::make_tuple(
            mantle_api::Precipitation::kOther,
            osi3::EnvironmentalConditions_Precipitation::EnvironmentalConditions_Precipitation_PRECIPITATION_OTHER),
        std::make_tuple(
            mantle_api::Precipitation::kNone,
            osi3::EnvironmentalConditions_Precipitation::EnvironmentalConditions_Precipitation_PRECIPITATION_NONE),
        std::make_tuple(mantle_api::Precipitation::kVeryLight,
                        osi3::EnvironmentalConditions_Precipitation::
                            EnvironmentalConditions_Precipitation_PRECIPITATION_VERY_LIGHT),
        std::make_tuple(
            mantle_api::Precipitation::kLight,
            osi3::EnvironmentalConditions_Precipitation::EnvironmentalConditions_Precipitation_PRECIPITATION_LIGHT),
        std::make_tuple(
            mantle_api::Precipitation::kModerate,
            osi3::EnvironmentalConditions_Precipitation::EnvironmentalConditions_Precipitation_PRECIPITATION_MODERATE),
        std::make_tuple(
            mantle_api::Precipitation::kHeavy,
            osi3::EnvironmentalConditions_Precipitation::EnvironmentalConditions_Precipitation_PRECIPITATION_HEAVY),
        std::make_tuple(mantle_api::Precipitation::kVeryHeavy,
                        osi3::EnvironmentalConditions_Precipitation::
                            EnvironmentalConditions_Precipitation_PRECIPITATION_VERY_HEAVY),
        std::make_tuple(
            mantle_api::Precipitation::kExtreme,
            osi3::EnvironmentalConditions_Precipitation::EnvironmentalConditions_Precipitation_PRECIPITATION_EXTREME),
        std::make_tuple(mantle_api::Precipitation::kUnknown,
                        osi3::EnvironmentalConditions_Precipitation::
                            EnvironmentalConditions_Precipitation_PRECIPITATION_UNKNOWN)}));

TEST_P(PrecipitationOSIConverterTestFixture, GivenPrecipitation_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    mantle_api::Precipitation gtgen_type = std::get<0>(GetParam());
    osi3::EnvironmentalConditions_Precipitation expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = GetOSIPrecipitation(gtgen_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

//
// EnvironmentConditions Fog
//
class FogConverterTestFixture
    : public testing::TestWithParam<std::tuple<mantle_api::Fog, osi3::EnvironmentalConditions_Fog>>
{
};

INSTANTIATE_TEST_SUITE_P(
    EnvironmentConverter,
    FogConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<mantle_api::Fog, osi3::EnvironmentalConditions_Fog>>{
        std::make_tuple(mantle_api::Fog::kOther,
                        osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_OTHER),
        std::make_tuple(mantle_api::Fog::kUnknown,
                        osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_UNKNOWN),
        std::make_tuple(mantle_api::Fog::kExcellentVisibility,
                        osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_EXCELLENT_VISIBILITY),
        std::make_tuple(mantle_api::Fog::kGoodVisibility,
                        osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_GOOD_VISIBILITY),
        std::make_tuple(mantle_api::Fog::kModerateVisibility,
                        osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_MODERATE_VISIBILITY),
        std::make_tuple(mantle_api::Fog::kPoorVisibility,
                        osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_POOR_VISIBILITY),
        std::make_tuple(mantle_api::Fog::kMist,
                        osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_MIST),
        std::make_tuple(mantle_api::Fog::kLight,
                        osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_LIGHT),
        std::make_tuple(mantle_api::Fog::kThick,
                        osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_THICK),
        std::make_tuple(mantle_api::Fog::kDense,
                        osi3::EnvironmentalConditions_Fog::EnvironmentalConditions_Fog_FOG_DENSE)}));

TEST_P(FogConverterTestFixture, GivenFog_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    mantle_api::Fog gtgen_type = std::get<0>(GetParam());
    osi3::EnvironmentalConditions_Fog expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = GetOSIFog(gtgen_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

}  // namespace gtgen::core::environment::proto_groundtruth
