/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Dynamic/pedestrian_entity_proto_converter.h"

#include "Core/Environment/Entities/pedestrian_entity.h"
#include "Core/Environment/GroundTruth/Internal/Dynamic/dynamic_proto_converter_utils.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::proto_groundtruth
{

TEST(PedestrianEntityProtoConverterTest, GivenPedestrianEntity_WhenFillProtoGroundTruthPedestrianEntity_ThenGTCorrect)
{
    entities::PedestrianEntity pedestrian(21, "PedestrianName");
    auto pedestrian_properties = std::make_unique<mantle_api::PedestrianProperties>();
    pedestrian.SetProperties(std::move(pedestrian_properties));

    osi3::GroundTruth gt;
    FillProtoGroundTruthPedestrianEntity(&pedestrian, gt);

    ASSERT_EQ(1, gt.moving_object().size());
    const auto object = gt.moving_object().cbegin();
    EXPECT_EQ(osi3::MovingObject::TYPE_PEDESTRIAN, object->type());

    EXPECT_EQ("Pedestrian", gt.moving_object(0).source_reference().begin()->identifier(0));
    EXPECT_EQ("PedestrianName", gt.moving_object(0).source_reference().begin()->identifier(1));
}

TEST(PedestrianEntityProtoConverterTest, GivenAnimalEntity_WhenFillProtoGroundTruth_ThenTypeIsAnimal)
{
    entities::PedestrianEntity pedestrian(21, "PedestrianName");
    auto pedestrian_properties = std::make_unique<mantle_api::PedestrianProperties>();
    pedestrian_properties->type = mantle_api::EntityType::kAnimal;
    pedestrian.SetProperties(std::move(pedestrian_properties));

    osi3::GroundTruth gt;
    FillProtoGroundTruthPedestrianEntity(&pedestrian, gt);

    const auto object = gt.moving_object().cbegin();
    EXPECT_EQ(osi3::MovingObject::TYPE_ANIMAL, object->type());
}

}  // namespace gtgen::core::environment::proto_groundtruth
