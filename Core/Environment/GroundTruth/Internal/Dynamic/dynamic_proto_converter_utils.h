/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_DYNAMIC_DYNAMICPROTOCONVERTERUTILS_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_DYNAMIC_DYNAMICPROTOCONVERTERUTILS_H

#include "osi_object.pb.h"

#include <MantleAPI/Traffic/i_entity.h>

namespace gtgen::core::environment::proto_groundtruth
{
void FillBasicMovingObjectProperties(const mantle_api::IEntity* entity, osi3::MovingObject* gt_moving_object);
void FillSourceReference(const mantle_api::IEntity* entity,
                         const std::string& entity_type,
                         osi3::ExternalReference* external_ref);
}  // namespace gtgen::core::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_DYNAMIC_DYNAMICPROTOCONVERTERUTILS_H
