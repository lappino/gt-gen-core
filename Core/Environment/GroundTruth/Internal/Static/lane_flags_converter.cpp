/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Static/lane_flags_converter.h"

#include "Core/Environment/GroundTruth/Internal/Static/lane_flags_converter_utils.h"

namespace gtgen::core::environment::proto_groundtruth
{

void AddLaneFlags(const map::Lane& lane, osi3::Lane& gt_lane)
{
    gt_lane.mutable_classification()->set_type(GetLaneFlagType(lane.flags));
    gt_lane.mutable_classification()->set_subtype(GetLaneFlagSubType(lane.flags));
}

}  // namespace gtgen::core::environment::proto_groundtruth
