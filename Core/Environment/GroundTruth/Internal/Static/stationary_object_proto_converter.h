/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_STATIONARYOBJECTPROTOCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_STATIONARYOBJECTPROTOCONVERTER_H

#include "Core/Environment/Map/GtGenMap/road_objects.h"
#include "osi_groundtruth.pb.h"

#include <MantleAPI/Traffic/i_entity.h>

namespace gtgen::core::environment::proto_groundtruth
{
/// @deprecated - remove when gtgen map does not contain any map::RoadObjects anymore!
void FillProtoGroundTruthStationaryObject(const map::RoadObject& gtgen_road_object, osi3::GroundTruth& groundtruth);

void FillProtoSourceReference(const mantle_api::IEntity* entity,
                              const std::string& entity_type,
                              osi3::ExternalReference* external_ref);

void AddStationaryEntityToGroundTruth(const mantle_api::IEntity* entity, osi3::GroundTruth& ground_truth);

}  // namespace gtgen::core::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_STATIONARYOBJECTPROTOCONVERTER_H
