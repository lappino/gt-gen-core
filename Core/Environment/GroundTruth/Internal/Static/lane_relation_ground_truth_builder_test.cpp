/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/GroundTruth/Internal/Static/lane_relation_ground_truth_builder.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::proto_groundtruth
{
TEST(LaneGroundTruthBuilderTest, GivenLaneWithoutLaneRelations_WhenFillLaneRelations_ThenNoRelationIsSet)
{
    LaneRelationGroundTruthBuilder builder({1});

    map::Lane lane1{1};

    osi3::Lane proto_lane;
    builder.AddLanePairings(lane1, proto_lane);

    EXPECT_EQ(0, proto_lane.classification().lane_pairing_size());
    EXPECT_EQ(0, proto_lane.classification().right_adjacent_lane_id_size());
    EXPECT_EQ(0, proto_lane.classification().left_adjacent_lane_id_size());
}

TEST(LaneGroundTruthBuilderTest, GivenSuccessorIdNotExisting_WhenFillLaneRelations_ThenNoLanePairingIsSet)
{
    LaneRelationGroundTruthBuilder builder({1});

    map::Lane lane1{1};
    lane1.successors.push_back(2);

    osi3::Lane proto_lane;
    builder.AddLanePairings(lane1, proto_lane);

    ASSERT_EQ(0, proto_lane.classification().lane_pairing_size());
}

TEST(LaneGroundTruthBuilderTest, GivenPredecessorIdNotExisting_WhenFillLaneRelations_ThenNoLanePairingIsSet)
{
    LaneRelationGroundTruthBuilder builder({1});

    map::Lane lane1{1};
    lane1.predecessors.push_back(2);

    osi3::Lane proto_lane;
    builder.AddLanePairings(lane1, proto_lane);

    ASSERT_EQ(0, proto_lane.classification().lane_pairing_size());
}

TEST(LaneGroundTruthBuilderTest,
     GivenSuccessorAndPredecessorBothNotExisting_WhenFillLaneRelations_ThenNoLanePairingIsSet)
{
    LaneRelationGroundTruthBuilder builder({1});

    map::Lane lane1{1};
    lane1.successors.push_back(2);
    lane1.predecessors.push_back(3);

    osi3::Lane proto_lane;
    builder.AddLanePairings(lane1, proto_lane);

    ASSERT_EQ(0, proto_lane.classification().lane_pairing_size());
}

TEST(LaneGroundTruthBuilderTest,
     GivenSuccessorExistingAndPredecessorNotExisting_WhenFillLaneRelations_ThenLanePairingWithInvalidIdsSet)
{
    LaneRelationGroundTruthBuilder builder({1, 2});

    map::Lane lane1{1};
    lane1.successors.push_back(2);
    lane1.predecessors.push_back(3);

    osi3::Lane proto_lane;
    builder.AddLanePairings(lane1, proto_lane);

    ASSERT_EQ(1, proto_lane.classification().lane_pairing_size());
    EXPECT_EQ(2, proto_lane.classification().lane_pairing(0).successor_lane_id().value());
    EXPECT_EQ(mantle_api::InvalidId, proto_lane.classification().lane_pairing(0).antecessor_lane_id().value());
}

TEST(LaneGroundTruthBuilderTest,
     GivenSuccessorNotExistingAndPredecessorExisting_WhenFillLaneRelations_ThenLanePairingWithInvalidIdsSet)
{
    LaneRelationGroundTruthBuilder builder({1, 3});

    map::Lane lane1{1};
    lane1.successors.push_back(2);
    lane1.predecessors.push_back(3);

    osi3::Lane proto_lane;
    builder.AddLanePairings(lane1, proto_lane);

    ASSERT_EQ(1, proto_lane.classification().lane_pairing_size());
    EXPECT_EQ(mantle_api::InvalidId, proto_lane.classification().lane_pairing(0).successor_lane_id().value());
    EXPECT_EQ(3, proto_lane.classification().lane_pairing(0).antecessor_lane_id().value());
}

TEST(LaneGroundTruthBuilderTest, GivenLaneWithOneSuccessor_WhenFillLaneRelations_ThenGroundTruthLaneHasOnlySuccessor)
{
    std::unordered_set<mantle_api::UniqueId> existing_lane_ids{1, 2};
    LaneRelationGroundTruthBuilder builder(existing_lane_ids);

    map::Lane lane1{1};
    lane1.successors.push_back(2);

    osi3::Lane proto_lane;
    builder.AddLanePairings(lane1, proto_lane);

    ASSERT_EQ(1, proto_lane.classification().lane_pairing().size());
    EXPECT_EQ(2, proto_lane.classification().lane_pairing(0).successor_lane_id().value());
    EXPECT_EQ(mantle_api::InvalidId, proto_lane.classification().lane_pairing(0).antecessor_lane_id().value());
}

TEST(LaneGroundTruthBuilderTest, GivenLaneWithTwoSuccessors_WhenFillLaneRelations_ThenGroundTruthLaneHasTwoSuccessors)
{
    std::unordered_set<mantle_api::UniqueId> existing_lane_ids{1, 2, 3};
    LaneRelationGroundTruthBuilder builder(existing_lane_ids);

    map::Lane lane1{1};
    lane1.successors.push_back(2);
    lane1.successors.push_back(3);

    osi3::Lane proto_lane;
    builder.AddLanePairings(lane1, proto_lane);

    ASSERT_EQ(2, proto_lane.classification().lane_pairing().size());
    EXPECT_EQ(2, proto_lane.classification().lane_pairing(0).successor_lane_id().value());
    EXPECT_EQ(3, proto_lane.classification().lane_pairing(1).successor_lane_id().value());
}

TEST(LaneGroundTruthBuilderTest,
     GivenLaneWithOnePredecessor_WhenFillLaneRelations_ThenGroundTruthLaneHasOnlyAnteSuccessor)
{
    std::unordered_set<mantle_api::UniqueId> existing_lane_ids{1, 2};
    LaneRelationGroundTruthBuilder builder(existing_lane_ids);

    map::Lane lane1{1};
    lane1.predecessors.push_back(2);

    osi3::Lane proto_lane;
    builder.AddLanePairings(lane1, proto_lane);

    ASSERT_EQ(1, proto_lane.classification().lane_pairing().size());
    EXPECT_EQ(mantle_api::InvalidId, proto_lane.classification().lane_pairing(0).successor_lane_id().value());
    EXPECT_EQ(2, proto_lane.classification().lane_pairing(0).antecessor_lane_id().value());
}

TEST(LaneGroundTruthBuilderTest,
     GivenLaneWithTwoPredecessors_WhenFillLaneRelations_ThenGroundTruthLaneHasTwoAntecessors)
{
    std::unordered_set<mantle_api::UniqueId> existing_lane_ids{1, 2, 3};
    LaneRelationGroundTruthBuilder builder(existing_lane_ids);

    map::Lane lane1{1};
    lane1.predecessors.push_back(2);
    lane1.predecessors.push_back(3);

    osi3::Lane proto_lane;
    builder.AddLanePairings(lane1, proto_lane);

    ASSERT_EQ(2, proto_lane.classification().lane_pairing().size());
    EXPECT_EQ(2, proto_lane.classification().lane_pairing(0).antecessor_lane_id().value());
    EXPECT_EQ(3, proto_lane.classification().lane_pairing(1).antecessor_lane_id().value());
}

TEST(LaneGroundTruthBuilderTest,
     GivenLaneWithOneSuccessorAndPredecessor_WhenFillLaneRelations_ThenGroundTruthLaneHasOneLanePairing)
{
    std::unordered_set<mantle_api::UniqueId> existing_lane_ids{1, 2, 3};
    LaneRelationGroundTruthBuilder builder(existing_lane_ids);

    map::Lane lane1{1};
    lane1.successors.push_back(2);
    lane1.predecessors.push_back(3);

    osi3::Lane proto_lane;
    builder.AddLanePairings(lane1, proto_lane);

    ASSERT_EQ(1, proto_lane.classification().lane_pairing().size());
    EXPECT_EQ(2, proto_lane.classification().lane_pairing(0).successor_lane_id().value());
    EXPECT_EQ(3, proto_lane.classification().lane_pairing(0).antecessor_lane_id().value());
}

TEST(LaneGroundTruthBuilderTest,
     GivenLaneWithTwoSuccessorAndPredecessorEach_WhenFillLaneRelations_ThenGroundTruthLaneHasAllFourPairingCombinations)
{
    std::unordered_set<mantle_api::UniqueId> existing_lane_ids{1, 2, 3, 4, 5};
    LaneRelationGroundTruthBuilder builder(existing_lane_ids);

    map::Lane lane1{1};
    lane1.successors.push_back(2);
    lane1.successors.push_back(3);
    lane1.predecessors.push_back(4);
    lane1.predecessors.push_back(5);

    osi3::Lane proto_lane;
    builder.AddLanePairings(lane1, proto_lane);

    ASSERT_EQ(4, proto_lane.classification().lane_pairing().size());
    EXPECT_EQ(2, proto_lane.classification().lane_pairing(0).successor_lane_id().value());
    EXPECT_EQ(4, proto_lane.classification().lane_pairing(0).antecessor_lane_id().value());

    EXPECT_EQ(3, proto_lane.classification().lane_pairing(1).successor_lane_id().value());
    EXPECT_EQ(4, proto_lane.classification().lane_pairing(1).antecessor_lane_id().value());

    EXPECT_EQ(2, proto_lane.classification().lane_pairing(2).successor_lane_id().value());
    EXPECT_EQ(5, proto_lane.classification().lane_pairing(2).antecessor_lane_id().value());

    EXPECT_EQ(3, proto_lane.classification().lane_pairing(3).successor_lane_id().value());
    EXPECT_EQ(5, proto_lane.classification().lane_pairing(3).antecessor_lane_id().value());
}

TEST(LaneGroundTruthBuilderTest,
     GivenLaneWithTwoSuccessorAndThreePredecessor_WhenFillLaneRelations_ThenGroundTruthLaneHasAllSixPairingCombinations)
{
    std::unordered_set<mantle_api::UniqueId> existing_lane_ids{1, 2, 3, 4, 5, 6};
    LaneRelationGroundTruthBuilder builder(existing_lane_ids);

    map::Lane lane1{1};
    lane1.successors.push_back(2);
    lane1.successors.push_back(3);
    lane1.predecessors.push_back(4);
    lane1.predecessors.push_back(5);
    lane1.predecessors.push_back(6);

    osi3::Lane proto_lane;
    builder.AddLanePairings(lane1, proto_lane);

    ASSERT_EQ(6, proto_lane.classification().lane_pairing().size());
    EXPECT_EQ(2, proto_lane.classification().lane_pairing(0).successor_lane_id().value());
    EXPECT_EQ(4, proto_lane.classification().lane_pairing(0).antecessor_lane_id().value());

    EXPECT_EQ(3, proto_lane.classification().lane_pairing(1).successor_lane_id().value());
    EXPECT_EQ(4, proto_lane.classification().lane_pairing(1).antecessor_lane_id().value());

    EXPECT_EQ(2, proto_lane.classification().lane_pairing(2).successor_lane_id().value());
    EXPECT_EQ(5, proto_lane.classification().lane_pairing(2).antecessor_lane_id().value());

    EXPECT_EQ(3, proto_lane.classification().lane_pairing(3).successor_lane_id().value());
    EXPECT_EQ(5, proto_lane.classification().lane_pairing(3).antecessor_lane_id().value());

    EXPECT_EQ(2, proto_lane.classification().lane_pairing(4).successor_lane_id().value());
    EXPECT_EQ(6, proto_lane.classification().lane_pairing(4).antecessor_lane_id().value());

    EXPECT_EQ(3, proto_lane.classification().lane_pairing(5).successor_lane_id().value());
    EXPECT_EQ(6, proto_lane.classification().lane_pairing(5).antecessor_lane_id().value());
}

TEST(LaneGroundTruthBuilderTest,
     GivenLaneWithThreeSuccessorAndOnePredecessor_WhenFillLaneRelations_ThenGroundTruthLaneHasAllSixPairingCombinations)
{
    std::unordered_set<mantle_api::UniqueId> existing_lane_ids{1, 2, 3, 4, 5, 6};
    LaneRelationGroundTruthBuilder builder(existing_lane_ids);

    map::Lane lane1{1};
    lane1.successors.push_back(2);
    lane1.successors.push_back(3);
    lane1.successors.push_back(4);
    lane1.predecessors.push_back(5);
    lane1.predecessors.push_back(6);

    osi3::Lane proto_lane;
    builder.AddLanePairings(lane1, proto_lane);

    ASSERT_EQ(6, proto_lane.classification().lane_pairing().size());
    EXPECT_EQ(2, proto_lane.classification().lane_pairing(0).successor_lane_id().value());
    EXPECT_EQ(5, proto_lane.classification().lane_pairing(0).antecessor_lane_id().value());

    EXPECT_EQ(3, proto_lane.classification().lane_pairing(1).successor_lane_id().value());
    EXPECT_EQ(5, proto_lane.classification().lane_pairing(1).antecessor_lane_id().value());

    EXPECT_EQ(4, proto_lane.classification().lane_pairing(2).successor_lane_id().value());
    EXPECT_EQ(5, proto_lane.classification().lane_pairing(2).antecessor_lane_id().value());

    EXPECT_EQ(2, proto_lane.classification().lane_pairing(3).successor_lane_id().value());
    EXPECT_EQ(6, proto_lane.classification().lane_pairing(3).antecessor_lane_id().value());

    EXPECT_EQ(3, proto_lane.classification().lane_pairing(4).successor_lane_id().value());
    EXPECT_EQ(6, proto_lane.classification().lane_pairing(4).antecessor_lane_id().value());

    EXPECT_EQ(4, proto_lane.classification().lane_pairing(5).successor_lane_id().value());
    EXPECT_EQ(6, proto_lane.classification().lane_pairing(5).antecessor_lane_id().value());
}

TEST(LaneGroundTruthBuilderTest, GivenLaneWithOneLeftAdjacentLane_WhenAddAdjacentLaneIds_ThenOneLeftAdjacentLaneIdSet)
{
    std::unordered_set<mantle_api::UniqueId> existing_lane_ids{1, 2};
    LaneRelationGroundTruthBuilder builder(existing_lane_ids);

    map::Lane lane1{1};
    lane1.left_adjacent_lanes.push_back(2);

    osi3::Lane proto_lane;
    builder.AddAdjacentLaneIds(lane1, proto_lane);

    ASSERT_EQ(1, proto_lane.classification().left_adjacent_lane_id_size());
    EXPECT_EQ(2, proto_lane.classification().left_adjacent_lane_id(0).value());
    EXPECT_EQ(0, proto_lane.classification().right_adjacent_lane_id_size());
}

TEST(LaneGroundTruthBuilderTest, GivenLaneWithOneRightAdjacentLane_WhenAddAdjacentLaneIds_ThenOneRightAdjacentLaneIdSet)
{
    std::unordered_set<mantle_api::UniqueId> existing_lane_ids{1, 2};
    LaneRelationGroundTruthBuilder builder(existing_lane_ids);

    map::Lane lane1{1};
    lane1.right_adjacent_lanes.push_back(2);

    osi3::Lane proto_lane;
    builder.AddAdjacentLaneIds(lane1, proto_lane);

    ASSERT_EQ(1, proto_lane.classification().right_adjacent_lane_id_size());
    EXPECT_EQ(2, proto_lane.classification().right_adjacent_lane_id(0).value());
    EXPECT_EQ(0, proto_lane.classification().left_adjacent_lane_id_size());
}

TEST(LaneGroundTruthBuilderTest, GivenLaneWithMultipleAdjacentLanes_WhenAddAdjacentLaneIds_ThenAllAdjacentLanesAreSet)
{
    std::unordered_set<mantle_api::UniqueId> existing_lane_ids{1, 2, 3, 4, 5};
    LaneRelationGroundTruthBuilder builder(existing_lane_ids);

    map::Lane lane1{1};
    lane1.left_adjacent_lanes.push_back(2);
    lane1.left_adjacent_lanes.push_back(3);
    lane1.right_adjacent_lanes.push_back(4);
    lane1.right_adjacent_lanes.push_back(5);

    osi3::Lane proto_lane;
    builder.AddAdjacentLaneIds(lane1, proto_lane);

    ASSERT_EQ(2, proto_lane.classification().left_adjacent_lane_id_size());
    EXPECT_EQ(2, proto_lane.classification().left_adjacent_lane_id(0).value());
    EXPECT_EQ(3, proto_lane.classification().left_adjacent_lane_id(1).value());

    ASSERT_EQ(2, proto_lane.classification().right_adjacent_lane_id_size());
    EXPECT_EQ(4, proto_lane.classification().right_adjacent_lane_id(0).value());
    EXPECT_EQ(5, proto_lane.classification().right_adjacent_lane_id(1).value());
}

TEST(LaneGroundTruthBuilderTest,
     GivenLaneWithNonExistingAdjacentLanes_WhenAddAdjacentLaneIds_ThenOnlyExistingAdjacentLanesAreSet)
{
    std::unordered_set<mantle_api::UniqueId> existing_lane_ids{1, 2, 4};
    LaneRelationGroundTruthBuilder builder(existing_lane_ids);

    map::Lane lane1{1};
    lane1.left_adjacent_lanes.push_back(2);
    lane1.left_adjacent_lanes.push_back(3);
    lane1.right_adjacent_lanes.push_back(4);
    lane1.right_adjacent_lanes.push_back(5);

    osi3::Lane proto_lane;
    builder.AddAdjacentLaneIds(lane1, proto_lane);

    ASSERT_EQ(1, proto_lane.classification().left_adjacent_lane_id_size());
    EXPECT_EQ(2, proto_lane.classification().left_adjacent_lane_id(0).value());

    ASSERT_EQ(1, proto_lane.classification().right_adjacent_lane_id_size());
    EXPECT_EQ(4, proto_lane.classification().right_adjacent_lane_id(0).value());
}

}  // namespace gtgen::core::environment::proto_groundtruth
