/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Entities/static_object_entity.h"
#include "Core/Environment/GroundTruth/sensor_view_builder.h"
#include "Core/Environment/GroundTruth/sensor_view_builder_test_common.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"

#include <gtest/gtest.h>
#include <units.h>

using units::literals::operator""_m;
using units::literals::operator""_rad;
using units::literals::operator""_deg;

namespace gtgen::core::environment::proto_groundtruth
{
class GroundSignBaseTest : public BaseSensorViewBuilderObjectGenerationTest
{
  protected:
    void SetUp() override
    {
        BaseSensorViewBuilderObjectGenerationTest::SetUp();
        sensor_view_builder_.Step(entities_, entities_.front().get());
        gt_ = sensor_view_builder_.GetSensorView().global_ground_truth();
    }

    osi3::GroundTruth gt_;
};

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class GroundSignPoseTestParameterized : public GroundSignBaseTest, public testing::WithParamInterface<mantle_api::Pose>
{
  protected:
    void AddObjectsToMap() override
    {
        auto ground_sign = std::make_shared<map::GroundSign>();
        ground_sign->pose = GetParam();
        ground_sign->id = GetNewId();
        gtgen_map_->traffic_signs.push_back(ground_sign);
    }

    void AddEntities() override
    {
        auto road_marking_entity = std::make_unique<environment::entities::StaticObject>(GetNewId(), "road_marking");
        road_marking_entity->SetPosition(GetParam().position);
        road_marking_entity->SetOrientation(GetParam().orientation);

        auto road_marking_properties = std::make_unique<mantle_ext::RoadMarkingProperties>();
        road_marking_properties->type = mantle_api::EntityType::kStatic;
        road_marking_entity->SetProperties(std::move(road_marking_properties));

        entities_.push_back(std::move(road_marking_entity));
    }
};

TEST_P(GroundSignPoseTestParameterized,
       GivenGroundSignWithSpecificPose_WhenStep_ThenRoadMarkingHasCorrectPositionOrientation)
{
    ASSERT_TRUE(gt_.road_marking_size());

    const auto& actual_road_marking_position = gt_.road_marking(0).base().position();
    const auto& actual_road_marking_orientation = gt_.road_marking(0).base().orientation();

    const mantle_api::Pose ref_pose(GetParam());

    EXPECT_EQ(actual_road_marking_position.x(), ref_pose.position.x.value());
    EXPECT_EQ(actual_road_marking_position.y(), ref_pose.position.y.value());
    EXPECT_EQ(actual_road_marking_position.z(), ref_pose.position.z.value());

    EXPECT_EQ(actual_road_marking_orientation.pitch(), ref_pose.orientation.pitch.value());
    EXPECT_EQ(actual_road_marking_orientation.roll(), ref_pose.orientation.roll.value());
    EXPECT_EQ(actual_road_marking_orientation.yaw(), ref_pose.orientation.yaw.value());

    const auto& actual_entity_sign_position = gt_.road_marking(1).base().position();
    const auto& actual_entity_sign_orientation = gt_.road_marking(1).base().orientation();

    EXPECT_EQ(actual_entity_sign_position.x(), ref_pose.position.x.value());
    EXPECT_EQ(actual_entity_sign_position.y(), ref_pose.position.y.value());
    EXPECT_EQ(actual_entity_sign_position.z(), ref_pose.position.z.value());

    EXPECT_EQ(actual_entity_sign_orientation.pitch(), ref_pose.orientation.pitch.value());
    EXPECT_EQ(actual_entity_sign_orientation.roll(), ref_pose.orientation.roll.value());
    EXPECT_EQ(actual_entity_sign_orientation.yaw(), ref_pose.orientation.yaw.value());
}

INSTANTIATE_TEST_SUITE_P(GroundSignPoseTest,
                         GroundSignPoseTestParameterized,
                         testing::Values(mantle_api::Pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}},
                                         mantle_api::Pose{{0.0_m, 0.0_m, 0.0_m}, {1.0_rad, 2.0_rad, 3.0_rad}},
                                         mantle_api::Pose{{0.0_m, 0.0_m, 0.0_m}, {90.0_deg, 30.0_deg, 180_deg}},
                                         mantle_api::Pose{{1.0_m, 2.0_m, 3.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}},
                                         mantle_api::Pose{{1.0_m, 2.0_m, 3.0_m}, {90.0_deg, 30.0_deg, 180_deg}}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class GroundSignDimensionsTestParameterized : public GroundSignBaseTest,
                                              public testing::WithParamInterface<mantle_api::Dimension3>
{
  protected:
    void AddObjectsToMap() override
    {
        auto ground_sign = std::make_shared<map::GroundSign>();
        ground_sign->dimensions = GetParam();
        ground_sign->id = GetNewId();
        gtgen_map_->traffic_signs.push_back(ground_sign);
    }

    void AddEntities() override
    {
        auto road_marking_entity = std::make_unique<environment::entities::StaticObject>(GetNewId(), "road_marking");

        auto road_marking_properties = std::make_unique<mantle_ext::RoadMarkingProperties>();
        road_marking_properties->type = mantle_api::EntityType::kStatic;
        road_marking_properties->bounding_box.dimension = GetParam();
        road_marking_entity->SetProperties(std::move(road_marking_properties));

        entities_.push_back(std::move(road_marking_entity));
    }
};

TEST_P(GroundSignDimensionsTestParameterized,
       GivenGroundSignWithSpecificDimensions_WhenStep_ThenRoadMarkingHasCorrectDimensions)
{
    ASSERT_TRUE(gt_.road_marking_size());

    const auto& actual_road_marking_dimension = gt_.road_marking(0).base().dimension();

    const mantle_api::Dimension3 ref_dimension(GetParam());

    EXPECT_EQ(actual_road_marking_dimension.length(), ref_dimension.length.value());
    EXPECT_EQ(actual_road_marking_dimension.width(), ref_dimension.width.value());
    EXPECT_EQ(actual_road_marking_dimension.height(), ref_dimension.height.value());

    const auto& actual_entity_sign_dimension = gt_.road_marking(1).base().dimension();

    EXPECT_EQ(actual_entity_sign_dimension.length(), ref_dimension.length.value());
    EXPECT_EQ(actual_entity_sign_dimension.width(), ref_dimension.width.value());
    EXPECT_EQ(actual_entity_sign_dimension.height(), ref_dimension.height.value());
}

INSTANTIATE_TEST_SUITE_P(GroundSignDimensionsTest,
                         GroundSignDimensionsTestParameterized,
                         testing::Values(mantle_api::Dimension3{0.0_m, 0.0_m, 0.0_m},
                                         mantle_api::Dimension3{0.1_m, 0.02_m, 0.3_m}));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class GroundSignMarkingTypeTestParameterized
    : public GroundSignBaseTest,
      public testing::WithParamInterface<std::tuple<osi::OsiRoadMarkingsType, osi3::RoadMarking_Classification_Type>>
{
  protected:
    void AddObjectsToMap() override
    {
        auto ground_sign = std::make_shared<map::GroundSign>();
        ground_sign->marking_type = std::get<0>(GetParam());
        gtgen_map_->traffic_signs.push_back(std::move(ground_sign));
    }

    void AddEntities() override
    {
        auto road_marking_entity = std::make_unique<environment::entities::StaticObject>(GetNewId(), "road_marking");

        auto road_marking_properties = std::make_unique<mantle_ext::RoadMarkingProperties>();
        road_marking_properties->type = mantle_api::EntityType::kStatic;
        road_marking_properties->marking_type = std::get<0>(GetParam());
        road_marking_entity->SetProperties(std::move(road_marking_properties));

        entities_.push_back(std::move(road_marking_entity));
    }
};

TEST_P(GroundSignMarkingTypeTestParameterized,
       GivenGroundSignWithMarkingType_WhenStep_ThenExpectRoadMarkingHasCorrectType)
{
    ASSERT_TRUE(gt_.road_marking_size());

    const auto& actual_road_marking_type = gt_.road_marking(0).classification().type();
    EXPECT_EQ(actual_road_marking_type, std::get<1>(GetParam()));

    const auto& actual_entity_type = gt_.road_marking(1).classification().type();
    EXPECT_EQ(actual_entity_type, std::get<1>(GetParam()));
}

INSTANTIATE_TEST_SUITE_P(
    GroundSignMarkingTypeTest,
    GroundSignMarkingTypeTestParameterized,
    testing::Values(
        std::make_tuple(osi::OsiRoadMarkingsType::kUnknown,
                        osi3::RoadMarking_Classification_Type::RoadMarking_Classification_Type_TYPE_UNKNOWN),
        std::make_tuple(osi::OsiRoadMarkingsType::kOther,
                        osi3::RoadMarking_Classification_Type::RoadMarking_Classification_Type_TYPE_OTHER),
        std::make_tuple(
            osi::OsiRoadMarkingsType::kPaintedTrafficSign,
            osi3::RoadMarking_Classification_Type::RoadMarking_Classification_Type_TYPE_PAINTED_TRAFFIC_SIGN),
        std::make_tuple(
            osi::OsiRoadMarkingsType::kSymbolicTrafficSign,
            osi3::RoadMarking_Classification_Type::RoadMarking_Classification_Type_TYPE_SYMBOLIC_TRAFFIC_SIGN),
        std::make_tuple(
            osi::OsiRoadMarkingsType::kTextualTrafficSign,
            osi3::RoadMarking_Classification_Type::RoadMarking_Classification_Type_TYPE_TEXTUAL_TRAFFIC_SIGN),
        std::make_tuple(osi::OsiRoadMarkingsType::kGenericSymbol,
                        osi3::RoadMarking_Classification_Type::RoadMarking_Classification_Type_TYPE_GENERIC_SYMBOL),
        std::make_tuple(osi::OsiRoadMarkingsType::kGenericLine,
                        osi3::RoadMarking_Classification_Type::RoadMarking_Classification_Type_TYPE_GENERIC_LINE),
        std::make_tuple(osi::OsiRoadMarkingsType::kGenericText,
                        osi3::RoadMarking_Classification_Type::RoadMarking_Classification_Type_TYPE_GENERIC_TEXT)));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class GroundSignMarkingColorTestParameterized
    : public GroundSignBaseTest,
      public testing::WithParamInterface<std::tuple<osi::OsiRoadMarkingColor, osi3::RoadMarking_Classification_Color>>
{
  protected:
    void AddObjectsToMap() override
    {
        auto ground_sign = std::make_shared<map::GroundSign>();
        ground_sign->marking_color = std::get<0>(GetParam());
        gtgen_map_->traffic_signs.push_back(std::move(ground_sign));
    }

    void AddEntities() override
    {
        auto road_marking_entity = std::make_unique<environment::entities::StaticObject>(GetNewId(), "road_marking");

        auto road_marking_properties = std::make_unique<mantle_ext::RoadMarkingProperties>();
        road_marking_properties->type = mantle_api::EntityType::kStatic;
        road_marking_properties->marking_color = std::get<0>(GetParam());
        road_marking_entity->SetProperties(std::move(road_marking_properties));

        entities_.push_back(std::move(road_marking_entity));
    }
};

TEST_P(GroundSignMarkingColorTestParameterized,
       GivenGroundSignWithMarkingColor_WhenStep_ThenExpectRoadMarkingHasCorrectColor)
{
    ASSERT_TRUE(gt_.road_marking_size());

    const auto& actual_road_marking_color = gt_.road_marking(0).classification().monochrome_color();
    EXPECT_EQ(actual_road_marking_color, std::get<1>(GetParam()));

    const auto& actual_entity_color = gt_.road_marking(1).classification().monochrome_color();
    EXPECT_EQ(actual_entity_color, std::get<1>(GetParam()));
}

INSTANTIATE_TEST_SUITE_P(
    GroundSignMarkingColorTest,
    GroundSignMarkingColorTestParameterized,
    testing::Values(
        std::make_tuple(osi::OsiRoadMarkingColor::kUnknown,
                        osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_UNKNOWN),
        std::make_tuple(osi::OsiRoadMarkingColor::kOther,
                        osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_OTHER),
        std::make_tuple(osi::OsiRoadMarkingColor::kWhite,
                        osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_WHITE),
        std::make_tuple(osi::OsiRoadMarkingColor::kYellow,
                        osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_YELLOW),
        std::make_tuple(osi::OsiRoadMarkingColor::kBlue,
                        osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_BLUE),
        std::make_tuple(osi::OsiRoadMarkingColor::kRed,
                        osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_RED),
        std::make_tuple(osi::OsiRoadMarkingColor::kGreen,
                        osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_GREEN),
        std::make_tuple(osi::OsiRoadMarkingColor::kViolet,
                        osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_VIOLET),
        std::make_tuple(osi::OsiRoadMarkingColor::kOrange,
                        osi3::RoadMarking_Classification_Color::RoadMarking_Classification_Color_COLOR_ORANGE)));

class GroundSignFaultyColorTest : public BaseSensorViewBuilderObjectGenerationTest
{
  protected:
    void AddObjectsToMap() override
    {
        auto ground_sign = std::make_shared<map::GroundSign>();
        ground_sign->marking_color = static_cast<osi::OsiRoadMarkingColor>(-1);
        ground_sign->id = GetNewId();
        gtgen_map_->traffic_signs.push_back(ground_sign);
    }
};

TEST_F(GroundSignFaultyColorTest, GivenGroundSignWithNonExistingColor_WhenStep_ThenExpectException)
{
    EXPECT_THROW(sensor_view_builder_.Step(entities_, entities_.front().get()), EnvironmentException);
}

class RoadMarkingEntityFaultyColorTest : public BaseSensorViewBuilderObjectGenerationTest
{
  protected:
    void AddEntities() override
    {
        auto road_marking_entity = std::make_unique<environment::entities::StaticObject>(GetNewId(), "road_marking");

        auto road_marking_properties = std::make_unique<mantle_ext::RoadMarkingProperties>();
        road_marking_properties->type = mantle_api::EntityType::kStatic;
        road_marking_properties->marking_color = static_cast<osi::OsiRoadMarkingColor>(-1);
        road_marking_entity->SetProperties(std::move(road_marking_properties));

        entities_.push_back(std::move(road_marking_entity));
    }
};

TEST_F(RoadMarkingEntityFaultyColorTest, GivenRoadMarkingEntityWithNonExistingColor_WhenStep_ThenExpectException)
{
    EXPECT_THROW(sensor_view_builder_.Step(entities_, entities_.front().get()), EnvironmentException);
}

auto GetGroundSignValueUnitTuples()
{
    static auto traffic_sign_value_units_testing = {
        std::make_tuple(osi::OsiTrafficSignValueUnit::kUnknown,
                        osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_UNKNOWN),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kOther,
                        osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_OTHER),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kNoUnit,
                        osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_NO_UNIT),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kKilometerPerHour,
                        osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_KILOMETER_PER_HOUR),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kMilePerHour,
                        osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_MILE_PER_HOUR),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kMeter,
                        osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_METER),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kKilometer,
                        osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_KILOMETER),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kFeet,
                        osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_FEET),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kMile,
                        osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_MILE),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kMetricTon,
                        osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_METRIC_TON),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kLongTon,
                        osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_LONG_TON),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kShortTon,
                        osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_SHORT_TON),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kMinutes,
                        osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_MINUTES),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kDay,
                        osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_DAY),
        std::make_tuple(osi::OsiTrafficSignValueUnit::kPercentage,
                        osi3::TrafficSignValue_Unit::TrafficSignValue_Unit_UNIT_PERCENTAGE)};

    return traffic_sign_value_units_testing;
}

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class GroundSignValueTestParameterized
    : public GroundSignBaseTest,
      public testing::WithParamInterface<std::tuple<osi::OsiTrafficSignValueUnit, osi3::TrafficSignValue_Unit>>
{
  protected:
    void AddObjectsToMap() override
    {
        map::SignValueInformation value_information;
        value_information.text = "foo";
        value_information.value = 120.0;
        value_information.value_unit = std::get<0>(GetParam());

        auto ground_sign = std::make_shared<map::GroundSign>();
        ground_sign->value_information = value_information;
        ground_sign->id = GetNewId();
        gtgen_map_->traffic_signs.push_back(ground_sign);
    }

    void AddEntities() override
    {
        entities_.push_back(std::make_unique<environment::entities::StaticObject>(GetNewId(), "road_marking"));
        auto road_marking_properties = std::make_unique<mantle_ext::RoadMarkingProperties>();
        road_marking_properties->type = mantle_api::EntityType::kStatic;
        road_marking_properties->text = "foo";
        road_marking_properties->value = 120.0;
        road_marking_properties->unit = std::get<0>(GetParam());
        entities_.back()->SetProperties(std::move(road_marking_properties));
    }
};

TEST_P(GroundSignValueTestParameterized, GivenGroundSignWithSpecificValue_WhenStep_ThenExpectRoadMarkingHasCorrectValue)
{
    ASSERT_TRUE(gt_.road_marking_size());

    const auto& actual_ground_sign_value = gt_.road_marking(0).classification().value();

    EXPECT_EQ(actual_ground_sign_value.text(), "foo");
    EXPECT_EQ(gt_.road_marking(0).classification().value_text(), "foo");
    EXPECT_EQ(actual_ground_sign_value.value(), 120.0);
    EXPECT_EQ(actual_ground_sign_value.value_unit(), std::get<1>(GetParam()));

    const auto& actual_entity_value = gt_.road_marking(1).classification().value();

    EXPECT_EQ(actual_entity_value.text(), "foo");
    EXPECT_EQ(gt_.road_marking(1).classification().value_text(), "foo");
    EXPECT_EQ(actual_entity_value.value(), 120.0);
    EXPECT_EQ(actual_entity_value.value_unit(), std::get<1>(GetParam()));
}

INSTANTIATE_TEST_SUITE_P(GroundSignValueTest,
                         GroundSignValueTestParameterized,
                         testing::ValuesIn(GetGroundSignValueUnitTuples()));

// NOLINTNEXTLINE(fuchsia-multiple-inheritance)
class GroundSignAssignedLaneIdTestParameterized : public GroundSignBaseTest,
                                                  public testing::WithParamInterface<std::vector<mantle_api::UniqueId>>
{
  protected:
    void AddObjectsToMap() override
    {
        auto ground_sign = std::make_shared<map::GroundSign>();
        ground_sign->assigned_lanes = GetParam();
        ground_sign->id = GetNewId();
        gtgen_map_->traffic_signs.push_back(ground_sign);
    }

    void AddEntities() override
    {
        auto road_marking_entity = std::make_unique<environment::entities::StaticObject>(GetNewId(), "road_marking");
        road_marking_entity->SetAssignedLaneIds(GetParam());

        auto road_marking_properties = std::make_unique<mantle_ext::RoadMarkingProperties>();
        road_marking_properties->type = mantle_api::EntityType::kStatic;
        road_marking_entity->SetProperties(std::move(road_marking_properties));

        entities_.push_back(std::move(road_marking_entity));
    }
};

TEST_P(GroundSignAssignedLaneIdTestParameterized,
       GivenGroundSignWithAssignedLanes_WhenStep_ThenExpectRoadMarkingHasCorrectAssignedLanes)
{
    ASSERT_TRUE(gt_.road_marking_size());

    const auto& actual_road_marking_classification = gt_.road_marking(0).classification();
    const auto& actual_entity_classification = gt_.road_marking(0).classification();

    auto expected_lane_ids = GetParam();
    ASSERT_EQ(expected_lane_ids.size(), actual_road_marking_classification.assigned_lane_id_size());
    ASSERT_EQ(expected_lane_ids.size(), actual_entity_classification.assigned_lane_id_size());

    for (std::size_t i = 0; i < expected_lane_ids.size(); i++)
    {
        EXPECT_EQ(expected_lane_ids[i],
                  actual_road_marking_classification.assigned_lane_id(static_cast<int>(i)).value());
        EXPECT_EQ(expected_lane_ids[i], actual_entity_classification.assigned_lane_id(static_cast<int>(i)).value());
    }
}

INSTANTIATE_TEST_SUITE_P(GroundSignAssignedLaneIdTest,
                         GroundSignAssignedLaneIdTestParameterized,
                         testing::Values(std::vector<mantle_api::UniqueId>{0, 1},
                                         std::vector<mantle_api::UniqueId>{1, 0},
                                         std::vector<mantle_api::UniqueId>{0},
                                         std::vector<mantle_api::UniqueId>{1}));

class GroundSignMainTypeTest : public GroundSignBaseTest
{
  protected:
    void AddObjectsToMap() override
    {
        auto ground_sign = std::make_shared<map::GroundSign>();
        ground_sign->type = osi::OsiTrafficSignType::kZebraCrossing;
        ground_sign->id = GetNewId();
        gtgen_map_->traffic_signs.push_back(ground_sign);
    }

    void AddEntities() override
    {
        auto road_marking_entity = std::make_unique<environment::entities::StaticObject>(GetNewId(), "road_marking");

        auto road_marking_properties = std::make_unique<mantle_ext::RoadMarkingProperties>();
        road_marking_properties->type = mantle_api::EntityType::kStatic;
        road_marking_properties->sign_type = osi::OsiTrafficSignType::kZebraCrossing;
        road_marking_entity->SetProperties(std::move(road_marking_properties));

        entities_.push_back(std::move(road_marking_entity));
    }
};

TEST_F(GroundSignMainTypeTest, GivenGroundSignWithMainSignType_WhenStep_ThenExpectRoadMarkingHasCorrectMainSignType)
{
    ASSERT_TRUE(gt_.road_marking_size());

    const auto& actual_road_marking = gt_.road_marking(0);
    const auto& actual_entity = gt_.road_marking(1);

    EXPECT_EQ(
        actual_road_marking.classification().traffic_main_sign_type(),
        osi3::TrafficSign_MainSign_Classification_Type::TrafficSign_MainSign_Classification_Type_TYPE_ZEBRA_CROSSING);
    EXPECT_EQ(
        actual_entity.classification().traffic_main_sign_type(),
        osi3::TrafficSign_MainSign_Classification_Type::TrafficSign_MainSign_Classification_Type_TYPE_ZEBRA_CROSSING);
}

}  // namespace gtgen::core::environment::proto_groundtruth
