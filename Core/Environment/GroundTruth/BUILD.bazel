cc_library(
    name = "sensor_view_builder",
    srcs = ["sensor_view_builder.cpp"],
    hdrs = ["sensor_view_builder.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Communication/Dispatchers:__subpackages__",
        "//Core/Environment/Controller:__subpackages__",
        "//Core/Environment/GtGenEnvironment:__subpackages__",
    ],
    deps = [
        "//Core/Environment/GroundTruth:sensor_view_sanity_checker",
        "//Core/Environment/GroundTruth/Internal/Dynamic:dynamic_proto_ground_truth_builder",
        "//Core/Environment/GroundTruth/Internal/Environment:environment_proto_ground_truth_builder",
        "//Core/Environment/GroundTruth/Internal/Static:static_proto_ground_truth_builder",
        "//Core/Service/UserSettings:user_settings",
    ],
)

cc_test(
    name = "sensor_view_builder_dynamic_object_test",
    timeout = "short",
    srcs = ["sensor_view_builder_dynamic_object_test.cpp"],
    deps = [
        ":sensor_view_builder",
        ":sensor_view_builder_test_common",
        "//Core/Environment/GroundTruth/Internal/Dynamic:dynamic_proto_ground_truth_builder",
        "@googletest//:gtest_main",
        "@units_nhh",
    ],
)

cc_test(
    name = "sensor_view_builder_ground_sign_test",
    timeout = "short",
    srcs = ["sensor_view_builder_ground_sign_test.cpp"],
    deps = [
        ":sensor_view_builder",
        ":sensor_view_builder_test_common",
        "@googletest//:gtest_main",
        "@units_nhh",
    ],
)

cc_test(
    name = "sensor_view_builder_stationary_object_test",
    timeout = "short",
    srcs = ["sensor_view_builder_stationary_object_test.cpp"],
    deps = [
        ":sensor_view_builder",
        ":sensor_view_builder_test_common",
        "@googletest//:gtest_main",
        "@mantle_api",
        "@units_nhh",
    ],
)

cc_test(
    name = "sensor_view_builder_test",
    timeout = "short",
    srcs = ["sensor_view_builder_test.cpp"],
    deps = [
        ":sensor_view_builder",
        "//Core/Service/Utility:clock",
        "//Core/Tests/TestUtils:exception_testing",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "//Core/Tests/TestUtils/ProtoUtils:proto_utilities",
        "@googletest//:gtest_main",
        "@units_nhh",
    ],
)

cc_library(
    name = "sensor_view_builder_test_common",
    testonly = True,
    hdrs = ["sensor_view_builder_test_common.h"],
    deps = ["//Core/Tests/TestUtils/MapCatalogue:map_catalogue"],
)

cc_test(
    name = "sensor_view_builder_traffic_light_test",
    timeout = "short",
    srcs = ["sensor_view_builder_traffic_light_test.cpp"],
    deps = [
        ":sensor_view_builder",
        ":sensor_view_builder_test_common",
        "@googletest//:gtest_main",
        "@units_nhh",
    ],
)

cc_test(
    name = "sensor_view_builder_traffic_sign_test",
    timeout = "short",
    srcs = ["sensor_view_builder_traffic_sign_test.cpp"],
    deps = [
        ":sensor_view_builder",
        ":sensor_view_builder_test_common",
        "@googletest//:gtest_main",
        "@units_nhh",
    ],
)

cc_library(
    name = "sensor_view_sanity_checker",
    srcs = ["sensor_view_sanity_checker.cpp"],
    hdrs = ["sensor_view_sanity_checker.h"],
    visibility = ["//Core/Environment/GroundTruth:__subpackages__"],
    deps = [
        "//Core/Environment/Chunking:chunking",
        "//Core/Environment/Entities:entities",
        "//Core/Environment/GroundTruth/Internal/Static:static_proto_ground_truth_builder",
        "//Core/Environment/Map/GtGenMap:gtgenmap",
    ],
)

cc_test(
    name = "sensor_view_sanity_checker_test",
    timeout = "short",
    srcs = ["sensor_view_sanity_checker_test.cpp"],
    deps = [
        ":sensor_view_builder",
        ":sensor_view_sanity_checker",
        "//Core/Service/Utility:clock",
        "//Core/Tests/TestUtils:exception_testing",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "//Core/Tests/TestUtils/ProtoUtils:proto_utilities",
        "@googletest//:gtest_main",
        "@units_nhh",
    ],
)
