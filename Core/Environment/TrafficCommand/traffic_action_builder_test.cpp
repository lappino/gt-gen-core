/*******************************************************************************
 * Copyright (c) 2022-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/TrafficCommand/traffic_action_builder.h"

#include "Core/Environment/Entities/vehicle_entity.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"

#include <MantleAPI/Common/i_identifiable.h>
#include <gtest/gtest.h>

namespace gtgen::core::environment::traffic_command
{
using units::literals::operator""_mps;
using units::literals::operator""_m;

class TrafficActionBuilderTest : public testing::Test
{
  protected:
    entities::VehicleEntity vehicle_entity_{0, "host"};
    std::unique_ptr<map::GtGenMap> gtgen_map_{test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints()};
    map::LaneLocationProvider lane_location_provider_{*gtgen_map_};
};

TEST_F(TrafficActionBuilderTest, GivenUnsupportedControlStrategy_WhenCallConvertToTrafficAction_ThenNoValueReturned)
{
    mantle_api::KeepVelocityControlStrategy control_strategy;

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy);

    EXPECT_FALSE(result.has_value());
}

TEST_F(TrafficActionBuilderTest,
       GivenFollowVelocitySplineControlStrategy_WhenCallConvertToTrafficAction_ThenTrafficActionWithSpeedActionReturned)
{
    mantle_api::FollowVelocitySplineControlStrategy control_strategy;
    control_strategy.default_value = 10_mps;

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy);

    ASSERT_TRUE(result.has_value());
    ASSERT_TRUE(result.value().has_speed_action());
    EXPECT_EQ(control_strategy.default_value(), result.value().speed_action().absolute_target_speed());
}

TEST_F(
    TrafficActionBuilderTest,
    GivenPerformLaneChangeControlStrategy_WhenCallConvertToTrafficAction_ThenTrafficActionWithLaneChangeActionReturned)
{
    mantle_api::PerformLaneChangeControlStrategy control_strategy;
    control_strategy.target_lane_id = -3;  // shift 1 lane right

    vehicle_entity_.SetPosition(mantle_api::Vec3<units::length::meter_t>{0.0_m, 4.0_m, 0.0_m});  // on local lane -2

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy);

    ASSERT_TRUE(result.has_value());
    ASSERT_TRUE(result.value().has_lane_change_action());
    EXPECT_EQ(1, result.value().lane_change_action().relative_target_lane());
}

TEST_F(
    TrafficActionBuilderTest,
    GivenAcquireLaneOffsetControlStrategy_WhenCallConvertToTrafficAction_ThenTrafficActionWithLaneOffsetActionReturned)
{
    mantle_api::AcquireLaneOffsetControlStrategy control_strategy;
    control_strategy.offset = 1_m;

    auto result =
        TrafficActionBuilder::ConvertToTrafficAction(vehicle_entity_, &lane_location_provider_, &control_strategy);

    ASSERT_TRUE(result.has_value());
    ASSERT_TRUE(result.value().has_lane_offset_action());
    EXPECT_EQ(control_strategy.offset(), result.value().lane_offset_action().target_lane_offset());
}

TEST_F(TrafficActionBuilderTest,
       GivenCommandTypeAndCommand_WhenCallCreateCustomTrafficAction_ThenTrafficActionWithCustomActionReturned)
{
    std::string command_type = "route";
    std::string command = "5,6,7,8";
    auto result = TrafficActionBuilder::CreateCustomTrafficAction(command_type, command);

    ASSERT_TRUE(result.has_custom_action());
    EXPECT_EQ(command_type, result.custom_action().command_type());
    EXPECT_EQ(command, result.custom_action().command());
}

}  // namespace gtgen::core::environment::traffic_command
