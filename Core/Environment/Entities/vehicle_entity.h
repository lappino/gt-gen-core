/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_ENTITIES_VEHICLEENTITY_H
#define GTGEN_CORE_ENVIRONMENT_ENTITIES_VEHICLEENTITY_H

#include "Core/Environment/Entities/Internal/base_entity.h"

namespace gtgen::core::environment::entities
{

struct WheelStates
{
    double front_right_mue{1.0};
    double front_left_mue{1.0};
    double rear_right_mue{1.0};
    double rear_left_mue{1.0};
};

class VehicleEntity : public BaseEntity, public mantle_api::IVehicle
{
  public:
    VehicleEntity(mantle_api::UniqueId id, const std::string& name);

    mantle_api::VehicleProperties* GetProperties() const override;

    void SetIndicatorState(mantle_api::IndicatorState state) override;
    mantle_api::IndicatorState GetIndicatorState() const override;

    WheelStates GetWheelStates() const;
    void SetWheelStates(WheelStates wheel_states);

  private:
    mantle_api::IndicatorState state_{mantle_api::IndicatorState::kOff};
    WheelStates wheel_states_;
};

}  // namespace gtgen::core::environment::entities

#endif  // GTGEN_CORE_ENVIRONMENT_ENTITIES_VEHICLEENTITY_H
