/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/Entities/vehicle_entity.h"

namespace gtgen::core::environment::entities
{

VehicleEntity::VehicleEntity(mantle_api::UniqueId id, const std::string& name) : BaseEntity(id, name) {}

mantle_api::VehicleProperties* VehicleEntity::GetProperties() const
{
    return GetPropertiesAs<mantle_api::VehicleProperties>();
}

void VehicleEntity::SetIndicatorState(mantle_api::IndicatorState state)
{
    state_ = state;
}

mantle_api::IndicatorState VehicleEntity::GetIndicatorState() const
{
    return state_;
}

WheelStates VehicleEntity::GetWheelStates() const
{
    return wheel_states_;
}

void VehicleEntity::SetWheelStates(WheelStates wheel_states)
{
    wheel_states_ = wheel_states;
}

}  // namespace gtgen::core::environment::entities
