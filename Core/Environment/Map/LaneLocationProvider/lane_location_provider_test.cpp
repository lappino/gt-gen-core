/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/Geometry/bounding_box.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map_finalizer.h"
#include "Core/Service/GlmWrapper/glm_basic_vector_utils.h"
#include "Core/Tests/TestUtils/MapCatalogue/lane_group_catalogue.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue.h"
#include "Core/Tests/TestUtils/MapCatalogue/map_catalogue_id_provider.h"
#include "Core/Tests/TestUtils/MapCatalogue/raw_map_builder.h"
#include "Core/Tests/TestUtils/expect_extensions.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <units.h>

#include <cmath>
#include <cstdint>

namespace gtgen::core::environment::map
{
using units::literals::operator""_m;
using units::literals::operator""_rad;
using units::literals::operator""_deg;

class LaneLocationProviderTest : public testing::Test
{
  protected:
    void CheckEqualLaneLocation(const LaneLocation& expected_lane_location, const LaneLocation& actual_lane_location)
    {
        ASSERT_EQ(expected_lane_location.lanes, actual_lane_location.lanes);
        EXPECT_EQ(expected_lane_location.centerline_point_index, actual_lane_location.centerline_point_index);
        EXPECT_TRIPLE(expected_lane_location.projected_centerline_point,
                      actual_lane_location.projected_centerline_point);
        EXPECT_TRIPLE(expected_lane_location.direction, actual_lane_location.direction);
        EXPECT_TRIPLE(expected_lane_location.lane_normal, actual_lane_location.lane_normal);
    }
};

TEST_F(LaneLocationProviderTest, GivenPositionOnRoadSurface_WhenShiftingUpwards_ThenZValueOfPositionCorrect)
{
    test_utils::RawMapBuilder builder;
    builder.AddNewLaneGroup().AddStraightEastingLaneWithTwoPoints({0_m, 0_m, 1_m});

    auto gtgen_map = builder.Build();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    auto position_on_the_road = lane_location_provider.GetUpwardsShiftedLanePosition({0.0_m, 1.0_m, 0.0_m}, 3.3);

    EXPECT_EQ(0.0, position_on_the_road.x());
    EXPECT_EQ(1.0, position_on_the_road.y());
    EXPECT_EQ(4.3, position_on_the_road.z());
}

TEST_F(LaneLocationProviderTest, GivenPositionNotOnRoadSurface_WhenShiftingUpwards_ThenExceptionThrown)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(3, 1);

    LaneLocationProvider lane_location_provider{*gtgen_map};

    EXPECT_THROW(lane_location_provider.GetUpwardsShiftedLanePosition({0_m, 42_m, 0.0_m}, 3.3), EnvironmentException);
}

TEST_F(LaneLocationProviderTest, GivenPositionOnRoad_WhenIsPositionOnLane_ThenReturnTrue)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(3, 1);

    LaneLocationProvider lane_location_provider{*gtgen_map};

    EXPECT_TRUE(lane_location_provider.IsPositionOnLane({1_m, 0_m, 0.0_m}));
}

TEST_F(LaneLocationProviderTest, GivenPositionNotOnRoad_WhenIsPositionOnLane_ThenReturnFalse)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(3, 1);

    LaneLocationProvider lane_location_provider{*gtgen_map};

    EXPECT_FALSE(lane_location_provider.IsPositionOnLane({0_m, 42_m, 0.0_m}));
}

TEST_F(LaneLocationProviderTest, GivenPositionOnRoad_WhenGetLaneOrientation_ThenOrientationReturned)
{
    mantle_api::Orientation3<units::angle::radian_t> expected_orientation{
        units::angle::radian_t{M_PI_4}, 0.0_rad, 0.0_rad};
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneNorthEastingLaneWithNPoints(3, 1);

    LaneLocationProvider lane_location_provider{*gtgen_map};

    EXPECT_TRIPLE(expected_orientation, lane_location_provider.GetLaneOrientation({1_m, 1_m, 0.0_m}));
}

TEST_F(LaneLocationProviderTest, GivenPositionNotOnRoad_WhenGetLaneOrientation_ThenExceptionThrown)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(3, 1);

    LaneLocationProvider lane_location_provider{*gtgen_map};

    EXPECT_THROW(lane_location_provider.GetLaneOrientation({0_m, 42_m, 0.0_m}), EnvironmentException);
}

TEST_F(LaneLocationProviderTest, GivenPositionContainedByLane_WhenFindLanes_ThenOneLaneIsReturned)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithOne6kmEastingLaneWithTwoPoints();
    auto lane_id = gtgen_map->GetLanes().front().id;

    LaneLocationProvider lane_location_provider{*gtgen_map};

    std::vector<const Lane*> lanes = lane_location_provider.FindLanes({0_m, 0_m, 0_m});

    ASSERT_FALSE(lanes.empty());
    EXPECT_EQ(lane_id, lanes.front()->id);
}

TEST_F(LaneLocationProviderTest, GivenPositionContainedByLane_WhenGetLaneIdsAtPosition_ThenOneLaneIdIsReturned)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithOne6kmEastingLaneWithTwoPoints();
    auto lane_id = gtgen_map->GetLanes().front().id;

    LaneLocationProvider lane_location_provider{*gtgen_map};

    std::vector<mantle_api::UniqueId> lanes = lane_location_provider.GetLaneIdsAtPosition({0_m, 0_m, 0_m});

    ASSERT_EQ(1, lanes.size());
    EXPECT_EQ(lane_id, lanes.front());
}

TEST_F(LaneLocationProviderTest, GivenPositionContainedByTwoLanes_WhenGetLaneIdsAtPosition_ThenTwoLaneIdsAreReturned)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithTwoOverlappingEastingLaneWithHundredPointsEach();

    LaneLocationProvider lane_location_provider{*gtgen_map};

    std::vector<mantle_api::UniqueId> lanes = lane_location_provider.GetLaneIdsAtPosition({0_m, 0_m, 0_m});

    ASSERT_EQ(2, lanes.size());
}

TEST_F(LaneLocationProviderTest, GivenPositionOffOfRoad_WhenGetLaneIdsAtPosition_ThenNoLaneIdsReturned)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithOne6kmEastingLaneWithTwoPoints();

    LaneLocationProvider lane_location_provider{*gtgen_map};

    std::vector<mantle_api::UniqueId> lanes = lane_location_provider.GetLaneIdsAtPosition({-100_m, 0_m, 0_m});

    ASSERT_TRUE(lanes.empty());
}

TEST_F(LaneLocationProviderTest, GivenPositionNotContainedByAnyLane_WhenFindLanes_ThenReturnedLaneVectorIsEmpty)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithOne6kmEastingLaneWithTwoPoints();

    LaneLocationProvider lane_location_provider{*gtgen_map};

    std::vector<const Lane*> lanes = lane_location_provider.FindLanes({-10_m, -10_m, 0_m});

    EXPECT_TRUE(lanes.empty());
}

TEST_F(LaneLocationProviderTest,
       GivenPositionBeforeLastCenterLinePoint_WhenGetLaneLocation_ThenReturnedLocationWillBeBeforeLastCenterLinePoint)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(5, 1);
    auto lane_id = gtgen_map->GetLanes().front().id;

    LaneLocation expected_lane_location{};
    expected_lane_location.lanes = {gtgen_map->FindLane(lane_id)};
    expected_lane_location.projected_centerline_point = {3.9_m, 0.0_m, 0.0_m};
    expected_lane_location.direction = {1.0_m, 0.0_m, 0.0_m};
    expected_lane_location.lane_normal = {0.0_m, 0.0_m, 1.0_m};
    expected_lane_location.centerline_point_index = 3;

    LaneLocationProvider lane_location_provider{*gtgen_map};

    const LaneLocation lane_location =
        lane_location_provider.GetLaneLocation(mantle_api::Vec3<units::length::meter_t>{3.9_m, 0.4_m, 0_m});

    ASSERT_TRUE(lane_location.IsValid());
    CheckEqualLaneLocation(expected_lane_location, lane_location);
}

TEST_F(LaneLocationProviderTest,
       GivenPositionThatIsAfterLastCenterLinePoint_WhenGetLaneLocation_ThenReturnedLocationWillBeAtLastCenterLinePoint)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(5, 1);

    // Remove the last centerline point at 4;0
    auto lane_id = gtgen_map->GetLanes().front().id;
    auto& center_line_to_modify = gtgen_map->GetLane(lane_id).center_line;
    center_line_to_modify.erase(center_line_to_modify.end() - 1);

    LaneLocation expected_lane_location{};
    expected_lane_location.lanes = {gtgen_map->FindLane(lane_id)};
    expected_lane_location.projected_centerline_point = {3.0_m, 0.0_m, 0.0_m};
    expected_lane_location.direction = {1.0_m, 0.0_m, 0.0_m};
    expected_lane_location.lane_normal = {0.0_m, 0.0_m, 1.0_m};
    expected_lane_location.centerline_point_index = 3;

    LaneLocationProvider lane_location_provider{*gtgen_map};

    const LaneLocation lane_location =
        lane_location_provider.GetLaneLocation(mantle_api::Vec3<units::length::meter_t>{4.0_m, 0.4_m, 0_m});

    ASSERT_TRUE(lane_location.IsValid());
    CheckEqualLaneLocation(expected_lane_location, lane_location);
}

TEST_F(
    LaneLocationProviderTest,
    GivenPositionThatIsBeforeFirstCenterLinePoint_WhenGetLaneLocation_ThenReturnedLocationWillBeAtFirstCenterLinePoint)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(5, 1);

    // Remove the first centerline point at 0,0,0
    auto lane_id = gtgen_map->GetLanes().front().id;
    auto& center_line_to_modify = gtgen_map->GetLane(lane_id).center_line;
    center_line_to_modify.erase(center_line_to_modify.begin());

    LaneLocation expected_lane_location{};
    expected_lane_location.lanes = {gtgen_map->FindLane(lane_id)};
    expected_lane_location.projected_centerline_point = {1.0_m, 0.0_m, 0.0_m};
    expected_lane_location.direction = {1.0_m, 0.0_m, 0.0_m};
    expected_lane_location.lane_normal = {0.0_m, 0.0_m, 1.0_m};
    expected_lane_location.centerline_point_index = 0;

    LaneLocationProvider lane_location_provider{*gtgen_map};

    const LaneLocation lane_location =
        lane_location_provider.GetLaneLocation(mantle_api::Vec3<units::length::meter_t>{0.0_m, 0.0_m, 0_m});

    ASSERT_TRUE(lane_location.IsValid());
    CheckEqualLaneLocation(expected_lane_location, lane_location);
}

TEST_F(LaneLocationProviderTest, GivenMapWithNoLanes_WhenGetLaneLocation_ThenTheReturnedLaneLocationIsInvalid)
{
    map::GtGenMap map;
    map.axis_aligned_world_bounding_box = BoundingBox2d{{0.0, 0.0}, {1.0, 1.0}};

    GtGenMapFinalizer finalizer(map);
    finalizer.Finalize();

    const LaneLocation expected_lane_location{};

    LaneLocationProvider lane_location_provider{map};
    const LaneLocation lane_location =
        lane_location_provider.GetLaneLocation(mantle_api::Vec3<units::length::meter_t>{0_m, 0_m, 0_m});

    EXPECT_FALSE(lane_location.IsValid());
    CheckEqualLaneLocation(expected_lane_location, lane_location);
}

TEST_F(LaneLocationProviderTest, GivenCurvyLane_WhenGetLaneLocation_ThenLocationWillBeCalculatedCorrectly)
{
    auto gtgen_map = test_utils::MapCatalogue::MapOneLaneNorthingToWestingCurve();
    auto lane_id = gtgen_map->GetLanes().front().id;

    LaneLocation expected_lane_location{};
    expected_lane_location.lanes = {gtgen_map->FindLane(lane_id)};
    expected_lane_location.projected_centerline_point = {-3_m, 6_m, 0.0_m};
    expected_lane_location.direction = {-2.0_m / std::sqrt(5.0), -1.0_m / std::sqrt(5.0), 0.0_m};
    expected_lane_location.lane_normal = {0.0_m, 0.0_m, 1.0_m};
    expected_lane_location.centerline_point_index = 3;

    LaneLocationProvider lane_location_provider{*gtgen_map};

    const LaneLocation lane_location =
        lane_location_provider.GetLaneLocation(mantle_api::Vec3<units::length::meter_t>{-3_m, 6_m, 20.0_m});

    ASSERT_TRUE(lane_location.IsValid());
    CheckEqualLaneLocation(expected_lane_location, lane_location);
}

TEST_F(LaneLocationProviderTest,
       GivenLaneWithASuccessorAndPositionAtLastCenterlinePointOfLane_WhenGetLaneLocation_ThenBothLanesAreReturned)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithTwoConnectedEastingLanesWithHundredPointsEach();
    auto lane_id = gtgen_map->GetLanes().front().id;
    auto lane_id2 = gtgen_map->GetLanes().back().id;

    LaneLocation expected_lane_location{};
    expected_lane_location.lanes = {gtgen_map->FindLane(lane_id), gtgen_map->FindLane(lane_id2)};
    expected_lane_location.projected_centerline_point = {99_m, 0.0_m, 0.0_m};
    expected_lane_location.direction = {1.0_m, 0.0_m, 0.0_m};
    expected_lane_location.lane_normal = service::glmwrapper::Normalize({0_m, 0_m, 1_m});
    expected_lane_location.centerline_point_index = 99;

    LaneLocationProvider lane_location_provider{*gtgen_map};

    const LaneLocation lane_location =
        lane_location_provider.GetLaneLocation(mantle_api::Vec3<units::length::meter_t>{99.0_m, 0.0_m, 0.0_m});

    ASSERT_TRUE(lane_location.IsValid());
    CheckEqualLaneLocation(expected_lane_location, lane_location);
}

/// @test Tilted lane
///
/// relevant points are:
///    lane: (6:2) -> (8:1)
///  lookup-position: (7.25:0.75)
/// --> closest point on lane (7.5:1.25)
/// "leaning right" means normal is normalize({0:-1:1})
TEST_F(LaneLocationProviderTest, GivenRightLeaningLane_WhenGetLaneLocation_ThenLaneLocationWillBeCalculatedCorrectly)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPointsRolledRight(10, 1);
    auto lane_id = gtgen_map->GetLanes().front().id;

    LaneLocation expected_lane_location{};
    expected_lane_location.lanes = {gtgen_map->FindLane(lane_id)};
    expected_lane_location.projected_centerline_point = {7.5_m, 0.0_m, 2.0_m};
    expected_lane_location.direction = {1.0_m, 0.0_m, 0.0_m};
    expected_lane_location.lane_normal = service::glmwrapper::Normalize({0.0_m, -1.0_m, 1.0_m});
    expected_lane_location.centerline_point_index = 7;

    LaneLocationProvider lane_location_provider{*gtgen_map};

    const LaneLocation lane_location =
        lane_location_provider.GetLaneLocation(mantle_api::Vec3<units::length::meter_t>{7.5_m, 0.75_m, 20.0_m});

    ASSERT_TRUE(lane_location.IsValid());
    CheckEqualLaneLocation(expected_lane_location, lane_location);
}

TEST_F(LaneLocationProviderTest,
       GivenLaneWithDoubleLeftBoundaries_WhenGetLaneLocation_ThenLaneLocationWillBeCalculatedCorrectly)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithHundredPointsAndDoubleLeftBoundary();
    auto lane_id = gtgen_map->GetLanes().front().id;

    LaneLocation expected_lane_location{};
    expected_lane_location.lanes = {gtgen_map->FindLane(lane_id)};
    expected_lane_location.projected_centerline_point = {97.5_m, 0.0_m, 0.0_m};
    expected_lane_location.direction = {1.0_m, 0.0_m, 0.0_m};
    expected_lane_location.lane_normal = {0.0_m, 0.0_m, 1.0_m};
    expected_lane_location.centerline_point_index = 97;

    LaneLocationProvider lane_location_provider{*gtgen_map};

    const LaneLocation lane_location =
        lane_location_provider.GetLaneLocation(mantle_api::Vec3<units::length::meter_t>{97.5_m, 9.0_m, 20.0_m});

    ASSERT_TRUE(lane_location.IsValid());
    CheckEqualLaneLocation(expected_lane_location, lane_location);
}

/// @test Tilted lane
///
/// relevant points are:
///    lane: (6:2) -> (8:1)
///  lookup-position: (7.25:0.75)
/// --> closest point on lane (7.5:1.25)
/// "leaning left" means normal is normalize({0:1:1})
TEST_F(LaneLocationProviderTest, GivenLeftLeaningLane_WhenGetLaneLocation_ThenLaneLocationWillBeCalculatedCorrectly)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPointsRolledLeft(10, 1);
    auto lane_id = gtgen_map->GetLanes().front().id;

    LaneLocation expected_lane_location{};
    expected_lane_location.lanes = {gtgen_map->FindLane(lane_id)};
    expected_lane_location.projected_centerline_point = {7.5_m, 0.0_m, 0.0_m};
    expected_lane_location.direction = {1.0_m, 0.0_m, 0.0_m};
    expected_lane_location.lane_normal = service::glmwrapper::Normalize({0.0_m, 1.0_m, 1.0_m});
    expected_lane_location.centerline_point_index = 7;

    LaneLocationProvider lane_location_provider{*gtgen_map};

    const LaneLocation lane_location =
        lane_location_provider.GetLaneLocation(mantle_api::Vec3<units::length::meter_t>{7.5_m, 0.75_m, 20.0_m});

    ASSERT_TRUE(lane_location.IsValid());
    CheckEqualLaneLocation(expected_lane_location, lane_location);
}

TEST_F(LaneLocationProviderTest,
       GivenSecondLaneContainsNonExistingSuccessor_WhenGetLaneLocation_ThenDirectionOfFirstLaneWithoutSuccessor)
{
    auto gtgen_map = test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanesEachWithoutSuccessors();
    auto& lane_with_not_existing_successor = gtgen_map->GetLane(9);
    lane_with_not_existing_successor.successors.push_back(42);

    LaneLocationProvider lane_location_provider{*gtgen_map};
    const auto lane_location = lane_location_provider.GetLaneLocation({105_m, 0_m, 0_m});

    // Direction is oriented to x-axis
    EXPECT_TRIPLE(lane_location.direction, mantle_api::Vec3<units::length::meter_t>(1_m, 0.0_m, 0.0_m));
}

TEST_F(
    LaneLocationProviderTest,
    GivenLaneWithMultipleSuccessors_WhenGettingCachedSuccessorLane_ThenTheSuccessorLaneWhichHasSuccessorWillBeReturned)
{
    /// A successor lane with a successor exist (lane 1)
    ///
    /// @verbatim
    /*
    ///
    ///      -> 1 -> 3
    ///     /
    /// 0 ->
    ///     \
    ///      -> 2
    ///
    */
    /// @endverbatim

    auto gtgen_map = test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanes();
    auto expected_lane_id = gtgen_map->GetLanes()[0].id;
    auto expected_successor_id = gtgen_map->GetLanes()[1].id;

    LaneLocationProvider lane_location_provider{*gtgen_map};

    auto start_lane = lane_location_provider.FindLane(expected_lane_id);
    ASSERT_NE(nullptr, start_lane);
    EXPECT_EQ(start_lane->id, expected_lane_id);

    const Lane* successor_lane = lane_location_provider.GetCachedSuccessorLane(start_lane);

    ASSERT_NE(nullptr, successor_lane);
    EXPECT_EQ(successor_lane->id, expected_successor_id);
}

TEST_F(
    LaneLocationProviderTest,
    GivenThatNoSuccessorLaneWithSuccessorExists_WhenGettingCachedSuccessorLane_ThenTheLastSuccessorLaneWillBeReturned)
{
    /// No successor lane with a successor exist, so the last successor lane (2) is returned
    ///
    /// @verbatim
    /*
    ///      -> 1
    ///     /
    /// 0 ->
    ///     \
    ///      -> 2
    */
    /// @endverbatim

    auto gtgen_map = test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanesEachWithoutSuccessors();
    auto expected_lane_id = gtgen_map->GetLanes()[0].id;
    auto expected_successor_id = gtgen_map->GetLanes()[2].id;

    LaneLocationProvider lane_location_provider{*gtgen_map};

    auto start_lane = lane_location_provider.FindLane(expected_lane_id);
    ASSERT_NE(nullptr, start_lane);
    EXPECT_EQ(start_lane->id, expected_lane_id);

    const Lane* successor_lane = lane_location_provider.GetCachedSuccessorLane(start_lane);
    ASSERT_NE(nullptr, successor_lane);
    EXPECT_EQ(successor_lane->id, expected_successor_id);
}

/// @brief This is a real world bug: Returned position was wrongly at y = ~989 (beginning of center line).
TEST_F(
    LaneLocationProviderTest,
    GivenMapAndLaneLocationProvider_WhenLookingUpAProblematicPosition_ThenReturnedProjectionAndIndexIsCloseToThisPoint)
{
    auto gtgen_map = test_utils::MapCatalogue::MapStraightRoad2km();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    mantle_api::Vec3<units::length::meter_t> expected_position{0.24393829324278565_m, 1640.2500192731936_m, 0.0_m};

    auto lane_location = lane_location_provider.GetLaneLocation(expected_position);
    EXPECT_TRIPLE(expected_position, lane_location.projected_centerline_point);
}

TEST_F(LaneLocationProviderTest, GivenLaneIdAndPosition_WhenGettingLaneLocationById_ThenCorrectLaneLocationReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    mantle_api::UniqueId expected_lane_id{3};
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(2, 1);
    LaneLocationProvider lane_location_provider{*gtgen_map};

    auto lane_location = lane_location_provider.GetLaneLocationById(expected_lane_id, {0_m, 0_m, 0_m});
    ASSERT_EQ(1, lane_location.lanes.size());
    EXPECT_EQ(expected_lane_id, lane_location.lanes.front()->id);
}

TEST_F(LaneLocationProviderTest, GivenInvalidLaneIdAndPosition_WhenGettingLaneLocationById_ThenExceptionThrown)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(2, 1);
    LaneLocationProvider lane_location_provider{*gtgen_map};

    EXPECT_THROW(lane_location_provider.GetLaneLocationById(666, {0_m, 0_m, 0_m}), EnvironmentException);
}

TEST_F(LaneLocationProviderTest,
       GivenOnRoadPoseAndPositiveDistanceWithForwardDirection_WhenFindLanePoseAtDistanceFrom_ThenExpectPoseWithValue)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(5, 3);
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{2.0};
    const mantle_api::Direction direction = mantle_api::Direction::kForward;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);

    EXPECT_TRUE(actual_pose.has_value());
}

TEST_F(LaneLocationProviderTest,
       GivenOnRoadPoseAndPositiveDistanceWithBackwardsDirection_WhenFindLanePoseAtDistanceFrom_ThenExpectPoseWithValue)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(5, 3);
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{9.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{2.0};
    const mantle_api::Direction direction = mantle_api::Direction::kBackwards;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);

    EXPECT_TRUE(actual_pose.has_value());
}

TEST_F(LaneLocationProviderTest, GivenPoseNotOnRoadWithForwardDirection_WhenFindLanePoseAtDistance_ThenExpectEmptyPose)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(5, 3);
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{-1.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{2.0};
    const mantle_api::Direction direction = mantle_api::Direction::kForward;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);

    EXPECT_FALSE(actual_pose.has_value());
}

TEST_F(LaneLocationProviderTest,
       GivenPoseNotOnRoadWithBackwardsDirection_WhenFindLanePoseAtDistance_ThenExpectEmptyPose)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(5, 3);
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{-1.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{2.0};
    const mantle_api::Direction direction = mantle_api::Direction::kBackwards;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);

    EXPECT_FALSE(actual_pose.has_value());
}

TEST_F(LaneLocationProviderTest,
       GivenNegativeDistanceWithForwardDirection_WhenFindLanePoseAtDistanceFrom_ThenExpectEmptyPose)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(5, 3);
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{-2.0};
    const mantle_api::Direction direction = mantle_api::Direction::kForward;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);

    EXPECT_FALSE(actual_pose.has_value());
}

TEST_F(LaneLocationProviderTest,
       GivenNegativeDistanceWithBackwardsDirection_WhenFindLanePoseAtDistanceFrom_ThenExpectEmptyPose)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(5, 3);
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{-2.0};
    const mantle_api::Direction direction = mantle_api::Direction::kBackwards;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);

    EXPECT_FALSE(actual_pose.has_value());
}

TEST_F(
    LaneLocationProviderTest,
    GivenPositionInStraightLaneWithForwardDirection__WhenFindLanePoseAtDistanceFrom_ThenExpectPositionShiftedLongitudinallyByDistance)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapWithOne6kmEastingLaneWithTwoPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{150.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{50.0};
    const mantle_api::Direction direction = mantle_api::Direction::kForward;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);
    const mantle_api::Vec3<units::length::meter_t> expected_position{200.0_m, 0.0_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0.0_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());

    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenPositionInStraightLaneWithBackwards_WhenFindLanePoseAtDistanceFrom_ThenExpectPositionShiftedLongitudinallyByDistance)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapWithTwoConnectedEastingLanesWithHundredPointsEach();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{195.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{100.0};
    const mantle_api::Direction direction = mantle_api::Direction::kBackwards;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);
    const mantle_api::Vec3<units::length::meter_t> expected_position{95.0_m, 0.0_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0.0_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());

    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenPositionNotOnCenterLineInStraightLaneWithForwardDirection_WhenFindLanePoseAtDistanceFrom_ThenExpectPositionShiftedLongitudinallyByDistanceAndKeepLateralOffset)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapWithOne6kmEastingLaneWithTwoPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 1.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{150.0};
    const mantle_api::Direction direction = mantle_api::Direction::kForward;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);
    const mantle_api::Vec3<units::length::meter_t> expected_position{150.0_m, 1.0_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0.0_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());

    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenPositionNotOnCenterLineInStraightLaneWithBackwardsDirection_WhenFindLanePoseAtDistanceFrom_ThenExpectPositionShiftedLongitudinallyByDistanceAndKeepLateralOffset)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapWithOne6kmEastingLaneWithTwoPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{150.0_m, 1.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{50.0};
    const mantle_api::Direction direction = mantle_api::Direction::kBackwards;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);
    const mantle_api::Vec3<units::length::meter_t> expected_position{100.0_m, 1.0_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0.0_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());

    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenPositionOnCenterLineInNorthingEastLaneWithForwardDirection_WhenFindLanePoseAtDistanceFrom_ThenExpectPositionShiftedLaterlallyAndLongitudinallyAndYawPointsNorthEast)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneNorthEastingLaneWithNPoints(5, 1);
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{3.0};
    const mantle_api::Direction direction = mantle_api::Direction::kForward;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);
    const mantle_api::Vec3<units::length::meter_t> expected_position{2.12_m, 2.12_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{
        units::angle::radian_t(M_PI_4), 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());

    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenPositionOnCenterLineInNorthingEastLaneWithBackwardsDirection_WhenFindLanePoseAtDistanceFrom_ThenExpectPositionShiftedLaterlallyAndLongitudinallyAndYawPointsNorthEast)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneNorthEastingLaneWithNPoints(5, 1);

    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{4.0_m, 4.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{3.0};
    const mantle_api::Direction direction = mantle_api::Direction::kBackwards;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);
    const mantle_api::Vec3<units::length::meter_t> expected_position{1.87_m, 1.87_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{
        units::angle::radian_t(M_PI_4), 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());

    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenPositionBeforeLaneSplitLeftWithForwardDirection_WhenFindLanePoseAtDistanceFrom_ThenExpectPositionShiftedLongitudinallyOnTheSameLane)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanes();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{85.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{20.0};
    const mantle_api::Direction direction = mantle_api::Direction::kForward;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);
    const mantle_api::Vec3<units::length::meter_t> expected_position{105.0_m, 0.0_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0.0_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());

    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenPositionBeforeLaneSplitLeftWithBackwardsDirection_WhenFindLanePoseAtDistanceFrom_ThenExpectPositionShiftedLongitudinallyOnTheSameLane)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanes();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{85.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{20.0};
    const mantle_api::Direction direction = mantle_api::Direction::kBackwards;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);
    const mantle_api::Vec3<units::length::meter_t> expected_position{65.0_m, 0.0_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0.0_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());

    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenPositionBeforeLaneSplitRightWithForwardDirection_WhenFindLanePoseAtDistanceFrom_ThenExpectPositionShiftedLongitudinallyOnTheSameLane)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapSplitOneLaneToTheRightIntoTwoLanesWithShortJunction();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{85.0_m, 4.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{30.0};
    const mantle_api::Direction direction = mantle_api::Direction::kForward;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);
    const mantle_api::Vec3<units::length::meter_t> expected_position{115.0_m, 4.0_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0.0_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());

    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenPositionBeforeLaneSplitRightWithBackwards_WhenFindLanePoseAtDistanceFrom_ThenExpectPositionShiftedLongitudinallyOnTheSameLane)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapSplitOneLaneToTheRightIntoTwoLanesWithShortJunction();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{85.0_m, 4.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{30.0};
    const mantle_api::Direction direction = mantle_api::Direction::kBackwards;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);
    const mantle_api::Vec3<units::length::meter_t> expected_position{55.0_m, 4.0_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0.0_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());

    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenPositionBeforeLaneMergeRightWithForwardDirection_WhenFindLanePoseAtDistanceFrom_ThenExpectPositionShiftedLongitudinallyByDistance)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapMergeTwoLanesToTheRightIntoOneLane();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{99.0_m, 4.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{100.0};
    const mantle_api::Direction direction = mantle_api::Direction::kForward;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);
    const mantle_api::Vec3<units::length::meter_t> expected_position{198.84_m, 0.03_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0.0_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.1);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenPositionBeforeLaneMergeRightWithBackwardsDirection_WhenFindLanePoseAtDistanceFrom_ThenExpectPositionShiftedLongitudinallyByDistance)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapMergeTwoLanesToTheRightIntoOneLane();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{99.0_m, 4.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{20.0};
    const mantle_api::Direction direction = mantle_api::Direction::kBackwards;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);
    const mantle_api::Vec3<units::length::meter_t> expected_position{79.00_m, 4.0_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0.0_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.1);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(LaneLocationProviderTest,
       GivenPositionAtEndOfLaneMergingToRightWithStraightRightBoundary_WhenGetLaneLocationById_ThenLaneNormalValid)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();

    test_utils::RawMapBuilder builder;

    builder.AddNewLaneGroup();
    builder.AddFromCatalogue(test_utils::LaneCatalogue::EastingMergingTriangularLeftLaneWithHundredPoints());
    builder.AddFromCatalogue(test_utils::LaneCatalogue::StraightEastingLaneWithHundredPoints({-4_m, 0_m, 0_m}));

    auto gtgen_map = builder.Build();

    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Vec3<units::length::meter_t> ref_position{99.0_m, -4.0_m, 0.0_m};

    const auto lane_location = lane_location_provider.GetLaneLocationById(3, ref_position);
    const mantle_api::Vec3<units::length::meter_t> expected_lane_normal{0.0_m, 0.0_m, 1.0_m};

    EXPECT_TRIPLE(expected_lane_normal, lane_location.lane_normal);
}

TEST_F(
    LaneLocationProviderTest,
    GivenPositionOnCurvedLaneWithForwardDirection_WhenFindLanePoseAtDistanceFrom_ThenExpectPositionShiftedByDistanceAndYawPointsNorthEast)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapOneLaneNorthingToWestingCurve();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{1.0};
    const mantle_api::Direction direction = mantle_api::Direction::kForward;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);
    const mantle_api::Vec3<units::length::meter_t> expected_position{0.0_m, 1.0_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{
        units::angle::radian_t(M_PI_2), 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenPositionOnCurvedLaneWithBackwardsDirection_WhenFindLanePoseAtDistanceFrom_ThenExpectPositionShiftedByDistanceAndYawPointsNorthEast)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapOneLaneNorthingToWestingCurve();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{-5.0_m, 5.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t distance{2.82};
    const mantle_api::Direction direction = mantle_api::Direction::kBackwards;

    const auto actual_pose = lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, distance, direction);
    const mantle_api::Vec3<units::length::meter_t> expected_position{-2.58_m, 5.58_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{2.35_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenPositionNoLateralOffsetButWithHeightInForwardDirection_WhenFindLanePoseAtDistanceFrom_ThenExpectPositionIsNotShiftedLaterally)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto later_shift = 0.0_m;
    const auto vertical_shift = 5.0_m;
    const mantle_api::Pose ref_pose{{0.0_m, later_shift, vertical_shift}, {0.0_rad, 0.0_rad, 0.0_rad}};

    const auto actual_pose =
        lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, 1.0_m, mantle_api::Direction::kForward);
    const mantle_api::Vec3<units::length::meter_t> expected_position{1.0_m, later_shift, 0.0_m};

    ASSERT_TRUE(actual_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
}
TEST_F(
    LaneLocationProviderTest,
    GivenPositionNoLateralOffsetButWithHeightInBackwardsDirection_WhenFindLanePoseAtDistanceFrom_ThenExpectPositionIsNotShiftedLaterally)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto later_shift = 0.0_m;
    const auto vertical_shift = 5.0_m;
    const mantle_api::Pose ref_pose{{250.0_m, later_shift, vertical_shift}, {0.0_rad, 0.0_rad, 0.0_rad}};

    const auto actual_pose =
        lane_location_provider.FindLanePoseAtDistanceFrom(ref_pose, 100.0_m, mantle_api::Direction::kBackwards);
    const mantle_api::Vec3<units::length::meter_t> expected_position{150.0_m, later_shift, 0.0_m};

    ASSERT_TRUE(actual_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenAPositionOnLaneWithPositiveRelativeLaneTargetAndPositiveDistance_WhenFindRelativeLanePoseAtDistanceFrom_ThenEntityPoseIsOnRelativeLaneAtDistance)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};

    int relative_lane_target = 2;

    units::length::meter_t distance{1.0};
    units::length::meter_t lateral_offset{0.0};

    const auto actual_pose = lane_location_provider.FindRelativeLanePoseAtDistanceFrom(
        ref_pose, relative_lane_target, distance, lateral_offset);

    const mantle_api::Vec3<units::length::meter_t> expected_position{1.0_m, 8.0_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0.0_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenAPositionOnLaneWithPositiveRelativeLaneTargetAndPositiveDistanceAndLateralOffset_WhenFindRelativeLanePoseAtDistanceFrom_ThenEntityPoseIsOnRelativeLaneAtDistanceWithLateralOffset)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};

    int relative_lane_target = 2;

    units::length::meter_t distance{1.0};
    units::length::meter_t lateral_offset{0.5};

    const auto actual_pose = lane_location_provider.FindRelativeLanePoseAtDistanceFrom(
        ref_pose, relative_lane_target, distance, lateral_offset);

    const mantle_api::Vec3<units::length::meter_t> expected_position{1.0_m, 8.5_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0.0_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenAPositionOnLaneAWithPositiveRelativeLaneTargetAndPositiveDistanceAndNegativeLateralOffset_WhenFindRelativeLanePoseAtDistanceFrom_ThenEntityPoseIsInCorrectRelativeLaneWithNegativeLateralOffset)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};

    int relative_lane_target = 2;

    units::length::meter_t distance{1.0};
    units::length::meter_t lateral_offset{-0.5};

    const auto actual_pose = lane_location_provider.FindRelativeLanePoseAtDistanceFrom(
        ref_pose, relative_lane_target, distance, lateral_offset);

    const mantle_api::Vec3<units::length::meter_t> expected_position{1.0_m, 7.5_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0.0_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenAPositionOnLaneWithNegativeRelativeLaneTargetAndPositiveDistance_WhenFindRelativeLanePoseAtDistanceFrom_ThenEntityPoseIsOnRelativeLaneAtDistance)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 8.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};

    int relative_lane_target = -2;

    units::length::meter_t distance{1.0};
    units::length::meter_t lateral_offset{0.0};

    const auto actual_pose = lane_location_provider.FindRelativeLanePoseAtDistanceFrom(
        ref_pose, relative_lane_target, distance, lateral_offset);

    const mantle_api::Vec3<units::length::meter_t> expected_position{1.0_m, 0.0_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0.0_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenAPositionOnLaneWithZeroLaneTargetAndPositiveDistance_WhenFindRelativeLanePoseAtDistanceFrom_ThenEntityPoseIsOnTheSameLaneAtDistance)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 8.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};

    int relative_lane_target = 0;

    units::length::meter_t distance{1.0};
    units::length::meter_t lateral_offset{0.0};

    const auto actual_pose = lane_location_provider.FindRelativeLanePoseAtDistanceFrom(
        ref_pose, relative_lane_target, distance, lateral_offset);

    const mantle_api::Vec3<units::length::meter_t> expected_position{1.0_m, 8.0_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0.0_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenAPositionOnLaneWithNotRightAdjacentLane_WhenFindRelativeLanePoseAtDistanceFrom_ThenEntityPoseIsNotCalculated)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 16.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};

    int relative_lane_target = -2;

    units::length::meter_t distance{1.0};
    units::length::meter_t lateral_offset{0.0};

    const auto actual_pose = lane_location_provider.FindRelativeLanePoseAtDistanceFrom(
        ref_pose, relative_lane_target, distance, lateral_offset);

    EXPECT_FALSE(actual_pose.has_value());
}

TEST_F(
    LaneLocationProviderTest,
    GivenAPositionOnLaneAndTranslatedPositionNotOnLane_WhenFindRelativeLanePoseAtDistanceFrom_ThenEntityPoseIsNotCalculated)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{198.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};

    int relative_lane_target = -2;

    units::length::meter_t distance{101.0};
    units::length::meter_t lateral_offset{0.0};

    const auto actual_pose = lane_location_provider.FindRelativeLanePoseAtDistanceFrom(
        ref_pose, relative_lane_target, distance, lateral_offset);

    EXPECT_FALSE(actual_pose.has_value());
}

TEST_F(
    LaneLocationProviderTest,
    GivenAPositionOnLaneAndTranslatedPositionOnDifferentSegment_WhenFindRelativeLanePoseAtDistanceFrom_ThenEntityPoseIsInCorrectRelativeLane)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};

    int relative_lane_target = 2;

    units::length::meter_t distance{101.0};
    units::length::meter_t lateral_offset{0.0};

    const auto actual_pose = lane_location_provider.FindRelativeLanePoseAtDistanceFrom(
        ref_pose, relative_lane_target, distance, lateral_offset);

    const mantle_api::Vec3<units::length::meter_t> expected_position{101.0_m, 8.0_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0.0_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenAPositionOnLaneWithRightAdjacentLaneDoesNotExit_WhenFindRelativeLanePoseAtDistanceFrom_ThenEntityPoseIsNotCalculated)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};

    int relative_lane_target = -1;

    units::length::meter_t distance{1.0};
    units::length::meter_t lateral_offset{0.0};

    const auto actual_pose = lane_location_provider.FindRelativeLanePoseAtDistanceFrom(
        ref_pose, relative_lane_target, distance, lateral_offset);

    EXPECT_FALSE(actual_pose.has_value());
}

TEST_F(LaneLocationProviderTest,
       GivenAPositionOnLaneWithNegativeDistance_WhenFindRelativeLanePoseAtDistanceFrom_ThenEntityPoseIsCalculated)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{150.0_m, 4.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};

    int relative_lane_target = -1;

    units::length::meter_t distance{-100.0};
    units::length::meter_t lateral_offset{1.0};

    const mantle_api::Vec3<units::length::meter_t> expected_position{50.0_m, 1.0_m, 0.0_m};
    const mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0.0_rad, 0.0_rad, 0.0_rad};

    const auto actual_pose = lane_location_provider.FindRelativeLanePoseAtDistanceFrom(
        ref_pose, relative_lane_target, distance, lateral_offset);

    ASSERT_TRUE(actual_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.01);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenRefOnCircleLane_FindRelativeLanePoseAtDistanceFromAPositive90DegOnTheSameLane_ThenEntityPoseIsInCorrectRelativeLane)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::CircleClosedLoopMapWithFourLaneGroups();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_pose = mantle_api::Pose{{-70.71_m, -70.71_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};  // on lane26
    const auto distance = units::length::meter_t{2 * M_PI * 100 * 135 / 360};  // 135deg arc length in counter clockwise
    const auto lateral_offset = units::length::meter_t{0.0};
    const std::int16_t relative_lane_target = 0;  // target on the same lane

    const auto actual_pose = lane_location_provider.FindRelativeLanePoseAtDistanceFrom(
        ref_pose, relative_lane_target, distance, lateral_offset);

    const auto expected_position = mantle_api::Vec3<units::length::meter_t>{100.0_m, 0.0_m, 0.0_m};
    const auto expected_orientation = mantle_api::Orientation3<units::angle::radian_t>{1.5868_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.1);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenRefOnCircleLane_FindRelativeLanePoseAtDistanceFromAPositive90DegRightLane_ThenEntityPoseIsInCorrectRelativeLane)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::CircleClosedLoopMapWithFourLaneGroups();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_pose = mantle_api::Pose{{-70.71_m, -70.71_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};  // on lane26
    const auto distance = units::length::meter_t{2 * M_PI * 100 * 90 / 360};  // 90 deg arc length in counter clockwise
    const auto lateral_offset = units::length::meter_t{0.0};
    const std::int16_t relative_lane_target = -1;  // target on the right adjacent lane

    const auto actual_pose = lane_location_provider.FindRelativeLanePoseAtDistanceFrom(
        ref_pose, relative_lane_target, distance, lateral_offset);

    const auto expected_position = mantle_api::Vec3<units::length::meter_t>{72.12_m, -72.12_m, 0.0_m};
    const auto expected_orientation = mantle_api::Orientation3<units::angle::radian_t>{0.7854_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.1);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.01);
}

TEST_F(
    LaneLocationProviderTest,
    GivenRefOnCircleLane_FindRelativeLanePoseAtDistanceFromAPositive45DegLeftLane_ThenEntityPoseIsInCorrectRelativeLane)
{

    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::CircleClosedLoopMapWithFourLaneGroups();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_pose = mantle_api::Pose{{-70.71_m, -70.71_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};  // on lane26

    //@note move 45 deg to counter clockwise is the corner case that the target position is at the junction of two lanes
    // with a very small gap. The current algorithm does not cover this case. So use 44.9Deg instead of 45Deg that
    // ensures finding the target position
    const auto distance = units::length::meter_t{2 * M_PI * 100 * 44.9 / 360};
    const auto lateral_offset = units::length::meter_t{0.0};
    const std::int16_t relative_lane_target = 1;  // target on the left adjacent lane

    const auto actual_pose = lane_location_provider.FindRelativeLanePoseAtDistanceFrom(
        ref_pose, relative_lane_target, distance, lateral_offset);

    const auto expected_position = mantle_api::Vec3<units::length::meter_t>{-0.0_m, -98.0_m, 0.0_m};
    const auto expected_orientation = mantle_api::Orientation3<units::angle::radian_t>{0.0_rad, 0.0_rad, 0.0_rad};

    ASSERT_TRUE(actual_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, actual_pose.value().position, 0.2);
    EXPECT_TRIPLE_NEAR(expected_orientation, actual_pose.value().orientation, 0.02);
}

TEST_F(LaneLocationProviderTest, GivenPositionOffOfRoad_WhenGetRelativeLaneId_ThenNoLaneIdsReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{-100.0_m, 4.0_m, 0.0_m};  // not on lane
    const int lateral_shift = -1;

    const auto target_lane_id = lane_location_provider.GetRelativeLaneId({ref_position, {}}, lateral_shift);

    EXPECT_FALSE(target_lane_id.has_value());
}

TEST_F(LaneLocationProviderTest, GivenRelativeLaneNotExist_WhenGetRelativeLaneId_ThenNoLaneIdsReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{10.0_m, 4.0_m, 0.0_m};  // on local lane -2
    const int lateral_shift = -2;                                                              // not exist

    const auto target_lane_id = lane_location_provider.GetRelativeLaneId({ref_position, {}}, lateral_shift);

    EXPECT_FALSE(target_lane_id.has_value());
}

TEST_F(LaneLocationProviderTest, GivenStraightLane_WhenGetRelativeLaneIdWithNoLateralShift_ThenCorrectLaneIdReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{0.0_m, 4.0_m, 0.0_m};  // on local lane -2
    const int lateral_shift = 0;

    const auto target_lane_id = lane_location_provider.GetRelativeLaneId({ref_position, {}}, lateral_shift);

    ASSERT_TRUE(target_lane_id.has_value());
    EXPECT_EQ(-2, target_lane_id.value());
}

TEST_F(LaneLocationProviderTest,
       GivenRefPositionOnStraightLane_WhenGetRelativeLaneIdWithLateralShiftOneRight_ThenCorrectLaneIdReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{0.0_m, 4.0_m, 0.0_m};  // on local lane -2
    const int lateral_shift = -1;                                                             // shift 1 lane right

    const auto target_lane_id = lane_location_provider.GetRelativeLaneId({ref_position, {}}, lateral_shift);

    ASSERT_TRUE(target_lane_id.has_value());
    EXPECT_EQ(-3, target_lane_id.value());
}

TEST_F(LaneLocationProviderTest,
       GivenRefPositionOnStraightLane_WhenGetRelativeLaneIdWithLateralShiftOneLeft_ThenCorrectLaneIdReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{0.0_m, 4.0_m, 0.0_m};  // on local lane -2
    const int lateral_shift = 1;                                                              // shift 1 lane left

    const auto target_lane_id = lane_location_provider.GetRelativeLaneId({ref_position, {}}, lateral_shift);

    ASSERT_TRUE(target_lane_id.has_value());
    EXPECT_EQ(-1, target_lane_id.value());
}

TEST_F(LaneLocationProviderTest,
       GivenRefPositionOnStraightLane_WhenGetRelativeLaneIdWithLateralShiftTwoRight__ThenCorrectLaneIdReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{0.0_m, 8.0_m, 0.0_m};  // on local lane -1
    const int lateral_shift = -2;                                                             // shift 2 lanes right

    const auto target_lane_id = lane_location_provider.GetRelativeLaneId({ref_position, {}}, lateral_shift);

    ASSERT_TRUE(target_lane_id.has_value());
    EXPECT_EQ(-3, target_lane_id.value());
}

TEST_F(LaneLocationProviderTest,
       GivenRefPositionOnStraightLane_WhenGetRelativeLaneIdWithLateralShiftTwoLeft_ThenCorrectLaneIdReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{0.0_m, 0.0_m, 0.0_m};  // on local lane -3
    const int lateral_shift = 2;                                                              // shift 2 lanes left

    const auto target_lane_id = lane_location_provider.GetRelativeLaneId({ref_position, {}}, lateral_shift);

    ASSERT_TRUE(target_lane_id.has_value());
    EXPECT_EQ(-1, target_lane_id.value());
}

TEST_F(LaneLocationProviderTest,
       GivenRefPositionOnCircleLane_WhenGetRelativeLaneIdWithLateralShiftOneLeft_ThenCorrectLaneIdReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::CircleClosedLoopMapWithFourLaneGroups();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{-70.71_m, -70.71_m, 0.0_m};  // on local lane -2
    const int lateral_shift = -1;  // shift 1 lane right

    const auto target_lane_id = lane_location_provider.GetRelativeLaneId({ref_position, {}}, lateral_shift);

    ASSERT_TRUE(target_lane_id.has_value());
    EXPECT_EQ(-3, target_lane_id.value());
}

TEST_F(LaneLocationProviderTest,
       GivenRefPositionOnCircleLane_WhenGetRelativeLaneIdWithLateralShiftOneRight_ThenCorrectLaneIdReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::CircleClosedLoopMapWithFourLaneGroups();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{-70.71_m, -70.71_m, 0.0_m};  // on local lane -2
    const int lateral_shift = 1;                                                                    // shift 1 lane left

    const auto target_lane_id = lane_location_provider.GetRelativeLaneId({ref_position, {}}, lateral_shift);

    ASSERT_TRUE(target_lane_id.has_value());
    EXPECT_EQ(-1, target_lane_id.value());
}

TEST_F(LaneLocationProviderTest, GivenPositionOffOfRoad_WhenGetRelativeLaneTarget_ThenNoRelativeLaneTargetReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{-100.0_m, 4.0_m, 0.0_m};  // not on lane
    const mantle_api::LaneId target_lane_id = -3;

    const auto relative_lane_target = lane_location_provider.GetRelativeLaneTarget(ref_position, target_lane_id);

    EXPECT_FALSE(relative_lane_target.has_value());
}

TEST_F(LaneLocationProviderTest,
       GivenStraightRightLane_WhenGetRelativeLaneTargetWithTargetLaneIdOnSameLane_ThenZeroLateralShift)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{0.0_m, 4.0_m, 0.0_m};  // on local lane -2
    const mantle_api::LaneId target_lane_id = -2;

    const auto relative_lane_target = lane_location_provider.GetRelativeLaneTarget(ref_position, target_lane_id);

    ASSERT_TRUE(relative_lane_target.has_value());
    EXPECT_EQ(0, relative_lane_target.value());
}

TEST_F(
    LaneLocationProviderTest,
    GivenRefPositionOnStraightRightLane_WhenGetRelativeLaneTargetWithTargetLaneIdOneToTheRight_ThenCorrectRelativeLaneTargetReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{0.0_m, 4.0_m, 0.0_m};  // on local lane -2
    const mantle_api::LaneId target_lane_id = -3;                                             // shift 1 lane right

    const auto relative_lane_target = lane_location_provider.GetRelativeLaneTarget(ref_position, target_lane_id);

    ASSERT_TRUE(relative_lane_target.has_value());
    EXPECT_EQ(-1, relative_lane_target.value());
}

TEST_F(
    LaneLocationProviderTest,
    GivenRefPositionOnStraightRightLane_WhenGetRelativeLaneTargetWithTargetLaneIdOneToTheLeft_ThenCorrectRelativeLaneTargetReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{0.0_m, 4.0_m, 0.0_m};  // on local lane -2
    const mantle_api::LaneId target_lane_id = -1;                                             // shift 1 lane left

    const auto relative_lane_target = lane_location_provider.GetRelativeLaneTarget(ref_position, target_lane_id);

    ASSERT_TRUE(relative_lane_target.has_value());
    EXPECT_EQ(1, relative_lane_target.value());
}

TEST_F(
    LaneLocationProviderTest,
    GivenRefPositionOnStraightRightLane_WhenGetRelativeLaneTargetWithTargetLaneIdTwoToTheRight_ThenCorrectRelativeLaneTargetReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{0.0_m, 8.0_m, 0.0_m};  // on local lane -1
    const mantle_api::LaneId target_lane_id = -3;                                             // shift 2 lanes right

    const auto relative_lane_target = lane_location_provider.GetRelativeLaneTarget(ref_position, target_lane_id);

    ASSERT_TRUE(relative_lane_target.has_value());
    EXPECT_EQ(-2, relative_lane_target.value());
}

TEST_F(
    LaneLocationProviderTest,
    GivenRefPositionOnStraightRightLane_WhenGetRelativeLaneTargetWithTargetLaneIdTwoToTheLeft_ThenCorrectRelativeLaneTargetReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{0.0_m, 0.0_m, 0.0_m};  // on local lane -3
    const mantle_api::LaneId target_lane_id = -1;                                             // shift 2 lanes left

    const auto relative_lane_target = lane_location_provider.GetRelativeLaneTarget(ref_position, target_lane_id);

    ASSERT_TRUE(relative_lane_target.has_value());
    EXPECT_EQ(2, relative_lane_target.value());
}

TEST_F(
    LaneLocationProviderTest,
    GivenRefPositionOnCircleRightLane_WhenGetRelativeLaneTargetWithTargetLaneIdOneToTheLeft_ThenCorrectRelativeLaneTargetReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::CircleClosedLoopMapWithFourLaneGroups();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{-70.71_m, -70.71_m, 0.0_m};  // on local lane -2
    const mantle_api::LaneId target_lane_id = -3;  // shift 1 lane right

    const auto relative_lane_target = lane_location_provider.GetRelativeLaneTarget(ref_position, target_lane_id);

    ASSERT_TRUE(relative_lane_target.has_value());
    EXPECT_EQ(-1, relative_lane_target.value());
}

TEST_F(
    LaneLocationProviderTest,
    GivenRefPositionOnCircleRightLane_WhenGetRelativeLaneTargetWithTargetLaneIdOneToTheRight_ThenCorrectRelativeLaneTargetReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::CircleClosedLoopMapWithFourLaneGroups();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{-70.71_m, -70.71_m, 0.0_m};  // on local lane -2
    const mantle_api::LaneId target_lane_id = -1;                                                   // shift 1 lane left

    const auto relative_lane_target = lane_location_provider.GetRelativeLaneTarget(ref_position, target_lane_id);

    ASSERT_TRUE(relative_lane_target.has_value());
    EXPECT_EQ(1, relative_lane_target.value());
}

TEST_F(
    LaneLocationProviderTest,
    GivenRefPositionOnStraightLeftLane_WhenGetRelativeLaneTargetWithTargetLaneIdOneToTheRight_ThenCorrectRelativeLaneTargetReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapWithThreeParallelLeftLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{0.0_m, 4.0_m, 0.0_m};  // on local lane 2
    const mantle_api::LaneId target_lane_id = 3;                                              // shift 1 lane right

    const auto relative_lane_target = lane_location_provider.GetRelativeLaneTarget(ref_position, target_lane_id);

    ASSERT_TRUE(relative_lane_target.has_value());
    EXPECT_EQ(-1, relative_lane_target.value());
}

TEST_F(
    LaneLocationProviderTest,
    GivenRefPositionOnStraightLeftLane_WhenGetRelativeLaneTargetWithTargetLaneIdOneToTheLeft_ThenCorrectRelativeLaneTargetReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapWithThreeParallelLeftLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{0.0_m, 4.0_m, 0.0_m};  // on local lane 2
    const mantle_api::LaneId target_lane_id = 1;                                              // shift 1 lane left

    const auto relative_lane_target = lane_location_provider.GetRelativeLaneTarget(ref_position, target_lane_id);

    ASSERT_TRUE(relative_lane_target.has_value());
    EXPECT_EQ(1, relative_lane_target.value());
}

TEST_F(LaneLocationProviderTest, GivenPositionOffOfRoad_WhenGetProjectedPoseAtLane_ThenNoLaneIdsReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{-100.0_m, 4.0_m, 0.0_m};  // not on lane
    const auto target_lane_id = mantle_api::UniqueId{1};

    const auto projected_pose = lane_location_provider.GetProjectedPoseAtLane(ref_position, target_lane_id);

    EXPECT_FALSE(projected_pose.has_value());
}

TEST_F(LaneLocationProviderTest, GivenRelativeLaneNotExist_WhenGetProjectedPoseAtLane_ThenNoLaneIdsReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{10.0_m, 4.0_m, 0.0_m};  // on lane5
    const auto target_lane_id = mantle_api::UniqueId{30};                                      // no valid lane

    const auto projected_pose = lane_location_provider.GetProjectedPoseAtLane(ref_position, target_lane_id);

    EXPECT_FALSE(projected_pose.has_value());
}

TEST_F(LaneLocationProviderTest, GivenStraightLane_WhenGetProjectedPoseAtLaneNoLateralShift_ThenCorrectLaneIdReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{0.0_m, 4.0_m, 0.0_m};  // on lane5
    const auto target_lane_id = mantle_api::UniqueId{5};                                      // no valid lane
    const auto& expected_position = ref_position;                                             // on lane5

    const auto projected_pose = lane_location_provider.GetProjectedPoseAtLane(ref_position, target_lane_id);

    ASSERT_TRUE(projected_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, projected_pose.value().position, 0.001);
}

TEST_F(LaneLocationProviderTest,
       GivenRefPositionOnStraightLane_WhenGetProjectedPoseAtLaneWithLateralShiftOneLeft_ThenCorrectLaneIdReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{10.0_m, 4.0_m, 0.0_m};  // on lane5
    const auto target_lane_id = mantle_api::UniqueId{8};
    const auto expected_position = mantle_api::Vec3<units::length::meter_t>{10.0_m, 8.0_m, 0.0_m};  // on lane8

    const auto projected_pose = lane_location_provider.GetProjectedPoseAtLane(ref_position, target_lane_id);

    ASSERT_TRUE(projected_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, projected_pose.value().position, 0.001);
}

TEST_F(LaneLocationProviderTest,
       GivenRefPositionOnStraightLane_WhenGetProjectedPoseAtLaneWithLateralShiftTwoRight__ThenCorrectLaneIdReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{10.0_m, 8.0_m, 0.0_m};  // on lane8
    const int target_lane_id = 2;
    const auto expected_position = mantle_api::Vec3<units::length::meter_t>{10.0_m, 0.0_m, 0.0_m};

    const auto projected_pose = lane_location_provider.GetProjectedPoseAtLane(ref_position, target_lane_id);

    ASSERT_TRUE(projected_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, projected_pose.value().position, 0.001);
}

TEST_F(LaneLocationProviderTest,
       GivenRefPositionOnCircleLane_WhenGetProjectedPoseAtLaneWithLateralShiftOneLeft_ThenCorrectLaneIdReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::CircleClosedLoopMapWithFourLaneGroups();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{-70.71_m, -70.71_m, 0.0_m};  // on lane26
    const int target_lane_id = 23;
    const auto expected_position = mantle_api::Vec3<units::length::meter_t>{-69.287_m, -69.287_m, 0.0_m};

    const auto projected_pose = lane_location_provider.GetProjectedPoseAtLane(ref_position, target_lane_id);

    ASSERT_TRUE(projected_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, projected_pose.value().position, 0.001);
}

TEST_F(LaneLocationProviderTest,
       GivenRefPositionOnCircleLane_WhenGetProjectedPoseAtLaneWithLateralShiftOneRight_ThenCorrectLaneIdReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::CircleClosedLoopMapWithFourLaneGroups();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto ref_position = mantle_api::Vec3<units::length::meter_t>{-70.71_m, -70.71_m, 0.0_m};  // on lane26
    const int target_lane_id = 29;
    const auto expected_position = mantle_api::Vec3<units::length::meter_t>{-72.115_m, -72.115_m, 0.0_m};

    const auto projected_pose = lane_location_provider.GetProjectedPoseAtLane(ref_position, target_lane_id);

    ASSERT_TRUE(projected_pose.has_value());
    EXPECT_TRIPLE_NEAR(expected_position, projected_pose.value().position, 0.001);
}

TEST_F(LaneLocationProviderTest,
       GivenStartPositionNotOnALane_GetLongitudinalLaneDistanceBetweenPositions_ThenNoValueReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto start_postion = mantle_api::Vec3<units::length::meter_t>{-100.0_m, 4.0_m, 0.0_m};  // not on lane
    const auto target_position = mantle_api::Vec3<units::length::meter_t>{0.0_m, 4.0_m, 0.0_m};

    const auto distance =
        lane_location_provider.GetLongitudinalLaneDistanceBetweenPositions(start_postion, target_position);

    EXPECT_FALSE(distance.has_value());
}

TEST_F(LaneLocationProviderTest,
       GivenTargetPositionNotOnALane_GetLongitudinalLaneDistanceBetweenPositions_ThenNoValueReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto start_postion = mantle_api::Vec3<units::length::meter_t>{0.0_m, 4.0_m, 0.0_m};
    const auto target_position = mantle_api::Vec3<units::length::meter_t>{-100.0_m, 4.0_m, 0.0_m};  // not on lane

    const auto distance =
        lane_location_provider.GetLongitudinalLaneDistanceBetweenPositions(start_postion, target_position);

    EXPECT_FALSE(distance.has_value());
}

TEST_F(LaneLocationProviderTest,
       GivenTargetNotOnTheLanePathFromStartingPosition_GetLongitudinalLaneDistanceBetweenPositions_ThenNoValueReturned)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto start_postion = mantle_api::Vec3<units::length::meter_t>{100.0_m, 0.0_m, 0.0_m};
    const auto target_position =
        mantle_api::Vec3<units::length::meter_t>{0.0_m, 8.0_m, 0.0_m};  // backward to the start position

    const auto distance =
        lane_location_provider.GetLongitudinalLaneDistanceBetweenPositions(start_postion, target_position);

    EXPECT_FALSE(distance.has_value());
}

TEST_F(
    LaneLocationProviderTest,
    GivenStartTargetOnStraightLaneWithinTheSameSegment_GetLongitudinalLaneDistanceBetweenPositions_ThenReturnCorrectValue)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto start_postion = mantle_api::Vec3<units::length::meter_t>{29.1_m, 4.0_m, 0.0_m};
    const auto target_position = mantle_api::Vec3<units::length::meter_t>{42.5_m, 4.0_m, 0.0_m};
    const auto expected_distance = target_position.x - start_postion.x;

    const auto distance =
        lane_location_provider.GetLongitudinalLaneDistanceBetweenPositions(start_postion, target_position);

    ASSERT_TRUE(distance.has_value());
    EXPECT_EQ(expected_distance, distance.value());
}

TEST_F(
    LaneLocationProviderTest,
    GivenStartTargetOnStraightLaneWithinTheDifferentSegment_GetLongitudinalLaneDistanceBetweenPositions_ThenReturnCorrectValue)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto start_postion = mantle_api::Vec3<units::length::meter_t>{29.1_m, 4.0_m, 0.0_m};
    const auto target_position = mantle_api::Vec3<units::length::meter_t>{242.5_m, 4.0_m, 0.0_m};
    const auto expected_distance = target_position.x - start_postion.x;

    const auto distance =
        lane_location_provider.GetLongitudinalLaneDistanceBetweenPositions(start_postion, target_position);

    ASSERT_TRUE(distance.has_value());
    EXPECT_EQ(expected_distance, distance.value());
}

TEST_F(
    LaneLocationProviderTest,
    GivenStartTargetOnCircleLaneWithinTheDifferentSegment_GetLongitudinalLaneDistanceBetweenPositions_ThenReturnCorrectValue)
{
    const auto radius = units::length::meter_t{100};
    auto get_position_on_circle_lane = [&radius](const units::angle::radian_t& angle) {
        return mantle_api::Vec3<units::length::meter_t>{
            radius * units::math::cos(angle), radius * units::math::sin(angle), 0.0_m};
    };

    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::CircleClosedLoopMapWithFourLaneGroups();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const auto start_postion = get_position_on_circle_lane(units::angle::radian_t{M_PI / 6.0});
    const auto target_position = get_position_on_circle_lane(units::angle::radian_t{M_PI / 3.0});

    const auto expected_distance = 2.0 * M_PI * radius.value() / 12.0;  // 1/12 circumference

    const auto distance =
        lane_location_provider.GetLongitudinalLaneDistanceBetweenPositions(start_postion, target_position);

    ASSERT_TRUE(distance.has_value());
    EXPECT_NEAR(expected_distance, distance.value().value(), 0.01);
}

TEST_F(LaneLocationProviderTest, GivenPositionOnNoLane_WhenFindBestLaneCandidate_ThenReturnNullPtr)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(2, 1);

    LaneLocationProvider lane_location_provider{*gtgen_map};

    const Lane* lane = lane_location_provider.FindBestLaneCandidate({0_m, 10_m, 0_m});

    EXPECT_THAT(lane, ::testing::IsNull());
}

TEST_F(LaneLocationProviderTest,
       GivenPositionOnOneLaneWithNoSuccessor_WhenFindBestLaneCandidate_ThenReturnLaneWithoutSuccessor)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(2, 1);

    auto expected_lane = gtgen_map->GetLanes().front();

    LaneLocationProvider lane_location_provider{*gtgen_map};

    auto current_lane = lane_location_provider.FindBestLaneCandidate({0_m, 0_m, 0_m});

    EXPECT_EQ(expected_lane.id, current_lane->id);
}

TEST_F(LaneLocationProviderTest, GivenPositionOnTwoLanesWithNoSuccessors_WhenFindBestLaneCandidate_ThenReturnFirstLane)
{
    auto gtgen_map = test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanesEachWithoutSuccessors();

    auto expected_lane = gtgen_map->GetLane(6);

    LaneLocationProvider lane_location_provider{*gtgen_map};

    auto current_lane = lane_location_provider.FindBestLaneCandidate({100_m, 0_m, 0_m});

    EXPECT_EQ(expected_lane.id, current_lane->id);
}

TEST_F(LaneLocationProviderTest,
       GivenPositionOnTwoLanesWithFirstLaneWithSuccessor_WhenFindBestLaneCandidate_ThenReturnFirstLane)
{
    auto gtgen_map = test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanes();

    gtgen_map->GetLane(9).successors.clear();
    auto expected_lane = gtgen_map->GetLane(6);

    LaneLocationProvider lane_location_provider{*gtgen_map};

    auto current_lane = lane_location_provider.FindBestLaneCandidate({100_m, 0_m, 0_m});

    EXPECT_EQ(expected_lane.id, current_lane->id);
}

TEST_F(LaneLocationProviderTest,
       GivenPositionOnTwoLanesWithSecondLaneWithSuccessor_WhenFindBestLaneCandidate_ThenReturnSecondLane)
{
    auto gtgen_map = test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanes();
    gtgen_map->GetLane(6).successors.clear();
    auto expected_lane = gtgen_map->GetLane(9);

    LaneLocationProvider lane_location_provider{*gtgen_map};
    auto current_lane = lane_location_provider.FindBestLaneCandidate({100_m, 0_m, 0_m});

    EXPECT_EQ(expected_lane.id, current_lane->id);
}

TEST_F(LaneLocationProviderTest,
       GivenPositionOnTwoLanesBothWithSuccessors_WhenFindBestLaneCandidate_ThenReturnFirstLane)
{
    auto gtgen_map = test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanes();

    LaneLocationProvider lane_location_provider{*gtgen_map};
    auto current_lane = lane_location_provider.FindBestLaneCandidate({100_m, 0_m, 0_m});

    EXPECT_EQ(6, current_lane->id);
}

TEST_F(LaneLocationProviderTest,
       GivenPositionOnTwoLanesWithSecondLaneSuccessorNotInMap_WhenFindBestLaneCandidate_ThenReturnFirstLane)
{
    auto gtgen_map = test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanesEachWithoutSuccessors();
    auto& lane_with_not_existing_successor = gtgen_map->GetLane(9);
    lane_with_not_existing_successor.successors.push_back(42);

    LaneLocationProvider lane_location_provider{*gtgen_map};
    auto current_lane = lane_location_provider.FindBestLaneCandidate({100_m, 0_m, 0_m});

    EXPECT_EQ(6, current_lane->id);
}

TEST_F(LaneLocationProviderTest,
       GivenOverlappingLanesAtSameAltitude_WhenGetSortedLaneIdsAtPosition_ThenIdsSortedByDirection)
{
    auto gtgen_map = test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanesEachWithoutSuccessors();

    LaneLocationProvider lane_location_provider{*gtgen_map};
    auto lane_ids = lane_location_provider.GetSortedLaneIdsAtPosition({100_m, 1.25_m, 0_m}, {0.5_rad, 0_rad, 0_rad});

    EXPECT_EQ(2, lane_ids.size());

    EXPECT_EQ(9, lane_ids.at(0));
    EXPECT_EQ(6, lane_ids.at(1));
}

TEST_F(LaneLocationProviderTest,
       GivenOverlappingLanesAtDifferentAltitude_WhenGetSortedLaneIdsAtPosition_ThenIdsFilteredByAltitude)
{
    auto gtgen_map =
        test_utils::MapCatalogue::MapWithTwoOverlappingEastingLanesAtDifferentAltitudesWithHundredPointsEach();

    LaneLocationProvider lane_location_provider{*gtgen_map};
    auto lane_ids = lane_location_provider.GetSortedLaneIdsAtPosition({50_m, 0.0_m, 6.9_m}, {0_rad, 0_rad, 0_rad});
    EXPECT_EQ(2, lane_ids.size());
    EXPECT_EQ(7, lane_ids.at(0));
    EXPECT_EQ(3, lane_ids.at(1));
}

TEST_F(
    LaneLocationProviderTest,
    GivenOverlappingLanesAtDifferentAltitude_WhenGetSortedLaneIdsAtPositionOnBridge_SortedInGroupsWhichAreSortedByAngle)
{
    auto gtgen_map = test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanesWithSameLanesOnABridge();
    LaneLocationProvider lane_location_provider{*gtgen_map};
    auto lane_ids = lane_location_provider.GetSortedLaneIdsAtPosition({120_m, 0.0_m, 6.9_m}, {0_rad, 0_rad, 0_rad});

    EXPECT_EQ(4, lane_ids.size());
    EXPECT_EQ(24, lane_ids.at(0));
    EXPECT_EQ(27, lane_ids.at(1));
    EXPECT_EQ(6, lane_ids.at(2));
    EXPECT_EQ(9, lane_ids.at(3));
}

TEST_F(LaneLocationProviderTest,
       GivenOverlappingLanesAtDifferentAltitude_WhenGetSortedLaneIdsAtPosition_SortedInGroupsWhichAreSortedByAngle)
{
    auto gtgen_map = test_utils::MapCatalogue::MapSplitOneLaneToTheLeftIntoTwoLanesWithSameLanesOnABridge();
    LaneLocationProvider lane_location_provider{*gtgen_map};
    auto lane_ids = lane_location_provider.GetSortedLaneIdsAtPosition({120_m, 0.0_m, 0_m}, {0_rad, 0_rad, 0_rad});

    EXPECT_EQ(2, lane_ids.size());
    EXPECT_EQ(6, lane_ids.at(0));
    EXPECT_EQ(9, lane_ids.at(1));
}

TEST_F(LaneLocationProviderTest,
       GivenOverlappingLanesAtSameAltitudeAndDirection_WhenGetSortedLaneIdsAtPosition_ThenIdsSortedByValue)
{
    auto gtgen_map = test_utils::MapCatalogue::MapWithTwoOverlappingEastingLaneWithHundredPointsEach();

    LaneLocationProvider lane_location_provider{*gtgen_map};
    auto lane_ids = lane_location_provider.GetSortedLaneIdsAtPosition({50_m, 0_m, 0_m}, {0_rad, 0_rad, 0_rad});

    EXPECT_EQ(2, lane_ids.size());

    EXPECT_EQ(3, lane_ids.at(0));
    EXPECT_EQ(7, lane_ids.at(1));
}

TEST_F(LaneLocationProviderTest,
       GiveRefPosOnLaneWithZeroLateralDistanceInAnyDirection_WhenGetPosition_ThenExpectPositionWithValue)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(5, 3);
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t lateral_distance{0.0};
    const mantle_api::LateralDisplacementDirection direction = mantle_api::LateralDisplacementDirection::kAny;

    const auto actual_position = lane_location_provider.GetPosition(ref_pose, direction, lateral_distance);

    EXPECT_TRUE(actual_position.has_value());
}

TEST_F(LaneLocationProviderTest,
       GiveRefPosNotOnLaneWithZeroLateralDistanceInAnyDirection_WhenGetPosition_ThenExpectNoPosition)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::MapWithOneEastingLaneWithNPoints(5, 3);
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{5.0_m, 5.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t lateral_distance{0.0};
    const mantle_api::LateralDisplacementDirection direction = mantle_api::LateralDisplacementDirection::kAny;

    const auto actual_position = lane_location_provider.GetPosition(ref_pose, direction, lateral_distance);

    EXPECT_FALSE(actual_position.has_value());
}

TEST_F(LaneLocationProviderTest,
       GiveRefPosOnLaneWithReasonablePositiveLateralDistanceInAnyDirection_WhenGetPosition_ThenExpectPositionWithValue)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t lateral_distance{3.0};
    const mantle_api::LateralDisplacementDirection direction = mantle_api::LateralDisplacementDirection::kAny;

    const auto actual_position = lane_location_provider.GetPosition(ref_pose, direction, lateral_distance);

    EXPECT_TRUE(actual_position.has_value());
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 3.0_m, 0.0_m), actual_position.value())
}

TEST_F(LaneLocationProviderTest,
       GiveRefPosOnLaneWithReasonableNegativeLateralDistanceInAnyDirection_WhenGetPosition_ThenExpectPositionWithValue)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t lateral_distance{-3.0};
    const mantle_api::LateralDisplacementDirection direction = mantle_api::LateralDisplacementDirection::kAny;

    const auto actual_position = lane_location_provider.GetPosition(ref_pose, direction, lateral_distance);

    EXPECT_TRUE(actual_position.has_value());
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 3.0_m, 0.0_m), actual_position.value())
}

TEST_F(
    LaneLocationProviderTest,
    GiveRefPosOnLeftmostLaneWithLateralDistanceMoreThanLaneWidthInAnyDirection_WhenGetPosition_ThenExpectPositionWithValue)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 10.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t lateral_distance{4.0};
    const mantle_api::LateralDisplacementDirection direction = mantle_api::LateralDisplacementDirection::kAny;

    const auto actual_position = lane_location_provider.GetPosition(ref_pose, direction, lateral_distance);

    EXPECT_TRUE(actual_position.has_value());
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 6.0_m, 0.0_m), actual_position.value())
}

TEST_F(
    LaneLocationProviderTest,
    GiveRefPosOnRightmostLaneWithNegativeLateralDistanceMoreThanLaneWidthInAnyDirection_WhenGetPosition_ThenExpectPositionWithValue)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t lateral_distance{-10.0};
    const mantle_api::LateralDisplacementDirection direction = mantle_api::LateralDisplacementDirection::kAny;

    const auto actual_position = lane_location_provider.GetPosition(ref_pose, direction, lateral_distance);

    EXPECT_TRUE(actual_position.has_value());
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 10.0_m, 0.0_m), actual_position.value())
}

TEST_F(LaneLocationProviderTest,
       GiveRefPosOnLaneWithReasonableLateralDistanceInLeftDirection_WhenGetPosition_ThenExpectPositionWithValue)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t lateral_distance{3.0};
    const mantle_api::LateralDisplacementDirection direction = mantle_api::LateralDisplacementDirection::kLeft;

    const auto actual_position = lane_location_provider.GetPosition(ref_pose, direction, lateral_distance);

    EXPECT_TRUE(actual_position.has_value());
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 3.0_m, 0.0_m), actual_position.value())
}

TEST_F(
    LaneLocationProviderTest,
    GiveRefPosOnLaneWithReasonableNegetiveLateralDistanceInLeftDirection_WhenGetPosition_ThenExpectValidPoseOnRightSide)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 5.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t lateral_distance{-3.0};
    const mantle_api::LateralDisplacementDirection direction = mantle_api::LateralDisplacementDirection::kLeft;

    const auto actual_position = lane_location_provider.GetPosition(ref_pose, direction, lateral_distance);

    EXPECT_TRUE(actual_position.has_value());
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0.0_m, 2.0_m, 0.0_m), actual_position.value());
}

TEST_F(LaneLocationProviderTest,
       GiveRefPosOnLaneWithUnreasonableLateralDistanceInLeftDirection_WhenGetPosition_ThenExpectNoPose)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t lateral_distance{15.0};
    const mantle_api::LateralDisplacementDirection direction = mantle_api::LateralDisplacementDirection::kLeft;

    const auto actual_position = lane_location_provider.GetPosition(ref_pose, direction, lateral_distance);

    EXPECT_FALSE(actual_position.has_value());
}

TEST_F(LaneLocationProviderTest,
       GiveRefPosOnLaneWithUnreasonableNegativeLateralDistanceInLeftDirection_WhenGetPosition_ThenExpectNoPose)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t lateral_distance{-10.0};
    const mantle_api::LateralDisplacementDirection direction = mantle_api::LateralDisplacementDirection::kLeft;

    const auto actual_position = lane_location_provider.GetPosition(ref_pose, direction, lateral_distance);

    EXPECT_FALSE(actual_position.has_value());
}

TEST_F(LaneLocationProviderTest,
       GiveRefPosOnLaneWithReasonableLateralDistanceInRightDirection_WhenGetPosition_ThenExpectPositionWithValue)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 4.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t lateral_distance{3.0};
    const mantle_api::LateralDisplacementDirection direction = mantle_api::LateralDisplacementDirection::kRight;

    const auto actual_position = lane_location_provider.GetPosition(ref_pose, direction, lateral_distance);

    EXPECT_TRUE(actual_position.has_value());
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0.0_m, 1.0_m, 0.0_m), actual_position.value());
}

TEST_F(
    LaneLocationProviderTest,
    GiveRefPosOnLaneWithReasonableNegetiveLateralDistanceInRightDirection_WhenGetPosition_ThenExpectValidPositionOnLeftSide)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t lateral_distance{-3.0};
    const mantle_api::LateralDisplacementDirection direction = mantle_api::LateralDisplacementDirection::kRight;

    const auto actual_position = lane_location_provider.GetPosition(ref_pose, direction, lateral_distance);

    EXPECT_TRUE(actual_position.has_value());
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 3.0_m, 0.0_m), actual_position.value());
}

TEST_F(LaneLocationProviderTest,
       GiveRefPosOnLaneWithUnreasonableLateralDistanceInRightDirection_WhenGetPosition_ThenExpectNoPose)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t lateral_distance{10.0};
    const mantle_api::LateralDisplacementDirection direction = mantle_api::LateralDisplacementDirection::kRight;

    const auto actual_position = lane_location_provider.GetPosition(ref_pose, direction, lateral_distance);

    EXPECT_FALSE(actual_position.has_value());
}

TEST_F(LaneLocationProviderTest,
       GiveRefPosOnLaneWithUnreasonableNegativeLateralDistanceInRightDirection_WhenGetPosition_ThenExpectNoPose)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {0.0_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t lateral_distance{-15.0};
    const mantle_api::LateralDisplacementDirection direction = mantle_api::LateralDisplacementDirection::kRight;

    const auto actual_position = lane_location_provider.GetPosition(ref_pose, direction, lateral_distance);

    EXPECT_FALSE(actual_position.has_value());
}

TEST_F(LaneLocationProviderTest,
       GiveRefPosOnLaneWithReasonableLateralDistanceAndYawAndRightDirection_WhenGetPosition_ThenExpectPositionWithValue)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 4.0_m, 0.0_m}, {0.785398_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t lateral_distance{2.0};
    const mantle_api::LateralDisplacementDirection direction = mantle_api::LateralDisplacementDirection::kRight;

    const auto actual_position = lane_location_provider.GetPosition(ref_pose, direction, lateral_distance);

    EXPECT_TRUE(actual_position.has_value());
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 2.0_m, 0.0_m), actual_position.value());
}

TEST_F(
    LaneLocationProviderTest,
    GiveRefPosOnLaneWithReasonableLateralDistanceAndNegativeYawAndRightDirection_WhenGetPosition_ThenExpectPositionWithValue)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 4.0_m, 0.0_m}, {-0.785398_rad, 0.0_rad, 0.0_rad}};
    const units::length::meter_t lateral_distance{2.0};
    const mantle_api::LateralDisplacementDirection direction = mantle_api::LateralDisplacementDirection::kRight;

    const auto actual_position = lane_location_provider.GetPosition(ref_pose, direction, lateral_distance);

    EXPECT_TRUE(actual_position.has_value());
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 2.0_m, 0.0_m), actual_position.value());
}

TEST_F(
    LaneLocationProviderTest,
    GiveRefPosOnLaneWithReasonableLateralDistanceAnd180DegreeYawAndRightDirection_WhenGetPosition_ThenExpectPositionWithValue)
{
    test_utils::MapCatalogueIdProvider::Instance().Reset();
    auto gtgen_map = test_utils::MapCatalogue::Map3x3StraightEastingLanesWithHundredPoints();
    LaneLocationProvider lane_location_provider{*gtgen_map};

    const mantle_api::Pose ref_pose{{0.0_m, 0.0_m, 0.0_m}, {units::angle::radian_t(M_PI), 0.0_rad, 0.0_rad}};
    const units::length::meter_t lateral_distance{2.0};
    const mantle_api::LateralDisplacementDirection direction = mantle_api::LateralDisplacementDirection::kRight;

    const auto actual_position = lane_location_provider.GetPosition(ref_pose, direction, lateral_distance);

    EXPECT_TRUE(actual_position.has_value());
    EXPECT_TRIPLE(mantle_api::Vec3<units::length::meter_t>(0_m, 2.0_m, 0.0_m), actual_position.value());
}
}  // namespace gtgen::core::environment::map
