/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_MAPAPITOGTGENMAPCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_MAPAPITOGTGENMAPCONVERTER_H

#include "Core/Environment/Map/Common/i_any_to_gtgenmap_converter.h"
#include "Core/Environment/Map/MapApiConverter/Internal/mapapi_to_gtgenmap_converter_impl.h"

#include <MapAPI/map.h>

namespace gtgen::core::environment::map
{

class MapApiToGtGenMapConverter : public IAnyToGtGenMapConverter
{
  public:
    MapApiToGtGenMapConverter(service::utility::UniqueIdProvider& unique_id_provider,
                              const map_api::Map& data,
                              GtGenMap& gtgen_map);

    MapApiToGtGenMapConverter() = delete;
    MapApiToGtGenMapConverter(const MapApiToGtGenMapConverter&) = delete;
    MapApiToGtGenMapConverter(MapApiToGtGenMapConverter&&) = delete;
    MapApiToGtGenMapConverter& operator=(const MapApiToGtGenMapConverter&) = delete;
    MapApiToGtGenMapConverter& operator=(MapApiToGtGenMapConverter&&) = delete;
    ~MapApiToGtGenMapConverter() override;

    /// @copydoc IAnyToGtGenMapConverter::Convert()
    void Convert() override;

    /// @copydoc IAnyToGtGenMapConverter::GetNativeToGtGenTrafficLightIdMap()
    std::map<mantle_api::UniqueId, mantle_api::UniqueId> GetNativeToGtGenTrafficLightIdMap() const override;

  private:
    std::unique_ptr<MapApiToGtGenMapConverterImpl> impl_;
};

}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_MAPAPITOGTGENMAPCONVERTER_H
