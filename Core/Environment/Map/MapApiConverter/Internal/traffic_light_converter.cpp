/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/traffic_light_converter.h"

#include "Core/Service/Logging/logging.h"

namespace gtgen::core::environment::map
{
OsiTrafficLightColor ConvertTrafficLightColor(const map_api::TrafficLight::TrafficSignal::Color& from)
{
    return static_cast<OsiTrafficLightColor>(from);
}

OsiTrafficLightIcon ConvertTrafficLightIcon(const map_api::TrafficLight::TrafficSignal::Icon& from)
{
    return static_cast<OsiTrafficLightIcon>(from);
}

OsiTrafficLightMode ConvertTrafficLightMode(const map_api::TrafficLight::TrafficSignal::Mode& from)
{
    return static_cast<OsiTrafficLightMode>(from);
}

TrafficLightBulb ConvertTrafficLightBulb(const map_api::TrafficLight::TrafficSignal& from)
{
    TrafficLightBulb result;
    result.id = from.id;

    result.pose = mantle_api::Pose{from.base.position, from.base.orientation};
    result.dimensions = from.base.dimension;

    result.color = ConvertTrafficLightColor(from.color);
    result.icon = ConvertTrafficLightIcon(from.icon);
    result.mode = ConvertTrafficLightMode(from.mode);
    result.count = from.counter;

    for (const auto& assigned_lane : from.assigned_lanes)
    {
        result.assigned_lanes.emplace_back(assigned_lane.get().id);
    }

    return result;
}

TrafficLight ConvertTrafficLight(const map_api::TrafficLight& from)
{
    TrafficLight result;
    result.id = from.id;
    TrafficLightBulbStates states;

    for (const auto& traffic_signal : from.traffic_signals)
    {
        auto traffic_light_bulb = ConvertTrafficLightBulb(traffic_signal);
        result.light_bulbs.emplace_back(traffic_light_bulb);
        states.emplace(std::make_pair(traffic_light_bulb.color, traffic_light_bulb.mode));
    }
    result.SetBulbStates(states);
    return result;
}

}  // namespace gtgen::core::environment::map
