/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/lane_group_converter.h"

#include "Core/Service/Logging/logging.h"

#include <unordered_map>

namespace gtgen::core::environment::map
{

LaneGroup::Type ConvertLaneGroupType(const map_api::LaneGroup::Type from)
{
    switch (from)
    {
        case map_api::LaneGroup::Type::kUnknown:
            return LaneGroup::Type::kUnknown;
        case map_api::LaneGroup::Type::kOther:
            return LaneGroup::Type::kOther;
        case map_api::LaneGroup::Type::kOneWay:
            return LaneGroup::Type::kOneWay;
        case map_api::LaneGroup::Type::kJunction:
            return LaneGroup::Type::kJunction;
        default:
            Warn("The given map_api::LaneGroup::Type:: {} cannot be converted! Return Unknown as default.",
                 static_cast<std::uint8_t>(from));
            return LaneGroup::Type::kUnknown;
    }
}

LaneGroup CreateLaneGroup(const map_api::Identifier id, const map_api::LaneGroup::Type type)
{
    return LaneGroup{static_cast<mantle_api::UniqueId>(id), ConvertLaneGroupType(type)};
}

void AppendLaneAndLaneBoundaryToLaneGroupIdMap(
    const map_api::LaneGroup& lane_group,
    std::unordered_map<mantle_api::UniqueId, mantle_api::UniqueId>& lane_to_lane_group_id_map,
    std::unordered_map<mantle_api::UniqueId, mantle_api::UniqueId>& lane_boundary_to_lane_group_id_map)
{
    for (const auto& lane : lane_group.lanes)
    {
        lane_to_lane_group_id_map.emplace(lane.get().id, lane_group.id);
    }

    for (const auto& lane_boundary : lane_group.lane_boundaries)
    {
        lane_boundary_to_lane_group_id_map.emplace(lane_boundary.get().id, lane_group.id);
    }
}

}  // namespace gtgen::core::environment::map
