/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/traffic_sign_converter.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace gtgen::core::environment::map::map
{
using ::testing::ElementsAre;
using units::literals::operator""_m;
using units::literals::operator""_rad;

static map_api::Lane CreateLane(const map_api::Identifier id = 123)
{
    map_api::Lane lane;
    lane.id = id;
    return lane;
}

TEST(ConvertTrafficSignValueTest, GivenTrafficSignValue_WhenConvert_ThenSignValueInformationIsSet)
{
    map_api::TrafficSignValue input;
    input.value = 123.45;
    input.value_unit = map_api::TrafficSignValue::Unit::kKilometerPerHour;
    input.text = "Speed Limit 50";

    const auto result = ConvertTrafficSignValue(input);

    EXPECT_EQ(result.value, input.value);
    EXPECT_EQ(result.value_unit, osi::OsiTrafficSignValueUnit::kKilometerPerHour);
    EXPECT_EQ(result.text, input.text);
}

TEST(ConvertMainSignTest, GivenMainSignWithId_WhenConvert_ThenTrafficSignHasSameId)
{
    map_api::MainSign main_sign{};
    map_api::Identifier expected_id = 42;

    const auto result = ConvertMainSign(main_sign, expected_id);

    EXPECT_EQ(result.id, expected_id);
}

TEST(ConvertMainSignTest, GivenMainSignWithAssignedLanes_WhenConvert_ThenTrafficSignHasSameAssignedLanes)
{
    map_api::MainSign main_sign{};
    map_api::Identifier sign_id = 42;
    auto lane1 = CreateLane(1);
    auto lane2 = CreateLane(2);
    main_sign.assigned_lanes = {lane1, lane2};

    const auto result = ConvertMainSign(main_sign, sign_id);

    ASSERT_THAT(result.assigned_lanes, ElementsAre(lane1.id, lane2.id));
}

TEST(ConvertMainSignTest, GivenMainSign_WhenConvert_ThenTrafficSignBasePropertiesAreSet)
{
    map_api::MainSign main_sign{};
    map_api::Identifier sign_id = 42;
    main_sign.base.position = {1.0_m, 2.0_m, 3.0_m};
    main_sign.base.orientation = {4.0_rad, 5.0_rad, 6.0_rad};
    main_sign.base.dimension = {7.0_m, 8.0_m, 9.0_m};

    const auto result = ConvertMainSign(main_sign, sign_id);

    EXPECT_EQ(result.pose.position, main_sign.base.position);
    EXPECT_EQ(result.pose.orientation, main_sign.base.orientation);
    EXPECT_EQ(result.dimensions, main_sign.base.dimension);
}

TEST(ConvertMainSignTest, GivenMainSign_WhenConvert_ThenMainSignTypeIsSet)
{

    map_api::MainSign main_sign{};
    map_api::Identifier sign_id = 42;
    main_sign.variability = map_api::TrafficSignVariability::kVariable;
    main_sign.direction_scope = map_api::DirectionScope::kRight;

    const auto result = ConvertMainSign(main_sign, sign_id);

    EXPECT_EQ(result.variability, osi::OsiTrafficSignVariability::kVariable);
    EXPECT_EQ(result.direction_scope, osi::OsiTrafficSignDirectionScope::kRight);
}

TEST(ConvertMainSignTest, GivenMainSign_WhenConvert_ThenValueIsSet)
{

    map_api::MainSign main_sign{};
    map_api::Identifier sign_id = 42;
    main_sign.value.value = 50.0;
    main_sign.value.value_unit = map_api::TrafficSignValue::Unit::kKilometerPerHour;
    main_sign.value.text = "Speed Limit 50";

    const auto result = ConvertMainSign(main_sign, sign_id);

    EXPECT_EQ(result.value_information.value, main_sign.value.value);
    EXPECT_EQ(result.value_information.text, main_sign.value.text);
    EXPECT_EQ(result.value_information.value_unit, osi::OsiTrafficSignValueUnit::kKilometerPerHour);
}

TEST(ConvertMainSignTest, GivenMainSignWithCode_WhenConvert_ThenTrafficSignStvoIdIsSetCorrectly)
{
    map_api::MainSign main_sign{};
    map_api::Identifier sign_id = 42;
    main_sign.code = "237";

    const auto result = ConvertMainSign(main_sign, sign_id);

    EXPECT_EQ(result.stvo_id, main_sign.code);
}

TEST(ConvertMainSignTest, GivenMainSignWithCodeAndSubCode_WhenConvert_ThenTrafficSignStvoIdIsSetCorrectly)
{
    map_api::MainSign main_sign{};
    map_api::Identifier sign_id = 42;
    main_sign.code = "237";
    main_sign.sub_code = "10";

    const auto result = ConvertMainSign(main_sign, sign_id);

    EXPECT_EQ(result.stvo_id, main_sign.code + "-" + main_sign.sub_code);
}

TEST(ConvertSupplementarySignTest, GivenSupplementarySignWithMainSign_WhenConvert_ThenMainSignIsSet)
{
    map_api::SupplementarySign supplementary_sign{};
    auto main_sign = std::make_shared<MountedSign>();

    const auto result = ConvertSupplementarySign(supplementary_sign, main_sign.get());

    EXPECT_TRUE(result.main_sign);
}

TEST(ConvertSupplementarySignTest, GivenSupplementarySignWithVariability_WhenConvert_ThenVariabilityIsSet)
{
    map_api::SupplementarySign supplementary_sign{};
    supplementary_sign.variability = map_api::TrafficSignVariability::kVariable;

    const auto result = ConvertSupplementarySign(supplementary_sign);

    EXPECT_EQ(result.variability, osi::OsiTrafficSignVariability::kVariable);
}

TEST(ConvertSupplementarySignTest, GivenSupplementarySignWithActors_WhenConvert_ThenActorsAreSet)
{
    map_api::SupplementarySign supplementary_sign{};
    supplementary_sign.actors.emplace_back(map_api::SupplementarySignActor::kBuses);
    supplementary_sign.actors.emplace_back(map_api::SupplementarySignActor::kCars);

    const auto result = ConvertSupplementarySign(supplementary_sign);
    ASSERT_EQ(result.actors.size(), supplementary_sign.actors.size());
}

TEST(ConvertSupplementarySignTest, GivenSupplementarySignWithType_WhenConvert_ThenTypeIsSet)
{
    map_api::SupplementarySign supplementary_sign{};
    supplementary_sign.type = map_api::SupplementarySignType::kRain;

    const auto result = ConvertSupplementarySign(supplementary_sign);

    EXPECT_EQ(result.type, OsiSupplementarySignType::kRain);
}

TEST(ConvertSupplementarySignTest, GivenSupplementarySign_WhenConvert_ThenBasePropertiesAreSet)
{
    map_api::SupplementarySign supplementary_sign{};
    supplementary_sign.base.position = {1.0_m, 2.0_m, 3.0_m};
    supplementary_sign.base.orientation = {4.0_rad, 5.0_rad, 6.0_rad};
    supplementary_sign.base.dimension = {7.0_m, 8.0_m, 9.0_m};

    const auto result = ConvertSupplementarySign(supplementary_sign);

    EXPECT_EQ(result.pose.position, supplementary_sign.base.position);
    EXPECT_EQ(result.pose.orientation, supplementary_sign.base.orientation);
    EXPECT_EQ(result.dimensions, supplementary_sign.base.dimension);
}

TEST(ConvertSupplementarySignTest, GivenSupplementarySign_WhenConvert_ThenValuesAreSet)
{

    map_api::SupplementarySign supplementary_sign{};
    supplementary_sign.values.resize(10);

    const auto result = ConvertSupplementarySign(supplementary_sign);

    EXPECT_EQ(result.value_information.size(), supplementary_sign.values.size());
}

TEST(FillTrafficSignTest, GivenTrafficSign_WhenFillGtGenMap_ThenTrafficSignsAreSet)
{
    GtGenMap gtgen_map;

    map_api::TrafficSign traffic_sign;
    traffic_sign.id = 42;
    traffic_sign.main_sign.type = map_api::MainSignType::kSpeedLimitBegin;
    map_api::SupplementarySign supplementary_sign1{};
    supplementary_sign1.type = map_api::SupplementarySignType::kRain;
    traffic_sign.supplementary_signs.emplace_back(supplementary_sign1);
    map_api::SupplementarySign supplementary_sign2{};
    supplementary_sign2.type = map_api::SupplementarySignType::kSnow;
    traffic_sign.supplementary_signs.emplace_back(supplementary_sign2);

    FillTrafficSign(traffic_sign, gtgen_map);

    ASSERT_EQ(gtgen_map.traffic_signs.size(), 2);
    ASSERT_TRUE(dynamic_cast<TrafficSign*>(gtgen_map.traffic_signs.at(0).get()));

    const auto* mounted_sign = dynamic_cast<MountedSign*>(gtgen_map.traffic_signs.at(1).get());
    ASSERT_TRUE(mounted_sign);
    EXPECT_EQ(mounted_sign->supplementary_signs.size(), 2);
}

}  // namespace gtgen::core::environment::map::map
