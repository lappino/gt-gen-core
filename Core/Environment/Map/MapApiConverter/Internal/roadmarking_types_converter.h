/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_ROADMARKINGTYPESCONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_ROADMARKINGTYPESCONVERTER_H

#include "Core/Environment/Map/GtGenMap/signs.h"
#include "Core/Service/Osi/traffic_sign_types.h"

#include <MapAPI/road_marking.h>

namespace gtgen::core::environment::map
{
osi::OsiRoadMarkingsType ConvertRoadMarkingType(const map_api::RoadMarking::Type& from);
osi::OsiRoadMarkingColor ConvertRoadMarkingColor(const map_api::RoadMarking::Color& from);
}  // namespace gtgen::core::environment::map

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_MAPAPICONVERTER_INTERNAL_ROADMARKINGTYPESCONVERTER_H
