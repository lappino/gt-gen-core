/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/Common/exceptions.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::map
{

class GtGenMapTest : public testing::Test
{
  protected:
    static void ExpectEqualLanes(const Lane& lane_0, const Lane& lane_1)
    {
        EXPECT_EQ(lane_0.id, lane_1.id);
        EXPECT_EQ(lane_0.parent_lane_group_id, lane_1.parent_lane_group_id);
    }

    GtGenMap map_;
};

TEST_F(GtGenMapTest, GivenEmptyGtGenMap_WhenMapTypeIsSetToOpenDrive_ThenMapTypeWillBeIdentifiedAsOpenDrive)
{
    map_.SetSourceMapType(GtGenMap::SourceMapType::kOpenDrive);

    EXPECT_TRUE(map_.IsOpenDrive());
    EXPECT_FALSE(map_.IsNDS());
}

TEST_F(GtGenMapTest, GivenEmptyGtGenMap_WhenMapTypeIsSetToNDS_ThenMapTypeWillBeIdentifiedAsNDS)
{
    map_.SetSourceMapType(GtGenMap::SourceMapType::kNDS);

    EXPECT_FALSE(map_.IsOpenDrive());
    EXPECT_TRUE(map_.IsNDS());
}

TEST_F(GtGenMapTest, GivenMapWithSingleLaneGroup_WhenGettingLaneGroup_ThenLaneGroupCanBeAccessedUnchanged)
{
    mantle_api::UniqueId expected_lane_group_id{42};
    LaneGroup lane_group{expected_lane_group_id, LaneGroup::Type::kJunction};

    mantle_api::UniqueId dummy_lane_id{69};
    lane_group.lane_ids.push_back(dummy_lane_id);

    mantle_api::UniqueId dummy_lane_boundary_id{24};
    lane_group.lane_boundary_ids.push_back(dummy_lane_boundary_id);

    map_.AddLaneGroup(lane_group);

    ASSERT_EQ(map_.GetLaneGroups().size(), 1);
    EXPECT_EQ(map_.GetLaneGroups().at(0).id, expected_lane_group_id);
    EXPECT_EQ(map_.GetLaneGroup(expected_lane_group_id).id, expected_lane_group_id);
}

TEST_F(GtGenMapTest, GivenEmptyMap_WhenGettingLaneGroup_ThenExceptionIsThrown)
{
    ASSERT_EQ(map_.GetLaneGroups().size(), 0);
    EXPECT_THROW(map_.GetLaneGroup(0), exception::MapException);
}

TEST_F(GtGenMapTest, GivenEmptyGtGenMap_WhenFindLane_ThenNullptrIsReturned)
{
    EXPECT_EQ(map_.FindLane(0), nullptr);
}

TEST_F(GtGenMapTest, GivenMapWithSingleLane_WhenFindLane_ThenLaneIsReturned)
{
    mantle_api::UniqueId expected_lane_id{69};

    LaneGroup lane_group{42, LaneGroup::Type::kJunction};
    Lane input_lane{expected_lane_id};
    input_lane.parent_lane_group_id = lane_group.id;

    map_.AddLaneGroup(lane_group);
    map_.AddLane(lane_group.id, input_lane);
    Lane* found_lane = map_.FindLane(expected_lane_id);

    ASSERT_NE(found_lane, nullptr);
    ExpectEqualLanes(*found_lane, input_lane);
}

TEST_F(GtGenMapTest, GivenEmptyGtGenMap_WhenGetLane_ThenExceptionIsThrown)
{
    EXPECT_THROW(map_.GetLane(0), exception::LaneNotFound);
}

TEST_F(GtGenMapTest, GivenMapWithSingleLane_WhenGetLane_ThenLaneIsReturned)
{
    mantle_api::UniqueId expected_lane_id{69};

    LaneGroup lane_group{42, LaneGroup::Type::kJunction};
    Lane input_lane{expected_lane_id};
    input_lane.parent_lane_group_id = lane_group.id;

    map_.AddLaneGroup(lane_group);
    map_.AddLane(lane_group.id, input_lane);

    EXPECT_NO_THROW(map_.GetLane(expected_lane_id));
    Lane& found_lane = map_.GetLane(expected_lane_id);
    ExpectEqualLanes(found_lane, input_lane);
}

TEST_F(GtGenMapTest, GivenMapWithSingleLane_WhenSetLocalLaneId_ThenLocalLaneIdIsSet)
{
    mantle_api::LaneId expected_local_id{5};

    Lane input_lane{42};
    input_lane.local_id = expected_local_id;
    LaneGroup lane_group{1, LaneGroup::Type::kOther};
    map_.AddLaneGroup(lane_group);
    map_.AddLane(lane_group.id, input_lane);

    EXPECT_EQ(map_.GetLane(42).local_id, expected_local_id);
}

TEST_F(GtGenMapTest, GivenEmptyGtGenMap_WhenALaneGroupWithASingleBoundary_ThenBoundaryCanBeAccessedUnchanged)
{
    LaneGroup input_lane_group{42, LaneGroup::Type::kOther};
    LaneBoundary input_boundary{4, LaneBoundary::Type::kSolidLine, LaneBoundary::Color::kWhite};

    map_.AddLaneGroup(input_lane_group);
    map_.AddLaneBoundary(input_lane_group.id, input_boundary);

    const LaneGroup& lane_group = map_.GetLaneGroups().at(0);
    EXPECT_EQ(lane_group.lane_boundary_ids.size(), 1);

    const auto& boundary = map_.GetLaneBoundary(lane_group.lane_boundary_ids[0]);
    EXPECT_EQ(boundary.id, 4);
    EXPECT_EQ(boundary.type, LaneBoundary::Type::kSolidLine);
    EXPECT_EQ(boundary.color, LaneBoundary::Color::kWhite);
}

TEST_F(GtGenMapTest, GivenEmptyGtGenMap_WhenALaneGroupWithThreeLanesIsAdded_ThenEachLaneCanBeAccessedUnchanged)
{
    LaneGroup lane_group{42, LaneGroup::Type::kOther};

    map_.AddLaneGroup(lane_group);

    Lane input_lane0{1};
    Lane input_lane1{2};
    Lane input_lane2{3};

    input_lane0.parent_lane_group_id = lane_group.id;
    input_lane1.parent_lane_group_id = lane_group.id;
    input_lane2.parent_lane_group_id = lane_group.id;

    map_.AddLane(lane_group.id, input_lane0);
    map_.AddLane(lane_group.id, input_lane1);
    map_.AddLane(lane_group.id, input_lane2);

    const LaneGroup& lane_group_0 = map_.GetLaneGroups().at(0);

    const Lane& lane_0 = map_.GetLane(lane_group_0.lane_ids.at(0));
    const Lane& lane_1 = map_.GetLane(lane_group_0.lane_ids.at(1));
    const Lane& lane_2 = map_.GetLane(lane_group_0.lane_ids.at(2));

    ExpectEqualLanes(lane_0, input_lane0);
    ExpectEqualLanes(lane_1, input_lane1);
    ExpectEqualLanes(lane_2, input_lane2);
}

TEST_F(GtGenMapTest, GivenGtGenMap_WhenAnExistingLaneIsRequested_ThenTheLaneWillBeReturned)
{
    LaneGroup lane_group{1, LaneGroup::Type::kOther};
    Lane input_lane{1};
    input_lane.parent_lane_group_id = lane_group.id;

    map_.AddLaneGroup(lane_group);
    map_.AddLane(lane_group.id, input_lane);

    auto lane = map_.GetLane(1);
    ExpectEqualLanes(lane, input_lane);
}

TEST_F(GtGenMapTest, GivenGtGenMapWithLaneGroup_WhenLaneForNonExistingLaneIdIsRequested_ThenMapExceptionIsThrown)
{
    LaneGroup lane_group{1, LaneGroup::Type::kOther};
    map_.AddLaneGroup(lane_group);
    map_.AddLane(lane_group.id, Lane{1});

    EXPECT_THROW(map_.GetLane(42), exception::MapException);
}

TEST_F(GtGenMapTest, GivenGtGenMap_WhenLaneForNonExistingLaneGroupIdIsRequested_ThenMapExceptionIsThrown)
{
    LaneGroup lane_group{1, LaneGroup::Type::kOther};
    map_.AddLaneGroup(lane_group);
    map_.AddLane(lane_group.id, Lane{1});

    EXPECT_THROW(map_.GetLane(42), exception::MapException);
}

TEST_F(GtGenMapTest, GivenGtGenMap_WhenAnExistingLaneIsRequestedByMapRelation_ThenTheLaneWillBeReturned)
{
    LaneGroup lane_group{1, LaneGroup::Type::kOther};
    map_.AddLaneGroup(lane_group);
    map_.AddLane(lane_group.id, Lane{1});

    auto lane = map_.GetLane(1);
    EXPECT_EQ(lane.id, 1);
}

TEST_F(GtGenMapTest, GivenGtGenMap_WhenLaneForNonExistingLaneGroupIdIsRequestedByMapRelation_ThenMapExceptionIsThrown)
{
    LaneGroup lane_group{1, LaneGroup::Type::kOther};
    map_.AddLaneGroup(lane_group);
    map_.AddLane(lane_group.id, Lane{1});

    EXPECT_THROW(map_.GetLane(42), exception::MapException);
}

TEST_F(GtGenMapTest, GivenGtGenMap_WhenLaneForNonExistingLaneIdIsRequestedByMapRelation_ThenMapExceptionIsThrown)
{
    LaneGroup lane_group{1, LaneGroup::Type::kOther};
    map_.AddLaneGroup(lane_group);
    map_.AddLane(lane_group.id, Lane{1});

    EXPECT_THROW(map_.GetLane(42), exception::MapException);
}

TEST_F(GtGenMapTest, GivenEmptyGtGenMap_WhenThreeSignsAreAdded_ThenTheSignsWillBeContainedCorrectly)
{
    auto ground_sign0 = std::make_shared<GroundSign>();
    auto ground_sign1 = std::make_shared<GroundSign>();
    auto mounted_sign = std::make_shared<MountedSign>();

    map_.traffic_signs.emplace_back(ground_sign0);
    map_.traffic_signs.emplace_back(ground_sign1);
    map_.traffic_signs.emplace_back(mounted_sign);

    EXPECT_EQ(map_.traffic_signs.size(), 3);
    EXPECT_EQ(map_.GetGroundSigns().size(), 2);
    EXPECT_EQ(map_.GetMountedSigns().size(), 1);
}

TEST_F(GtGenMapTest, GivenEmptyGtGenMap_WhenGroundSignIsAdded_ThenTheSignWillBeContainedCorrectly)
{
    auto ground_sign = std::make_shared<GroundSign>();

    map_.traffic_signs.emplace_back(ground_sign);

    EXPECT_EQ(map_.traffic_signs.size(), 1);
}

TEST_F(GtGenMapTest, GivenEmptyGtGenMap_WhenTrafficSignIsAdded_ThenTheSignWillBeContainedCorrectly)
{
    auto ground_sign = std::make_shared<GroundSign>();

    map_.traffic_signs.emplace_back(ground_sign);

    EXPECT_EQ(map_.GetGroundSigns().size(), 1);
}

TEST_F(GtGenMapTest, GivenEmptyGtGenMap_WhenQueryingNonExistingLaneGroup_ThenThrowLaneNotFoundException)
{
    EXPECT_THROW(map_.GetLaneBoundary(0), exception::LaneBoundaryNotFound);
}

TEST_F(GtGenMapTest, GivenEmptyGtGenMap_WhenLaneBoundaryIsAddedAndLaneGroupIsNotAdded_ThenThrowMapException)
{
    LaneGroup lane_group{42, LaneGroup::Type::kOther};
    map_.AddLaneGroup(lane_group);

    EXPECT_THROW(map_.GetLaneBoundary(0), exception::MapException);
}

TEST_F(GtGenMapTest, GivenMapWithAllPossibleElements_WhenClearMap_ThenAllElementsArePurged)
{
    map_.AddLaneGroup({42, LaneGroup::Type::kOther});
    map_.AddLane(42, Lane{0});
    map_.AddLaneBoundary(42, {0, LaneBoundary::Type::kSolidLine, LaneBoundary::Color::kOrange});
    map_.traffic_lights.push_back({});
    map_.traffic_signs.push_back({});
    map_.road_objects.push_back({});
    map_.axis_aligned_world_bounding_box = {{10, 10}, {12, 12}};
    map_.path = "foo";
    map_.projection_string = "bar";
    map_.Add<map_api::ReferenceLine>({});
    map_.Add<map_api::LogicalLane>({});
    map_.Add<map_api::LogicalLaneBoundary>({});

    map_.ClearMap();

    EXPECT_EQ(map_.FindLane(0), nullptr);
    EXPECT_EQ(map_.FindLaneBoundary(0), nullptr);
    EXPECT_EQ(map_.GetLaneGroups().size(), 0);
    EXPECT_EQ(map_.GetLanes().size(), 0);
    EXPECT_EQ(map_.GetLaneBoundaries().size(), 0);
    EXPECT_EQ(map_.traffic_lights.size(), 0);
    EXPECT_EQ(map_.traffic_signs.size(), 0);
    EXPECT_EQ(map_.road_objects.size(), 0);
    EXPECT_EQ(map_.axis_aligned_world_bounding_box.bottom_left, BoundingBox2d{}.bottom_left);
    EXPECT_EQ(map_.axis_aligned_world_bounding_box.top_right, BoundingBox2d{}.top_right);
    EXPECT_EQ(map_.path, "");
    EXPECT_EQ(map_.projection_string, "");
    EXPECT_EQ(map_.GetAll<map_api::ReferenceLine>().size(), 0);
    EXPECT_EQ(map_.GetAll<map_api::LogicalLane>().size(), 0);
    EXPECT_EQ(map_.GetAll<map_api::LogicalLaneBoundary>().size(), 0);
}

}  // namespace gtgen::core::environment::map
