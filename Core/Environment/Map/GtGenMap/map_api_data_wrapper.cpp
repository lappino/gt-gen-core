/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/GtGenMap/map_api_data_wrapper.h"

namespace gtgen::core::environment::map
{

void MapApiDataWrapper::ClearMap()
{
    reference_lines_.clear();
    logical_lanes_.clear();
    logical_lane_boundaries_.clear();

    id_reference_line_index_map_.clear();
    id_logical_lane_index_map_.clear();
    id_logical_lane_boundary_index_map_.clear();
}

template <>
const MapApiDataWrapper::IndexMap& MapApiDataWrapper::GetIndexMap<map_api::ReferenceLine>() const
{
    return id_reference_line_index_map_;
}

template <>
const MapApiDataWrapper::IndexMap& MapApiDataWrapper::GetIndexMap<map_api::LogicalLane>() const
{
    return id_logical_lane_index_map_;
}

template <>
const MapApiDataWrapper::IndexMap& MapApiDataWrapper::GetIndexMap<map_api::LogicalLaneBoundary>() const
{
    return id_logical_lane_boundary_index_map_;
}

template <>
const std::vector<map_api::ReferenceLine>& MapApiDataWrapper::GetDataContainer() const
{
    return reference_lines_;
}

template <>
const std::vector<map_api::LogicalLane>& MapApiDataWrapper::GetDataContainer() const
{
    return logical_lanes_;
}

template <>
const std::vector<map_api::LogicalLaneBoundary>& MapApiDataWrapper::GetDataContainer() const
{
    return logical_lane_boundaries_;
}

template <>
std::string MapApiDataWrapper::GetExceptionString<map_api::ReferenceLine>() const
{
    return "ReferenceLine";
}

template <>
std::string MapApiDataWrapper::GetExceptionString<map_api::LogicalLane>() const
{
    return "LogicalLane";
}

template <>
std::string MapApiDataWrapper::GetExceptionString<map_api::LogicalLaneBoundary>() const
{
    return "LogicalLaneBoundary";
}

}  // namespace gtgen::core::environment::map
