/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/GtGenMap/map_api_data_wrapper.h"

#include <gtest/gtest.h>

#include <tuple>

namespace gtgen::core::environment::map
{

using namespace map_api;

class MapApiDataWrapperFixture : public testing::Test
{
  protected:
    template <class T>
    static void ExpectEqual(const T& right, const T& left)
    {
        EXPECT_EQ(right.id, left.id);
    }

    std::tuple<ReferenceLine, LogicalLane, LogicalLaneBoundary> AddNewElements(mantle_api::UniqueId id)
    {
        ReferenceLine reference_line;
        LogicalLane logical_lane;
        LogicalLaneBoundary logical_lane_boundary;

        reference_line.id = id;
        logical_lane.id = id;
        logical_lane_boundary.id = id;

        map_api_data_.Add(reference_line);
        map_api_data_.Add(logical_lane);
        map_api_data_.Add(logical_lane_boundary);
        return {reference_line, logical_lane, logical_lane_boundary};
    }

    MapApiDataWrapper map_api_data_;
};

TEST_F(MapApiDataWrapperFixture, GivenEmptyData_WhenGettingAnyType_ThenExceptionIsThrown)
{
    ASSERT_EQ(map_api_data_.GetAll<ReferenceLine>().size(), 0);
    ASSERT_EQ(map_api_data_.GetAll<LogicalLane>().size(), 0);
    ASSERT_EQ(map_api_data_.GetAll<LogicalLaneBoundary>().size(), 0);
    EXPECT_THROW(map_api_data_.Get<ReferenceLine>(0), exception::MapException);
    EXPECT_THROW(map_api_data_.Get<LogicalLane>(0), exception::MapException);
    EXPECT_THROW(map_api_data_.Get<LogicalLaneBoundary>(0), exception::MapException);
}

TEST_F(MapApiDataWrapperFixture, GivenEmptyData_WhenFind_ThenNullptrIsReturned)
{
    EXPECT_EQ(map_api_data_.Find<ReferenceLine>(0), nullptr);
    EXPECT_EQ(map_api_data_.Find<LogicalLane>(0), nullptr);
    EXPECT_EQ(map_api_data_.Find<LogicalLaneBoundary>(0), nullptr);
}

TEST_F(MapApiDataWrapperFixture, GivenEmptyData_WhenGet_ThenExceptionIsThrown)
{
    EXPECT_THROW(map_api_data_.Get<ReferenceLine>(0), exception::MapElementNotFound);
    EXPECT_THROW(map_api_data_.Get<LogicalLane>(0), exception::MapElementNotFound);
    EXPECT_THROW(map_api_data_.Get<LogicalLaneBoundary>(0), exception::MapElementNotFound);
}

TEST_F(MapApiDataWrapperFixture, GivenDataWithSingleElement_WhenFindOrGet_ThenTheElementIsReturned)
{
    mantle_api::UniqueId expected_id{69};
    auto [reference_line, logical_lane, logical_lane_boundary] = AddNewElements(expected_id);

    auto* found_reference_line = map_api_data_.Find<ReferenceLine>(expected_id);
    auto* found_logical_lane = map_api_data_.Find<LogicalLane>(expected_id);
    auto* found_logical_lane_boundary = map_api_data_.Find<LogicalLaneBoundary>(expected_id);

    ASSERT_NE(found_reference_line, nullptr);
    ASSERT_NE(found_logical_lane, nullptr);
    ASSERT_NE(found_logical_lane_boundary, nullptr);
    ExpectEqual(*found_reference_line, reference_line);
    ExpectEqual(*found_logical_lane, logical_lane);
    ExpectEqual(*found_logical_lane_boundary, logical_lane_boundary);

    auto get_reference_line = map_api_data_.Get<ReferenceLine>(expected_id);
    auto get_logical_lane = map_api_data_.Get<LogicalLane>(expected_id);
    auto get_logical_lane_boundary = map_api_data_.Get<LogicalLaneBoundary>(expected_id);

    ExpectEqual(get_reference_line, reference_line);
    ExpectEqual(get_logical_lane, logical_lane);
    ExpectEqual(get_logical_lane_boundary, logical_lane_boundary);
}

TEST_F(MapApiDataWrapperFixture, GivenDataWithMultipleElements_WhenFindOrGetNonExistingElement_ThenExceptionIsThrown)
{
    AddNewElements(6);
    AddNewElements(69);
    AddNewElements(3);

    EXPECT_THROW(map_api_data_.Get<ReferenceLine>(80), exception::MapElementNotFound);
    EXPECT_THROW(map_api_data_.Get<LogicalLane>(94), exception::MapElementNotFound);
    EXPECT_THROW(map_api_data_.Get<LogicalLaneBoundary>(16), exception::MapElementNotFound);
}

TEST_F(MapApiDataWrapperFixture, GivenDataWithMultipleElements_WhenFindOrGet_ThenCorrectElementIsReturned)
{
    auto [reference_line6, logical_lane6, logical_lane_boundary6] = AddNewElements(6);
    auto [reference_line69, logical_lane69, logical_lane_boundary69] = AddNewElements(69);
    auto [reference_line3, logical_lane3, logical_lane_boundary3] = AddNewElements(3);

    auto get_reference_line6 = map_api_data_.Get<ReferenceLine>(6);
    auto get_logical_lane69 = map_api_data_.Get<LogicalLane>(69);
    auto get_logical_lane_boundary3 = map_api_data_.Get<LogicalLaneBoundary>(3);

    EXPECT_EQ(get_reference_line6.id, 6);
    EXPECT_EQ(get_logical_lane69.id, 69);
    EXPECT_EQ(get_logical_lane_boundary3.id, 3);
}

TEST_F(MapApiDataWrapperFixture, GivenMapWithAllPossibleElements_WhenClearMap_ThenAllElementsArePurged)
{
    AddNewElements(6);
    AddNewElements(69);
    AddNewElements(3);

    EXPECT_EQ(map_api_data_.GetAll<ReferenceLine>().size(), 3);
    EXPECT_EQ(map_api_data_.GetAll<LogicalLane>().size(), 3);
    EXPECT_EQ(map_api_data_.GetAll<LogicalLaneBoundary>().size(), 3);

    map_api_data_.ClearMap();

    EXPECT_EQ(map_api_data_.GetAll<ReferenceLine>().size(), 0);
    EXPECT_EQ(map_api_data_.GetAll<LogicalLane>().size(), 0);
    EXPECT_EQ(map_api_data_.GetAll<LogicalLaneBoundary>().size(), 0);
}

}  // namespace gtgen::core::environment::map