/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_INTERNAL_LANEUTILITY_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_INTERNAL_LANEUTILITY_H

#include "Core/Environment/Map/Geometry/bounding_box_utils.h"
#include "Core/Environment/Map/GtGenMap/lane.h"

#include <fmt/format.h>

#include <vector>

namespace gtgen::core::environment::map
{

inline void AddPointsToLanesPolygonAndUpdateBoundingBox(Lane& lane, const std::vector<glm::dvec2>& points)
{
    for (const auto& point : points)
    {
        lane.shape_2d.points.emplace_back(point);
        MergeBoundingBoxWithPoint(lane.axis_aligned_bounding_box, point);
    }
}

/// @brief Returns a string with all the lane information
inline std::string ToVerboseString(const Lane& lane)
{
    return fmt::format(
        "Lane:\nID: {}\nCenter-Line-Point-Count: "
        "{}\nPredecessor-Count: {}\nSuccessor-Count: {}\nRight-Boundaries-Count: "
        "{}\nLeft-Boundaries-Count: {}\nRight-Adjacent-Lanes-Count: {}\nLeft-Adjacent-Lanes-Count: {}",
        lane.id,
        lane.center_line.size(),
        lane.predecessors.size(),
        lane.successors.size(),
        lane.right_lane_boundaries.size(),
        lane.left_lane_boundaries.size(),
        lane.right_adjacent_lanes.size(),
        lane.left_adjacent_lanes.size());
}

}  // namespace gtgen::core::environment::map

template <>
struct fmt::formatter<gtgen::core::environment::map::Lane>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    auto format(const gtgen::core::environment::map::Lane& lane,
                FormatContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return format_to(ctx.out(), "Lane {}", lane.id);
    }
};

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_INTERNAL_LANEUTILITY_H
