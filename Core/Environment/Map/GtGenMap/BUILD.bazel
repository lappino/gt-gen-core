cc_library(
    name = "gtgen_map_finalizer",
    srcs = ["gtgen_map_finalizer.cpp"],
    hdrs = ["gtgen_map_finalizer.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Environment/Map:__subpackages__",
        "//Core/Tests/TestUtils/MapCatalogue:__subpackages__",
    ],
    deps = [
        ":gtgenmap",
        "//Core/Environment/Map/Geometry:bounding_box_utils",
        "//Core/Environment/Map/GtGenMap/Internal:lane_shape_extractor",
        "//Core/Environment/Map/GtGenMap/Internal:spatial_hash",
        "//Core/Service/GlmWrapper:glm_basic_vector_utils",
        "//Core/Service/Utility:algorithm_utils",
    ],
)

cc_test(
    name = "gtgen_map_finalizer_test",
    timeout = "short",
    srcs = ["gtgen_map_finalizer_test.cpp"],
    deps = [
        ":gtgen_map_finalizer",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "gtgenmap",
    srcs = ["gtgen_map.cpp"],
    hdrs = ["gtgen_map.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Communication/Dispatchers:__subpackages__",
        "//Core/Communication/Serialization:__subpackages__",
        "//Core/Environment:__subpackages__",
        "//Core/Tests:__subpackages__",
    ],
    deps = [
        ":lane",
        ":lane_boundary",
        ":lane_group",
        ":map_api_data_wrapper",
        ":patch",
        ":road_objects",
        ":signs",
        "//Core/Environment/Map/Common:converter_utility",
        "//Core/Environment/Map/Common:exceptions",
        "//Core/Environment/Map/Geometry:point_in_polygon",
        "//Core/Environment/Map/GtGenMap/Internal:spatial_hash",
        "//Core/Environment/Map/GtGenMap/Internal:traffic_light",
    ],
)

cc_test(
    name = "gtgenmap_test",
    timeout = "short",
    srcs = ["gtgenmap_test.cpp"],
    deps = [
        ":gtgenmap",
        "@googletest//:gtest_main",
    ],
)

cc_library(
    name = "map_api_data_wrapper",
    srcs = ["map_api_data_wrapper.cpp"],
    hdrs = ["map_api_data_wrapper.h"],
    visibility = [
        "//Core/Environment/Map/GtGenMap:__subpackages__",
    ],
    deps = [
        "//Core/Environment/Map/Common:exceptions",
        "//Core/Service/Utility:unique_id_provider",
        "@map_sdk//:map_api",
    ],
)

cc_test(
    name = "map_api_data_wrapper_test",
    timeout = "short",
    srcs = ["map_api_data_wrapper_test.cpp"],
    deps = [
        ":gtgenmap",
        ":map_api_data_wrapper",
        "@googletest//:gtest_main",
    ],
)

# Data only - no test needed
cc_library(
    name = "lane",
    hdrs = ["lane.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Communication/Serialization:__subpackages__",
        "//Core/Environment/GroundTruth:__subpackages__",
        "//Core/Environment/Map:__subpackages__",
        "//Core/Environment/PathFinding:__subpackages__",
        "//Core/Tests/TestUtils/MapCatalogue:__subpackages__",
    ],
    deps = [
        "//Core/Environment/Map/Geometry:bounding_box",
        "//Core/Environment/Map/Geometry:polygon",
        "//Core/Environment/Map/GtGenMap/Internal:lane_flags",
        "@fmt",
    ],
)

# Data only - no test needed
cc_library(
    name = "lane_boundary",
    hdrs = ["lane_boundary.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Communication/Serialization:__subpackages__",
        "//Core/Environment/Map:__subpackages__",
        "//Core/Tests/TestUtils/MapCatalogue:__subpackages__",
    ],
    deps = ["@mantle_api"],
)

# Data only - no test needed
cc_library(
    name = "lane_group",
    hdrs = ["lane_group.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Communication/Serialization:__subpackages__",
        "//Core/Environment/Map/GtGenMap:__subpackages__",
        "//Core/Tests/TestUtils/MapCatalogue:__subpackages__",
    ],
    deps = ["@mantle_api"],
)

# TODO: and not used at all!!
cc_library(
    name = "lane_group_utility",
    hdrs = ["lane_group_utility.h"],
    deps = [
        ":lane_group",
        "@fmt",
        "@mantle_api",
    ],
)

# TODO: add test
#cc_test(
#    name = "lane_group_utility_test",
#    timeout = "short",
#    srcs = ["lane_group_utility_test.cpp"],
#
#    deps = [
#        ":lane_group_utility",
#        "@googletest//:gtest_main",
#    ],
#)
# TODO: Data only but a class? Tests needed or can this be changed to a struct? and simplified? because
#  inheritance is nice, but might never be needed to have another patch type - over engineered?
cc_library(
    name = "patch",
    hdrs = ["patch.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Environment/Map:__subpackages__",
    ],
    deps = [
        "//Core/Environment/Map/Geometry:bounding_box",
        "//Core/Environment/Map/Geometry:polygon",
        "@mantle_api",
    ],
)

cc_library(
    name = "road_objects",
    hdrs = ["road_objects.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Environment/Map:__subpackages__",
    ],
    deps = [
        "//Core/Service/Osi:osi",
        "@mantle_api",
    ],
)

cc_library(
    name = "signs",
    hdrs = ["signs.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Communication/Serialization:__subpackages__",
    ],
    deps = [
        ":lane",
        "//Core/Service/Osi:osi",
        "//Core/Service/Utility:unique_id_provider",
    ],
)

cc_test(
    name = "signs_test",
    timeout = "short",
    srcs = ["signs_test.cpp"],
    deps = [
        ":signs",
        "@googletest//:gtest_main",
    ],
)

# TODO: More suited in Geometry package?
cc_library(
    name = "utility",
    srcs = ["utility.cpp"],
    hdrs = ["utility.h"],
    visibility = [
        "//:__pkg__",
        "//Core/Environment/Map:__subpackages__",
    ],
    deps = [
        ":lane_boundary",
        "//Core/Environment/Map/Geometry:project_query_point_on_polyline",
        "//Core/Environment/Map/Geometry:scalar_projection_2d",
        "//Core/Service/GlmWrapper:glm_wrapper",
    ],
)

# TODO: too much stuff in here - split out and have dedicated tests as well. There is already a second test for this
cc_test(
    name = "utility_test",
    timeout = "short",
    srcs = ["utility_test.cpp"],
    deps = [
        ":utility",
        "//Core/Tests/TestUtils:expect_extensions",
        "//Core/Tests/TestUtils/MapCatalogue:map_catalogue",
        "@googletest//:gtest_main",
    ],
)
