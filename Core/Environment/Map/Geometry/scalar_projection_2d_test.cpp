/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Environment/Map/Geometry/scalar_projection_2d.h"

#include <gtest/gtest.h>

namespace gtgen::core::environment::map
{
using units::literals::operator""_m;

TEST(ScalarProjection2dVerticallyTranslatedBackTo3dTest,
     GivenLineSegmentAndOnePointRoughlyOnIt_WhenScalarProjection2d_ThenIsPointProjectedOnLineSegment)
{
    mantle_api::Vec3<units::length::meter_t> point{1.0_m, 1.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> start{0.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> end{2.0_m, 0.0_m, 0.0_m};

    const auto result = ScalarProjection2d(point, start, end);

    ASSERT_TRUE(result);
    // length of line is 4 --> half of it is 2
    EXPECT_DOUBLE_EQ(0.5, result.value());
}

TEST(ScalarProjection2dVerticallyTranslatedBackTo3dTest,
     GivenLineSegmentAndOnePointNearSegmentBeginning_WhenScalarProjection2d_ThenIsPointProjectedNearBeginningOfSegment)
{
    mantle_api::Vec3<units::length::meter_t> point{0.0_m, 3.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> start{0.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> end{2.0_m, 0.0_m, 0.0_m};

    const auto result = ScalarProjection2d(point, start, end);

    ASSERT_TRUE(result);
    EXPECT_DOUBLE_EQ(0.0, result.value());
}

TEST(ScalarProjection2dVerticallyTranslatedBackTo3dTest,
     GivenLineSegmentWithEqualPoints_WhenProjectionRatioIsRequested_ThenEmptyResultWillBeReturned)
{
    mantle_api::Vec3<units::length::meter_t> point{};
    mantle_api::Vec3<units::length::meter_t> line_seg_first_point{1.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> line_seg_second_point = line_seg_first_point;

    const auto projection_ratio = ScalarProjection2d(point, line_seg_first_point, line_seg_second_point);

    EXPECT_FALSE(projection_ratio.has_value());
}

TEST(ScalarProjection2dVerticallyTranslatedBackTo3dTest,
     GivenPointBeforeFirstLineSegmentPoint_WhenProjectionRatioIsRequested_ThenNegativeValueWillBeReturned)
{
    ///   x      x---------------x
    ///   p     ls1             ls2

    mantle_api::Vec3<units::length::meter_t> point{0.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> line_seg_first_point{2.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> line_seg_second_point{3.0_m, 0.0_m, 0.0_m};

    double expected_projection_ratio{-2.0};

    const auto projection_ratio = ScalarProjection2d(point, line_seg_first_point, line_seg_second_point);

    EXPECT_EQ(expected_projection_ratio, projection_ratio);
}

TEST(ScalarProjection2dVerticallyTranslatedBackTo3dTest,
     GivenPointAfterSecondLineSegmentPoint_WhenProjectionRatioIsRequested_ThenPostiveValueWillBeReturned)
{
    ///  x---------------x       x
    /// ls1             ls2      p

    mantle_api::Vec3<units::length::meter_t> point{4.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> line_seg_first_point{2.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> line_seg_second_point{3.0_m, 0.0_m, 0.0_m};

    double expected_projection_ratio{2.0};

    const auto projection_ratio = ScalarProjection2d(point, line_seg_first_point, line_seg_second_point);

    EXPECT_EQ(expected_projection_ratio, projection_ratio);
}

TEST(
    ScalarProjection2dVerticallyTranslatedBackTo3dTest,
    GivenPointBetweenLineSegmentPointsButVerticallyShifted_WhenProjectionRatioIsRequested_ThenPostiveValueWillBeReturned)
{
    ///          p
    ///          x
    ///
    ///  x----------------x
    /// ls1              ls2

    mantle_api::Vec3<units::length::meter_t> point{2.5_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> line_seg_first_point{2.0_m, 0.0_m, 0.0_m};
    mantle_api::Vec3<units::length::meter_t> line_seg_second_point{3.0_m, 0.0_m, 0.0_m};

    constexpr double expected_projection_ratio{0.5};

    const auto projection_ratio = ScalarProjection2d(point, line_seg_first_point, line_seg_second_point);

    EXPECT_EQ(expected_projection_ratio, projection_ratio);
}

}  // namespace gtgen::core::environment::map
