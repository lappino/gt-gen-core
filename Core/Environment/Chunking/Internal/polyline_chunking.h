/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_CHUNKING_INTERNAL_POLYLINECHUNKING_H
#define GTGEN_CORE_ENVIRONMENT_CHUNKING_INTERNAL_POLYLINECHUNKING_H

#include "Core/Environment/Chunking/world_chunk.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map.h"

#include <map>

namespace gtgen::core::environment::chunking
{

/// @brief Slightly improved chunking implementation.
///
/// Uses boost::geometry to identify all chunks that a lane or road-object intersects with.
///
/// Chunking strategy:
///    - LaneGroups: A LaneGroup is added to a chunk if any of the lane boundaries intersects the chunk.
///                  If a lane has only one-sided lane boundaries (or none), the centerline is processed additionally.
///    - RoadObjects: A RoadObject is added to a chunk if its pose.position or the polyline given by its base_polygon
///    intersects the chunk. The dimension attribute is not yet handled.
///    - TrafficSign: A TrafficSign is added to a chunk if its position is within a chunk. The dimension attribute is
///    not yet handled.
class PolylineChunking
{
  public:
    void SetChunkSize(std::uint16_t chunk_size);

    /// @brief Defines the size of the output grid.
    ///
    /// Example: cells = 2 results in a 5x5 grid.
    ///
    /// @param cells Number of cells in each direction, originating from the center cell.
    void SetCellsPerDirection(std::uint16_t cells);

    /// @brief Initializes the static chunk content from the given map.
    StaticChunkList InitializeChunks(const environment::map::GtGenMap& map);

    /// @brief Get the index of the chunk that contains the given position, see MapChunk
    ///
    /// Examples:
    ///      - ChunkSize: 50, position: (0.0, 0.0) --> ChunkKey = (0, 0)
    ///      - ChunkSize: 50, position: (10.0, 25.0) --> ChunkKey = (0, 0)
    ///      - ChunkSize: 50, position: (-10.0, 300.0) --> ChunkKey = (-1, 6)
    ///      - ChunkSize: 50, position: (-79.0, -10.0) --> ChunkKey = (-2, -1)
    ChunkKey GetChunkKey(const CoordinateType& position) const;

    /// @brief Compute the lower left coordinate of the chunk (its origin) identified by the given ChunkKey.
    CoordinateType GetLowerLeft(const ChunkKey& key) const;

    /// @brief Compute theg upper right coordinate of the chunk identified by the given ChunkKey.
    CoordinateType GetUpperRight(const ChunkKey& key) const;

    /// @brief Converst an internal chunk representation to a GroundTruthChunk
    WorldChunk ConvertToGroundTruthChunk(const MapChunk& chunk) const;

    /// @brief Converst an internal chunk representation to a GroundTruthChunk
    WorldChunk ConvertToGroundTruthChunk(MapChunk&& chunk) const;

    /// @brief Get a list of keys of StaticChunkList around the given point-of-interest.
    std::vector<ChunkKey> GetChunkKeysAroundPoint(const CoordinateType& point) const;

    void ValidateStaticChunkList(const StaticChunkList& static_chunk_list) const;

  private:
    MapChunk& GetOrCreateChunk(const ChunkKey& key, StaticChunkList& chunks) const;

    void ChunkLaneGroups(const environment::map::GtGenMap& map, StaticChunkList& chunks);
    void ChunkRoadObjects(const environment::map::RoadObjects& road_objects, StaticChunkList& chunks);
    void ChunkTrafficSigns(const environment::map::TrafficSigns& traffic_signs, StaticChunkList& chunks);
    void ChunkTrafficLights(const environment::map::TrafficLights& traffic_lights, StaticChunkList& chunks);

    void AddToChunk(const ChunkKey& key, const environment::map::LaneGroup* lane_group, StaticChunkList& chunks);
    void AddToChunk(const ChunkKey& key, const environment::map::RoadObject* road_object, StaticChunkList& chunks);
    void AddToChunk(const ChunkKey& key, const environment::map::TrafficSign* traffic_sign, StaticChunkList& chunks);
    void AddToChunk(const ChunkKey& key, const environment::map::TrafficLight* traffic_light, StaticChunkList& chunks);

    template <typename ValueType, typename ChunkKeyProcessor, typename PositionAccessor>
    void ProcessLineString(const std::vector<ValueType>& line_string,
                           ChunkKeyProcessor process_chunk_key,
                           PositionAccessor access_position);

    double chunk_size_{50};

    /// @brief This means number of cells around the center cell - quadratic.
    ///
    /// e.g. cells per direction = 2 means a 5x5 matrix of cells will be emitted
    std::int32_t cells_per_direction_{0};
};

}  // namespace gtgen::core::environment::chunking

#endif  // GTGEN_CORE_ENVIRONMENT_CHUNKING_INTERNAL_POLYLINECHUNKING_H
