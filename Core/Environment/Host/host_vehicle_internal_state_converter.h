/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_HOST_HOSTVEHCILEINTERNALSTATECONVERTER_H
#define GTGEN_CORE_ENVIRONMENT_HOST_HOSTVEHCILEINTERNALSTATECONVERTER_H

#include "osi_trafficupdate.pb.h"

#include <map>
#include <string>

namespace gtgen::core::environment::host
{

std::map<std::string, std::string> ConvertTrafficUpdateInternalStateToKeyValue(
    const osi3::TrafficUpdate& proto_traffic_update);

}  // namespace gtgen::core::environment::host

#endif  // GTGEN_CORE_ENVIRONMENT_HOST_HOSTVEHCILEINTERNALSTATECONVERTER_H
