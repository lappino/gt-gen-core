/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Host/host_vehicle_internal_state_converter.h"

#include "Core/Environment/Exception/exception.h"

#include <fmt/format.h>
#include <gtest/gtest.h>

namespace gtgen::core::environment::host
{

class HostVehicleInternalStateConverterTest : public testing::Test
{
  protected:
    HostVehicleInternalStateConverterTest()
    {
        osi3::HostVehicleData* host_vehicle_data = traffic_update_.add_internal_state();
        vehicle_adf_ = host_vehicle_data->add_vehicle_automated_driving_function();
        host_vehicle_data->mutable_host_vehicle_id()->set_value(0);
    }
    osi3::TrafficUpdate traffic_update_{};
    osi3::HostVehicleData::VehicleAutomatedDrivingFunction* vehicle_adf_;
};

TEST_F(HostVehicleInternalStateConverterTest,
       GivenTrafficUpdateWithNameAndState_WhenConvertTrafficUpdateInternalStateToKeyValue_ThenKeyValueIsReturned)
{
    vehicle_adf_->set_name(osi3::HostVehicleData::VehicleAutomatedDrivingFunction::NAME_CRUISE_CONTROL);
    vehicle_adf_->set_state(osi3::HostVehicleData::VehicleAutomatedDrivingFunction::STATE_ACTIVE);
    const auto values = ConvertTrafficUpdateInternalStateToKeyValue(traffic_update_);
    ASSERT_EQ(values.size(), 1);
    EXPECT_EQ(values.begin()->first, "NAME_CRUISE_CONTROL");
    EXPECT_EQ(values.begin()->second, "STATE_ACTIVE");
}

TEST_F(HostVehicleInternalStateConverterTest,
       GivenTrafficUpdateWithCustomNameAndState_WhenConvertTrafficUpdateInternalStateToKeyValue_ThenKeyValueIsReturned)
{
    const auto custom_name = "my_custom_name";
    vehicle_adf_->set_name(osi3::HostVehicleData::VehicleAutomatedDrivingFunction::NAME_OTHER);
    vehicle_adf_->set_custom_name(custom_name);
    vehicle_adf_->set_state(osi3::HostVehicleData::VehicleAutomatedDrivingFunction::STATE_ACTIVE);
    const auto values = ConvertTrafficUpdateInternalStateToKeyValue(traffic_update_);
    ASSERT_EQ(values.size(), 1);
    EXPECT_EQ(values.begin()->first, custom_name);
    EXPECT_EQ(values.begin()->second, "STATE_ACTIVE");
}

TEST_F(HostVehicleInternalStateConverterTest,
       GivenTrafficUpdateWithNameAndCustomState_WhenConvertTrafficUpdateInternalStateToKeyValue_ThenKeyValueIsReturned)
{
    const auto custom_state = "my_custom_state";
    vehicle_adf_->set_name(osi3::HostVehicleData::VehicleAutomatedDrivingFunction::NAME_CRUISE_CONTROL);
    vehicle_adf_->set_state(osi3::HostVehicleData::VehicleAutomatedDrivingFunction::STATE_OTHER);
    vehicle_adf_->set_custom_state(custom_state);
    const auto values = ConvertTrafficUpdateInternalStateToKeyValue(traffic_update_);
    ASSERT_EQ(values.size(), 1);
    EXPECT_EQ(values.begin()->first, "NAME_CRUISE_CONTROL");
    EXPECT_EQ(values.begin()->second, custom_state);
}

TEST_F(HostVehicleInternalStateConverterTest,
       GivenTrafficUpdateWithNameAndCustomDetail_WhenConvertTrafficUpdateInternalStateToKeyValue_ThenKeyValueIsReturned)
{
    const auto custom_detail_name = "detail_name1";
    const auto custom_detail_value = "detail_value1";
    vehicle_adf_->set_name(osi3::HostVehicleData::VehicleAutomatedDrivingFunction::NAME_CRUISE_CONTROL);
    auto* custom_detail = vehicle_adf_->add_custom_detail();
    custom_detail->set_key(custom_detail_name);
    custom_detail->set_value(custom_detail_value);
    const auto values = ConvertTrafficUpdateInternalStateToKeyValue(traffic_update_);
    ASSERT_EQ(values.size(), 1);
    EXPECT_EQ(values.begin()->first, fmt::format("NAME_CRUISE_CONTROL.{}", custom_detail_name));
    EXPECT_EQ(values.begin()->second, custom_detail_value);
}

TEST_F(HostVehicleInternalStateConverterTest,
       GivenTrafficUpdateWithNameAndNoState_WhenConvertTrafficUpdateInternalStateToKeyValue_ThenEmptyKeyValueIsReturned)
{
    vehicle_adf_->set_name(osi3::HostVehicleData::VehicleAutomatedDrivingFunction::NAME_CRUISE_CONTROL);
    const auto values = ConvertTrafficUpdateInternalStateToKeyValue(traffic_update_);
    ASSERT_EQ(values.size(), 0);
}

TEST_F(HostVehicleInternalStateConverterTest,
       GivenTrafficUpdateWithNoNameAndState_WhenConvertTrafficUpdateInternalStateToKeyValue_ThenEmptyKeyValueIsReturned)
{
    vehicle_adf_->set_state(osi3::HostVehicleData::VehicleAutomatedDrivingFunction::STATE_ACTIVE);
    const auto values = ConvertTrafficUpdateInternalStateToKeyValue(traffic_update_);
    ASSERT_EQ(values.size(), 0);
}

TEST_F(
    HostVehicleInternalStateConverterTest,
    GivenTrafficUpdateWithoutInternalState_WhenConvertTrafficUpdateInternalStateToKeyValue_ThenEmptyKeyValueIsReturned)
{
    traffic_update_.clear_internal_state();
    const auto values = ConvertTrafficUpdateInternalStateToKeyValue(traffic_update_);
    ASSERT_EQ(values.size(), 0);
}

TEST_F(HostVehicleInternalStateConverterTest,
       GivenTrafficUpdateWithTwoInternalStates_WhenConvertTrafficUpdateInternalStateToKeyValue_ThenAssertionError)
{
    traffic_update_.add_internal_state();
    EXPECT_THROW(ConvertTrafficUpdateInternalStateToKeyValue(traffic_update_),
                 gtgen::core::environment::EnvironmentException);
}

}  // namespace gtgen::core::environment::host
