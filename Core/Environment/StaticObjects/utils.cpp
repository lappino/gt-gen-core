/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/StaticObjects/utils.h"

#include "Core/Service/Utility/string_utils.h"

#include <vector>

namespace gtgen::core::environment::static_objects
{

std::string GetTypeIdentifier(const std::string& identifier)
{
    auto signal_type_information = service::utility::Split(identifier, '-');
    if (signal_type_information.size() != 0)
    {
        return signal_type_information.front();
    }
    else
    {
        return "";
    }
}

std::optional<std::string> GetSignalSubType(const std::string& identifier)
{
    auto signal_type_information = service::utility::Split(identifier, '-');
    if (signal_type_information.size() == 2)
    {
        return signal_type_information.back();
    }
    return std::nullopt;
}

}  // namespace gtgen::core::environment::static_objects
