/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#include "Core/Service/GlmWrapper/glm_basic_scalar_utils.h"

#include <glm/glm.hpp>

namespace gtgen::core::service::glmwrapper
{

double Radians(double deg)
{
    return glm::radians(deg);
}

double Clamp(double a, double b, double c)
{
    return glm::clamp(a, b, c);
}

double Mix(double d1, double d2, double d3)
{
    return glm::mix(d1, d2, d3);
}

}  // namespace gtgen::core::service::glmwrapper
