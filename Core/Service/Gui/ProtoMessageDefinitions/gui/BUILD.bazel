load("//Core/Service/Gui/ProtoMessageDefinitions/proto_helper_rules:bazel_proto_helper_rules.bzl", "get_only_source_files")

proto_library(
    name = "gui_proto",
    srcs = [
        "core_info.proto",
        "message_size.proto",
        "message_types.proto",
        "message_wrapper.proto",
        "scenario_data.proto",
    ],
    strip_import_prefix = "/Core/Service/Gui/ProtoMessageDefinitions/gui",
    deps = [
        ":config_proto",
        ":editor_proto",
        ":map_proto",
        ":scenario_proto",
        ":test_proto",
        ":ui_proto",
        "@//third_party/open_simulation_interface:open_simulation_interface_proto",
    ],
)

proto_library(
    name = "config_proto",
    srcs = glob(["config/*.proto"]),
    strip_import_prefix = "/Core/Service/Gui/ProtoMessageDefinitions/gui/config",
    deps = ["@//third_party/open_simulation_interface:open_simulation_interface_proto"],
)

proto_library(
    name = "editor_proto",
    srcs = ["editor/editor_api.proto"],
    strip_import_prefix = "/Core/Service/Gui/ProtoMessageDefinitions/gui/editor",
    deps = [
        ":map_proto",
        ":scenario_proto",
        "@//third_party/open_simulation_interface:open_simulation_interface_proto",
    ],
)

proto_library(
    name = "map_proto",
    srcs = glob(["map/*.proto"]),
    strip_import_prefix = "/Core/Service/Gui/ProtoMessageDefinitions/gui/map",
    deps = ["@//third_party/open_simulation_interface:open_simulation_interface_proto"],
)

proto_library(
    name = "scenario_proto",
    srcs = glob(["scenario/*.proto"]),
    strip_import_prefix = "/Core/Service/Gui/ProtoMessageDefinitions/gui/scenario",
    deps = ["@//third_party/open_simulation_interface:open_simulation_interface_proto"],
)

proto_library(
    name = "test_proto",
    srcs = ["test/test_interface.proto"],
    strip_import_prefix = "/Core/Service/Gui/ProtoMessageDefinitions/gui/test",
    deps = ["@//third_party/open_simulation_interface:open_simulation_interface_proto"],
)

proto_library(
    name = "ui_proto",
    srcs = glob(["ui/*.proto"]),
    strip_import_prefix = "/Core/Service/Gui/ProtoMessageDefinitions/gui/ui",
    deps = [
        ":map_proto",
        "@//third_party/open_simulation_interface:open_simulation_interface_proto",
    ],
)

proto_library(
    name = "osi_gui_map_vec3d_proto",
    srcs = ["map/vector3d.proto"],
    strip_import_prefix = "/Core/Service/Gui/ProtoMessageDefinitions/gui/map",
    deps = ["@//third_party/open_simulation_interface:open_simulation_interface_proto"],
)

get_only_source_files(
    name = "osi_gui_ui_exports",
    srcs = [":ui_proto_cc"],
    visibility = ["//visibility:public"],
)

get_only_source_files(
    name = "osi_gui_map_vec3d_exports",
    srcs = [":osi_gui_map_vec3d_proto_cc"],
    visibility = ["//visibility:public"],
)

cc_proto_library(
    name = "config_proto_cc",
    deps = [":config_proto"],
)

cc_proto_library(
    name = "editor_proto_cc",
    deps = [":editor_proto"],
)

cc_proto_library(
    name = "gui_proto_cc",
    deps = [":gui_proto"],
)

cc_proto_library(
    name = "map_proto_cc",
    deps = [":map_proto"],
)

cc_library(
    name = "osi_gui",
    strip_include_prefix = ".",
    visibility = ["//visibility:public"],
    deps = [
        ":config_proto_cc",
        ":editor_proto_cc",
        ":gui_proto_cc",
        ":map_proto_cc",
        ":scenario_proto_cc",
        ":test_proto_cc",
        ":ui_proto_cc",
    ],
)

# Needed for packaging
cc_proto_library(
    name = "osi_gui_map_vec3d_proto_cc",
    deps = [":osi_gui_map_vec3d_proto"],
)

cc_proto_library(
    name = "scenario_proto_cc",
    deps = [":scenario_proto"],
)

cc_proto_library(
    name = "test_proto_cc",
    deps = [":test_proto"],
)

cc_proto_library(
    name = "ui_proto_cc",
    deps = [":ui_proto"],
)
