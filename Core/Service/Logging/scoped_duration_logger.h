/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_LOGGING_SCOPEDDURATIONLOGGER_H
#define GTGEN_CORE_SERVICE_LOGGING_SCOPEDDURATIONLOGGER_H

#include "Core/Service/Logging/logging.h"

#include <chrono>
#include <string>

namespace gtgen::core::service::logging
{
/// Within a scope, this class will create a timer, which will, when destructed, log how long it was alive
/// i.e.:
/// {
///     ScopedDurationLogger("Parsing file");
/// }
/// will result in a logging like:
/// "Parsing file"
/// ...
/// "Parsing file took: 1ms"
class ScopedDurationLogger
{
  public:
    /// @brief Create a timer, which when constructed and destructed, will log the provided message. When destructed,
    /// the time this object was alive for in milliseconds will also be logged. The log message will be in the 'Info'
    /// level.
    ///
    /// @param message The message printed at the beginning of//Core/Service/Logging:scoped_duration_logger the logged
    /// statements
    explicit ScopedDurationLogger(const std::string& message)
        : message_{message}, start_{std::chrono::system_clock::now()}
    {
        Info("{}...", message_);
    }

    ~ScopedDurationLogger()
    {
        auto end = std::chrono::system_clock::now();
        auto time_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - start_).count();
        Info("{} took: {}ms", message_, time_ms);
    }

    ScopedDurationLogger(const ScopedDurationLogger&) = delete;
    ScopedDurationLogger& operator=(const ScopedDurationLogger&) = delete;
    ScopedDurationLogger(ScopedDurationLogger&&) = default;
    ScopedDurationLogger& operator=(ScopedDurationLogger&&) = default;

  private:
    std::string message_;
    std::chrono::time_point<std::chrono::system_clock> start_;
};

}  // namespace gtgen::core::service::logging

#endif  // GTGEN_CORE_SERVICE_LOGGING_SCOPEDDURATIONLOGGER_H
